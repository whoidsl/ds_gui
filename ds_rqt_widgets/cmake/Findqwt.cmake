# Look for QWT
#
# Set
#  GEOGRAPHICLIB_FOUND = TRUE
#  GeographicLib_INCLUDE_DIRS = /usr/local/include
#  GeographicLib_LIBRARIES = /usr/local/lib/libGeographic.so
#  GeographicLib_LIBRARY_DIRS = /usr/local/lib

find_library (qwt_LIBRARIES qwt)
find_path(qwt_HDR qwt.h
        PATHS /usr/local/include/qwt
              /usr/include/qwt
        )

message(STATUS "HEADER: ${qwt_HDR}")
if (qwt_HDR)
  get_filename_component (qwt_INCLUDE_DIRS
    "${qwt_HDR}" PATH)
endif ()

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args (qwt DEFAULT_MSG
  qwt_LIBRARIES qwt_INCLUDE_DIRS)
mark_as_advanced(qwt DEFAULT_MSG
  qwt_LIBRARIES qwt_INCLUDE_DIRS)
