//
// Created by ivaughn on 6/14/21.
//

#ifndef DS_RQT_WIDGETS_PHINSINSCONTROLLER_H
#define DS_RQT_WIDGETS_PHINSINSCONTROLLER_H

#include <QWidget>
#include <QLabel>
#include <QGroupBox>
#include "QtRosSubscription.h"
#include <ds_sensor_msgs/PhinsStdbin3.h>
#include <ds_sensor_msgs/PhinsConfig.h>
#include <geometry_msgs/Vector3Stamped.h>

#include "IndicatorButton.h"
#include "IndicatorLabel.h"
#include "IndicatorButtonGroup.h"

namespace ds_rqt_widgets {


class PhinsInsExternalSensor : public QWidget {
  Q_OBJECT
 public:
  explicit PhinsInsExternalSensor(const std::string& name,
                                  int algo_word,
                                  uint32_t algo_mask_reception,
                                  uint32_t algo_mask_valid,
                                  uint32_t algo_mask_waiting,
                                  uint32_t algo_mask_rejected,
                                  QWidget* parent=0);
  int setup(QGridLayout* layout, int row, int col=0, bool visible=true);
  void refresh(const ds_sensor_msgs::PhinsStdbin3& msg,
                const ds_sensor_msgs::PhinsConfig& config_msg);

  void updateConfig(ds_sensor_msgs::PhinsConfig& config_msg) const;

  virtual int getRejectMode(const ds_sensor_msgs::PhinsConfig& config_msg) const = 0;
  virtual void setRejectMode(ds_sensor_msgs::PhinsConfig& config_msg, int mode) const = 0;

 signals:
  void rejectModeUpdated(int);

 protected:
  int reject_filter_mode;
  std::string name;
  int algo_word;
  uint32_t algo_mask_reception;
  uint32_t algo_mask_valid;
  uint32_t algo_mask_waiting;
  uint32_t algo_mask_rejected;

  QLabel* label;
  IndicatorButton* on;
  IndicatorButton* off;
  IndicatorButton* force;
  IndicatorLabel* reception;
  IndicatorLabel* valid;
  IndicatorLabel* waiting;
  IndicatorLabel* rejected;

  void setRejectionPolicy(int value);
};

namespace PhinsIns {
enum PhinsModel {
  PHINS3=0,
  COMPACT_C3=1,
  COMPACT_C5=2,
  COMPACT_C7=3,
};

enum PhinsInputs {
  GPS1 = 0x0001,
  GPS2 = 0x0002,
  DVL1 = 0x0004,
  DVLWT1 = 0x0008,
  DVL2 = 0x0010,
  DVLWT2 = 0x0020,
  DEPTH = 0x0040,
  USBL = 0x0080
};
/// operator to let us sensibly combine inputs
inline PhinsInputs operator|(PhinsInputs lhs, PhinsInputs rhs) {
  return static_cast<PhinsInputs>(static_cast<int>(lhs) | static_cast<int>(rhs));
}

/// operator to let us sensibly compare inputs
inline bool isSet(const PhinsInputs& lhs, const PhinsInputs rhs) {
  return static_cast<bool>(static_cast<int>(lhs) & static_cast<int>(rhs));
}

} // namespace PhinsIns

class PhinsInsController : public QWidget {
  Q_OBJECT
 public:
  explicit PhinsInsController(const std::string& name,
                              const std::string& reconfigure_path,
                              const PhinsIns::PhinsModel& model,
                              const PhinsIns::PhinsInputs& inputs,
                              QWidget* parent=0);

 signals:
  void newPhinsConfig(AnyMessage config);
  void resetPhins();

 public slots:
  void updateAges();
  void refresh();
  void updatePhinsbin(AnyMessage phinsbin);
  void updateConfig(AnyMessage config);
  void generateNewConfig(int);
  void updateGyroBias(AnyMessage fogest);
  void updateAccelBias(AnyMessage accest);

 protected:
  void setup(const PhinsIns::PhinsInputs& inputs);
  std::string name_;
  PhinsIns::PhinsModel model_;
  ds_sensor_msgs::PhinsStdbin3 msg;
  ds_sensor_msgs::PhinsConfig config_msg;
  geometry_msgs::Vector3Stamped fogest_msg;
  geometry_msgs::Vector3Stamped accest_msg;
  QDateTime last_gui_update_time;

  std::vector<PhinsInsExternalSensor*> external_sensors_;
  QHBoxLayout* layout_;
  QVBoxLayout* grid_layout_;
  QGroupBox* sensors_box_;
  QGridLayout* sensors_layout_;
  QVBoxLayout* info_layout_;

  IndicatorButtonGroup* zupt_modes_;
  IndicatorButtonGroup* altitude_modes_;
  QGroupBox* alignment_box_;
  QLabel* alignment_status_;
  QGroupBox* message_box_;
  QLabel* messages_;

  QGroupBox* bias_fog_box_;
  QLabel* bias_fog_field_;
  QGroupBox* bias_acc_box_;
  QLabel* bias_acc_field_;
  QPushButton* restart_but_;

  std::string makeMessages(const ds_sensor_msgs::PhinsStdbin3& msg, const PhinsIns::PhinsModel& MODEL);
  void trigger_restart_dialog();
};

} // namespace ds_rqt_widgets

#endif // DS_RQT_WIDGETS_PHINSINSCONTROLLER_H
