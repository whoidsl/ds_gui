//
// Created by ivaughn on 12/27/19.
//

#ifndef DS_RQT_WIDGETS_USTRAINTABLE_H_
#define DS_RQT_WIDGETS_USTRAINTABLE_H_

#include "SensorTableBase.h"
#include <ds_sensor_msgs/MemsImu.h>

namespace ds_rqt_widgets {

class UstrainTable : public SensorTableBase {
  Q_OBJECT
 public:
  explicit UstrainTable(const std::string& name, QWidget *parent=0);

 protected:
  void setup();

 public slots:
  void refresh();
  void updateUstrain(AnyMessage memsimu);

 private:
  ds_sensor_msgs::MemsImu msg;
};

} // namespace ds_rqt_widgets

#endif //DS_RQT_WIDGETS_USTRAINTABLE_H_
