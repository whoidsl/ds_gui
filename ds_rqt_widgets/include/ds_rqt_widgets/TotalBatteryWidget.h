//
// Created by ivaughn on 12/17/19.
//

#ifndef DS_RQT_WIDGETS_TOTALBATTERYWIDGET_H
#define DS_RQT_WIDGETS_TOTALBATTERYWIDGET_H

#include <QtGui>
#include <QWidget>
#include <QPainter>
#include <QColor>
#include <QLabel>
#include <QGridLayout>


#include <ds_hotel_msgs/BatMan.h>
#include "QtRosSubscription.h"
#include <QFrame>

namespace ds_rqt_widgets {

class ModuleIndicator : public QWidget {
 Q_OBJECT
 public:
  explicit ModuleIndicator(QWidget* parent=0);

  bool getFault() const;
  bool getDischarge() const;
  bool getCharge() const;

  void setFault(bool value);
  void setDischarge(bool value);
  void setCharge(bool value);

 protected:
  virtual void resizeEvent(QResizeEvent *event);
  virtual void paintEvent(QPaintEvent *event);
  QSize sizeHint() const;

 private:
  bool fault_;
  bool charge_;
  bool discharge_;
  void draw(QPainter& painter);
  const static int SIZE;
};

class ChargeIndicator : public QWidget {
  Q_OBJECT
 public:
  explicit ChargeIndicator(QWidget* parent=0);

  // charge percent is nominally between [0-100], although values
  // slightly outside that range may happen
  double getChargePercent() const;
  void setChargePercent(double pct);

 protected:
  virtual void resizeEvent(QResizeEvent *event);
  virtual void paintEvent(QPaintEvent *event);
  QSize sizeHint() const;

 private:
  double charge_percent_;
  void draw(QPainter& painter);
  const static int SIZE;
};

class TotalBatteryWidget : public QFrame {
  Q_OBJECT
 public:
  explicit TotalBatteryWidget(int num_bats, QWidget* parent=0);

 public slots:
  void updateBatMan(AnyMessage batman);
  void refresh();

 protected:
  void setup();

 private:
  int num_bats;

  ChargeIndicator* charge;
  std::vector<ModuleIndicator*> modules;
  QLabel* percent;
  QLabel* ampHours;
  QLabel* minCellTemp;
  QLabel* maxCellTemp;
  QGridLayout* layout;


};

} // namespace ds_rqt_widgets

#endif //DS_RQT_WIDGETS_TOTALBATTERYWIDGET_H
