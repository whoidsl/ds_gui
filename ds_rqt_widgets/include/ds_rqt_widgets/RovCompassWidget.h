//
// Created by ivaughn on 2/25/19.
//

#ifndef DS_RQT_WIDGETS_ROVCOMPASS2_H
#define DS_RQT_WIDGETS_ROVCOMPASS2_H

#include <QtGui>
#include <QWidget>
#include <QPainter>
#include <QColor>
#include <QPixmap>

namespace ds_rqt_widgets {

class RovCompassWidget : public QWidget {
  Q_OBJECT
 public:
  explicit RovCompassWidget(QWidget *parent = 0);

  void refresh();

  double getHeadingValue() const;
  double getHeadingSetpoint() const;

 public slots:
  void setHeadingValueRadians(double headingRad);
  void setHeadingSetpointRadians(double headingRad);
  void setHeadingValueDegrees(double headingDeg);
  void setHeadingSetpointDegrees(double headingDeg);

 protected:
  virtual void resizeEvent(QResizeEvent *event);
  virtual void paintEvent(QPaintEvent *event);
  QSize sizeHint() const;

 private:
  double heading;
  double headingSetPoint;
  QPixmap backgroundCache;

  void setupPainter(QPainter& painter, int width, int height);
  void drawBackground(QPainter& painter);
  void updateBackgroundCache();
  void drawValue(QPainter& painter);
  void drawSetPoint(QPainter& painter);
};

}

#endif // DS_RQT_WIDGETS_ROVCOMPASS2_H
