//
// Created by ivaughn on 12/28/19.
//

#ifndef DS_RQT_WIDGETS_UBLOXNAVTABLE_H_
#define DS_RQT_WIDGETS_UBLOXNAVTABLE_H_

#include "SensorTableBase.h"
#include <ds_sensor_msgs/UbloxNav.h>

namespace ds_rqt_widgets {

class UbloxNavTable : public SensorTableBase {
 Q_OBJECT
 public:
  explicit UbloxNavTable(const std::string &name, QWidget *parent = 0);

 protected:
  void setup();

 public slots:
  void refresh();
  void updateNav(AnyMessage ubxNav);

 private:
  ds_sensor_msgs::UbloxNav msg;
};

} // namespace ds_rqt_widgets

#endif //DS_RQT_WIDGETS_UBLOXNAVTABLE_H_
