//
// Created by ivaughn on 3/1/19.
//

#ifndef DS_RQT_WIDGETS_THRUSTERWIDGET_H
#define DS_RQT_WIDGETS_THRUSTERWIDGET_H


#include <QWidget>
#include <QPainter>
#include <QtGui>

namespace ds_rqt_widgets {

class ThrusterWidget : public QWidget {
  Q_OBJECT

 public:

  enum Orientation {
    POS_IS_RIGHT=0x01,
    POS_IS_LEFT=0x02,
    POS_IS_UP=0x04,
    POS_IS_DOWN=0x08,
  };

  explicit ThrusterWidget(QWidget* parent=0);
  explicit ThrusterWidget(Orientation orientation, QWidget* parent=0);

  double getMax() const;
  double getCenter() const;
  double getMin() const;
  double getActual() const;
  double getCommand() const;
  bool isPosPos() const;
  Orientation getOrientation() const;

  void setMax(double max);
  void setCenter(double center);
  void setMin(double min);
  void setPosIsPos(bool value);
  void setOrientation(Orientation orientation);

 public slots:
  void setActual(double actual);
  void setCommand(double command);
  void setThrusterEnabled(bool enabled);

 protected:
  virtual void resizeEvent(QResizeEvent *event);
  virtual void paintEvent(QPaintEvent *event);
  QSize sizeHint() const;


 private:
  double max_;
  double min_;
  double center_;
  // orientation of the widget
  Orientation orientation_;
  bool pos_is_pos_;

  double actual_;
  double command_;

  void draw(QPainter& painter);
  double compute_percent(double value);

};

}

#endif //DS_RQT_WIDGETS_THRUSTERWIDGET_H
