//
// Created by ivaughn on 12/28/19.
//

#ifndef DS_RQT_WIDGETS_UBLOXTAB_H_
#define DS_RQT_WIDGETS_UBLOXTAB_H_

#include <QGridLayout>
#include "QtRosSubscription.h"

#include "UBloxNavTable.h"
#include "UBloxSvInTable.h"
#include "UBloxSatTable.h"

namespace ds_rqt_widgets {

class UbloxTab : public QWidget {
  Q_OBJECT
 public:
  explicit UbloxTab(const std::string& name, QWidget* parent=0);

 public slots:
  void updateAges();
  void updateNav(AnyMessage ubxNav);
  void updateSvin(AnyMessage svin);
  void updateSats(AnyMessage sats);
  void updateSigs(AnyMessage sigs);

 private:
  void setup();

  std::string name_;
  UbloxNavTable* nav_tab_;
  UbloxSvInTable* svin_tab_;
  UbloxSatTable* sat_tab_;
  QGridLayout* layout_;
};

}

#endif //DS_RQT_WIDGETS_UBLOXTAB_H_
