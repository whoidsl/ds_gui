//
// Created by ivaughn on 10/7/20.
//

#ifndef DS_RQT_WIDGETS_TECHNADYNETHRUSTER_H
#define DS_RQT_WIDGETS_TECHNADYNETHRUSTER_H

#include <QtGui>
#include <QWidget>
#include <QLabel>
#include <QGridLayout>

#include <ds_actuator_msgs/Tecnadyne561.h>
#include "QtRosSubscription.h"

namespace ds_rqt_widgets {

class TecnadyneThrusterTable : public QFrame {
  Q_OBJECT

 protected:

  struct ThrusterEntry {
    QLabel* name;
    QLabel* prop_pwm;
    QLabel* tach_rpm;
    QLabel* tach_error;
    QLabel* speed_loop_interval;
    QLabel* voltage_volts;
    QLabel* current_amps;
    QLabel* fault_status;
    QLabel* reset_status;
    QLabel* msg_age;
  };

 public:
  explicit TecnadyneThrusterTable(const std::vector<std::string>& names, QWidget* parent=0);

 public slots:
  void tick();
  void updateThruster(AnyMessage thrust_msg, int thrusternum);

 protected:
  void setup();

 private:
  std::vector<std::string> thruster_names;
  std::vector<ds_actuator_msgs::Tecnadyne561> thruster_msgs;
  std::vector<QDateTime> msg_times;

  QGridLayout* layout;

  std::vector<ThrusterEntry> thrusters;
  QLabel* header_prop_pwm;
  QLabel* header_tach_rpm;
  QLabel* header_tach_error;
  QLabel* header_speed_loop_interval;
  QLabel* header_voltage_volts;
  QLabel* header_current_amps;
  QLabel* header_fault_status;
  QLabel* header_reset_status;
  QLabel* header_msg_age;

  void setupLabel(QLabel* lbl, bool header=false);
};

} // namespace ds_rqt_widgets

#endif //DS_RQT_WIDGETS_TECHNADYNETHRUSTER_H
