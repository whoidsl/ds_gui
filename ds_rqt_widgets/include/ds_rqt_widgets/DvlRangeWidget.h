//
// Created by ivaughn on 2/25/19.
//

#ifndef DS_RQT_WIDGETS_DVLRANGE_H
#define DS_RQT_WIDGETS_DVLRANGE_H

#include <QtGui>
#include <QWidget>
#include <QPainter>
#include <QColor>
#include <QMutex>
#include <QMutexLocker>

#include <qt_gui_cpp/settings.h>
#include <ds_sensor_msgs/Dvl.h>
#include "QtRosSubscription.h"

namespace ds_rqt_widgets {

class DvlRangeWidget : public QWidget {
  Q_OBJECT
 public:
  explicit DvlRangeWidget(QWidget *parent=0);

  void refresh();

  double getRangeFwdPort() const;
  double getRangeFwdStbd() const;
  double getRangeAftPort() const;
  double getRangeAftStbd() const;
  double getRangeCombined() const;
  double getMinRange() const;
  double getMaxRange() const;
  const std::string& getDvlFrameId() const;
  const std::string& getBodyFrameId() const;
  bool getTfOk() const;

  // ROS plugin management settings
  void saveSettings(const std::string& name,
      qt_gui_cpp::Settings &plugin_settings,
      qt_gui_cpp::Settings &instance_settings) const;

  void restoreSettings(const std::string& name,
      const qt_gui_cpp::Settings &plugin_settings,
      const qt_gui_cpp::Settings &instance_settings);

 public slots:
  void updateFromMessage(AnyMessage raw_msg);
  void setMinRange(double range);
  void setMaxRange(double range);
  // NOTE: DVL frame is set automatically by the incoming data packet, so you
  // can probably ignore this
  void setDvlFrameId(const std::string& frame_id);
  void setBodyFrameId(const std::string& frame_id);

  const std::vector<double>& getRanges() const;
  std::vector<double>& getRanges();

  const std::vector<double>& getRange2Alt() const;
  std::vector<double>& getRange2Alt();


 protected:
  virtual void resizeEvent(QResizeEvent *event);
  virtual void paintEvent(QPaintEvent *event);
  virtual void ShowContextMenu(const QPoint &pos);
  QSize sizeHint() const;

 private:
  std::vector<double> ranges;
  std::vector<double> range2alt;
  double range_combined;
  double min_range;
  double max_range;
  bool tf_ok;
  std::string dvl_frame_id;
  std::string body_frame_id;

  double recalcCombined() const;
  void draw(QPainter& painter);

  void setupBeams();
  struct BeamIndicator {
    double angle_deg;
    double beamwidth_deg;

    int angle_sixteenths;
    int beamwidth_sixteenths;

    double center_edge_x, center_edge_y; // center of beamwidth

    BeamIndicator(double angle, double beamwidth);
  };

  void drawBeam(QPainter& painter,
      const DvlRangeWidget::BeamIndicator& beam, double range);
  std::vector<BeamIndicator> beams;

  void updateBeamAngles(const ds_sensor_msgs::DvlConstPtr& msg);
};

}
#endif //DS_RQT_WIDGETS_DVLRANGE_H
