//
// Created by ivaughn on 2/22/19.
//

#ifndef DS_RQT_WIDGETS_INDICATORLABEL_H
#define DS_RQT_WIDGETS_INDICATORLABEL_H

#include <QLabel>
#include <QColor>

namespace ds_rqt_widgets {


/// This class is for an indicator label with good, unknown, bad, and unavailable values.
/// It's like the indicator button, but for display rather than button things
class IndicatorLabel : public QLabel {
  Q_OBJECT

 public:
  enum Status {
    DISABLED=0,
    BAD=1,
    GOOD=2,
    WARN=3,
    ALERT=4,
    OFF=5,
    UNKNOWN=6,
  };

  struct ColorSpec {
    ColorSpec(QColor _text, QColor _background) {
      text = _text;
      background = _background;
    }

    QColor text;
    QColor background;
  };

  IndicatorLabel(QWidget* parent=0);
  IndicatorLabel(const QString& text, QWidget* parent=0);

  ColorSpec getColorForState(Status state) const;
  void setColorForState(Status state, ColorSpec color);

  Status getState() const;
  ColorSpec getColor() const;

 public slots:
  void setState(int state);

 private:
  Status current_state_;
  std::vector<ColorSpec> colors_;

  void init();

};



}

#endif //DS_RQT_WIDGETS_INDICATORBUTTON_H
