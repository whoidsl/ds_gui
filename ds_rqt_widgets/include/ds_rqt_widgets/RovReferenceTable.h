//
// Created by ivaughn on 3/1/19.
//

#ifndef DS_RQT_WIDGETS_ROVREFERENCETABLE_H
#define DS_RQT_WIDGETS_ROVREFERENCETABLE_H

#include <QWidget>
#include <QTableView>
#include <QAbstractTableModel>
#include <QtGui>
#include <QHBoxLayout>

#include <ds_nav_msgs/AggregatedState.h>
#include <geometry_msgs/WrenchStamped.h>
#include <ds_control_msgs/RovControllerState.h>

#include "QtRosSubscription.h"

namespace ds_rqt_widgets {

class RovReferenceTableModel : public QAbstractTableModel {
  Q_OBJECT

 public:
  explicit RovReferenceTableModel(QWidget* parent=nullptr);

  int rowCount(const QModelIndex &parent = QModelIndex()) const override;
  int columnCount(const QModelIndex &parent = QModelIndex()) const override;
  QVariant data(const QModelIndex&index, int role=Qt::DisplayRole) const override;
  QVariant headerData(int section, Qt::Orientation orientation, int role) const override;

  const ds_nav_msgs::AggregatedState& getNavagg() const;
  const ds_nav_msgs::AggregatedState& getReference() const;
  const ds_nav_msgs::AggregatedState& getGoal() const;
  const geometry_msgs::WrenchStamped& getWrench() const;

 public:
  void updateNavigation(AnyMessage raw_msg);
  void updateReference(AnyMessage raw_msg);
  void updateGoal(AnyMessage raw_msg);
  void updateWrench(AnyMessage raw_msg);
  void updateControllerState(AnyMessage raw_msg);
  void updateToGo();

 protected:
  QVariant getState(int idx, const ds_control_msgs::RovControllerState& state) const;
  QVariant getDiff(int idx, const ds_nav_msgs::AggregatedState& start,
                            const ds_nav_msgs::AggregatedState& end) const;
  QVariant getPos(int idx, const ds_nav_msgs::AggregatedState& state) const;
  QVariant getVel(int idx, const ds_nav_msgs::AggregatedState& state) const;
  QVariant getForce(int idx, const geometry_msgs::WrenchStamped& wrench) const;

 private:
  ds_nav_msgs::AggregatedState navagg_;
  ds_nav_msgs::AggregatedState reference_;
  ds_nav_msgs::AggregatedState goal_;
  geometry_msgs::WrenchStamped wrench_;
  ds_control_msgs::RovControllerState state_;
};

class RovReferenceTable : public QWidget {
  Q_OBJECT

 public:
  explicit RovReferenceTable(QWidget* parent=0);

  const ds_nav_msgs::AggregatedState& getNavagg() const;
  const ds_nav_msgs::AggregatedState& getReference() const;
  const ds_nav_msgs::AggregatedState& getGoal() const;
  const geometry_msgs::WrenchStamped& getWrench() const;

 public slots:
  void updateNavigation(AnyMessage raw_msg);
  void updateReference(AnyMessage raw_msg);
  void updateGoal(AnyMessage raw_msg);
  void updateWrench(AnyMessage raw_msg);
  void updateControllerState(AnyMessage raw_msg);

 private:
  QHBoxLayout* layout_;
  QTableView* tableView_;
  RovReferenceTableModel* model_;

};

}
#endif //DS_RQT_WIDGETS_ROVREFERENCETABLE_H
