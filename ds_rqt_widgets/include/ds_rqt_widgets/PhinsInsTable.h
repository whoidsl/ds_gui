//
// Created by ivaughn on 6/14/21.
//

#ifndef DS_RQT_WIDGETS_PHINSINSTABLE_H
#define DS_RQT_WIDGETS_PHINSINSTABLE_H

#include <QWidget>
#include "SensorTableBase.h"
#include <ds_sensor_msgs/PhinsStdbin3.h>

namespace ds_rqt_widgets {

class PhinsInsTable : public SensorTableBase {
  Q_OBJECT
 public:
  explicit PhinsInsTable(const std::string& name, QWidget *parent=0);

 protected:
  void setup();

 public slots:
  void refresh();
  void updatePhinsbin(AnyMessage phinsbin);

 private:
  ds_sensor_msgs::PhinsStdbin3 msg;
};


} // namespace ds_rqt_widgets

#endif // DS_RQT_WIDGETS_PHINSINSTABLE_H
