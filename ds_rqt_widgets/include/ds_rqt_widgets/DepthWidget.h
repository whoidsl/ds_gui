//
// Created by ivaughn on 2/25/19.
//

#ifndef DS_RQT_WIDGETS_DEPTH_H
#define DS_RQT_WIDGETS_DEPTH_H

#include <QtGui>
#include <QWidget>
#include <QPainter>
#include <QColor>
#include <QMutex>
#include <QMutexLocker>

#include <qt_gui_cpp/settings.h>

namespace ds_rqt_widgets {

class DepthWidget : public QWidget {
  Q_OBJECT
 public:
  explicit DepthWidget(QWidget *parent=0);

  void refresh();

  double getDepth() const;
  double getDepthLimit() const;
  double getDepthSetpoint() const;
  double getAltitude() const;
  double getHeadroom() const;
  int getDigits() const;

  double getDepthRange() const;

  void saveSettings(const std::string& name,
    qt_gui_cpp::Settings &plugin_settings,
    qt_gui_cpp::Settings &instance_settings) const;
  void restoreSettings(const std::string& name,
    const qt_gui_cpp::Settings &plugin_settings,
    const qt_gui_cpp::Settings &instance_settings);

 public slots:
  void setDepth(double depth_m);
  void setDepthLimit(double max_depth_m);
  void setDepthSetpoint(double setpoint_m);
  void setAltitude(double alt_m);
  void setHeadroom(double head_m);
  void setDepthRange(double range_m);
  void setDigits(int depth_digits);

 protected:
  virtual void resizeEvent(QResizeEvent *event);
  virtual void paintEvent(QPaintEvent *event);
  virtual void ShowContextMenu(const QPoint& pos);

 private:
  double depth;
  double depth_limit;
  double depth_setpoint;
  double altitude;
  double headroom;
  int digits;

  double depth_range;

  void draw(QPainter& painter);

};
}

#endif //DS_RQT_WIDGETS_DEPTH_H
