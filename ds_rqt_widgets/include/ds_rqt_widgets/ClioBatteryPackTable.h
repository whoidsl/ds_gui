//
// Created by ivaughn on 12/17/19.
//

#ifndef DS_RQT_WIDGETS_CLIOBATTERYPACKTABLE_H
#define DS_RQT_WIDGETS_CLIOBATTERYPACKTABLE_H

#include <QtGui>
#include <QWidget>
#include <QLabel>
#include <QGridLayout>

#include <ds_hotel_msgs/ClioBattery.h>
#include <ds_hotel_msgs/ClioBatteryPack.h>
#include "QtRosSubscription.h"

namespace ds_rqt_widgets {

class ClioBatteryPackTable : public QWidget {
  Q_OBJECT

 protected:
  struct PackEntry {
    QLabel* pack_temps;
    QLabel* pack_min_cell_voltage;
    QLabel* pack_max_cell_voltage;
    QLabel* pack_voltage;
    QLabel* percentage;
    QLabel* status_code;
  };

  struct BatteryEntry {
    std::vector<PackEntry> packs;
    std::array<QLabel*, 6> module_name;
    QLabel* max_temp;
    QLabel* min_cell_voltage;
    QLabel* max_cell_voltage;
    QLabel* voltage;
    QLabel* percentage;
    QLabel* status_code;
  };

 public:
  explicit ClioBatteryPackTable(int num_bats, int num_packs, QWidget *parent=0);

 public slots:
  void updateBattery(AnyMessage clio_bat, int batnum);

 protected:
  void setup();

 private:
  int num_packs; // number of packs per battery module
  int num_bats; // number of battery modules installed

  QGridLayout* layout;
  std::vector<BatteryEntry> batteries;
  QLabel* header_temp;
  QLabel* header_percentage;
  QLabel* header_min_cell;
  QLabel* header_max_cell;
  QLabel* header_voltage;
  QLabel* header_status_code;

};

} // namespace ds_rqt_widgets

#endif //DS_RQT_WIDGETS_CLIOBATTERYPACKTABLE_H
