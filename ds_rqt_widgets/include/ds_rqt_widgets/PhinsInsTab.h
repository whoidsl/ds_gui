//
// Created by ivaughn on 6/14/21.
//

#ifndef DS_RQT_WIDGETS_PHINSINSTAB_H
#define DS_RQT_WIDGETS_PHINSINSTAB_H

#include <QGridLayout>
#include "QtRosSubscription.h"
#include <ros/ros.h>

#include "PhinsInsTable.h"
#include "PhinsInsController.h"

namespace ds_rqt_widgets {

class PhinsInsTab : public QWidget {
  Q_OBJECT
 public:
  explicit PhinsInsTab(const std::string& name,
                       const std::string& driver_path,
                       PhinsIns::PhinsModel model, PhinsIns::PhinsInputs inputs,
                       QWidget* parent=0);

  PhinsInsController* controller();
  PhinsInsTable* table();

 public slots:
  void updateAges();
  void refresh();
  void updatePhinsbin(AnyMessage phinsbin);
  void updateConfig(AnyMessage phinsconfig);
  void updateGyroBias(AnyMessage fogest);
  void updateAccelBias(AnyMessage accest);
  void resetPhins();

 private:
  void setup(const std::string& driver_path,
             PhinsIns::PhinsModel model, PhinsIns::PhinsInputs inputs);
  std::string name_;
  PhinsInsTable* table_;
  PhinsInsController* controller_;
  QGridLayout* layout_;

  ros::NodeHandle nh_;
  ros::ServiceClient reset_phins_srv_;

};

} // namespace ds_rqt_widgets

#endif // DS_RQT_WIDGETS_PHINSINSTABLE_H
