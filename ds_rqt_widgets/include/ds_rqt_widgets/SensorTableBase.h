//
// Created by ivaughn on 12/27/19.
//

#ifndef DS_RQT_WIDGETS_SENSORTABLEBASE_H_
#define DS_RQT_WIDGETS_SENSORTABLEBASE_H_

#include <QtGui>
#include <QWidget>
#include <QFrame>
#include <QLabel>
#include <QGridLayout>

#include "QtRosSubscription.h"

namespace ds_rqt_widgets {

class SensorTableBase : public QFrame {
  Q_OBJECT
 public:
  explicit SensorTableBase(int age_field_idx, std::string name, QWidget *parent=0);


  // You'll need to add a slot here to update based
  // on sensor data.  Preferably using AnyMessage

 public slots:
  void updateAges();

 public:
  const std::string& name() const;

 protected:
  void setup(const std::vector<std::string>& row_labels);
  bool checkRefresh();
  void updateMsgTime();

  QGridLayout* layout;
  std::vector<QLabel*> fields;
  std::vector<QLabel*> headers;
  std::string name_;
  qint64 msg_interval;
  QDateTime msg_time;
  QDateTime last_gui_update_time;

 private:
  int age_field_idx_;
};
} // namespace ds_rqt_widgets

#endif //DS_RQT_WIDGETS_SENSORTABLEBASE_H_
