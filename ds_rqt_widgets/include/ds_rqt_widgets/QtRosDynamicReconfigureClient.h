//
// Created by ivaughn on 6/14/21.
//

#ifndef PROJECT_QTROSDYNAMICRECONFIGURECLIENT_H
#define PROJECT_QTROSDYNAMICRECONFIGURECLIENT_H

#include <boost/shared_ptr.hpp>
#include <boost/any.hpp>
#include <boost/function.hpp>

#include "QtRosSubscription.h"

#include <dynamic_reconfigure/client.h>

/// This module presents a DynamicReconfigure client as a QT signal generator
/// / slot acceptor.  It uses the same type erasure tricks as QtRosSubscription
/// to get around the inability to use templates

namespace ds_rqt_widgets {

/// Abstract base class for all subscriptions
class GenericDynamicReconfigureClient {

 public:
  GenericDynamicReconfigureClient(const std::string& _name);
  virtual ~GenericDynamicReconfigureClient() = 0;

  void setCallback(const boost::function<void(AnyMessage)> &cb);

  virtual void shutdown() = 0;
  virtual std::string getTopicName() const = 0;
  virtual void setTopicName(ros::NodeHandle& nh, const std::string& topicname) = 0;
  const std::string& getName() const;

  virtual bool setConfiguration(const AnyMessage& msg) = 0;

 protected:
  boost::function<void(AnyMessage)> callback;
  std::string name;
};

/// This is a templated subclass for subscriptions.
/// \tparam T The type of the message to subscribe to.  This MUST be JUST the
/// message type-- no const, pointers, *, shared_ptrs, etc.
template<typename T>
class DynamicReconfigureClient : public GenericDynamicReconfigureClient {
 public:
  DynamicReconfigureClient(ros::NodeHandle &nh, const std::string& name,
                           const std::string& topic_name) : GenericDynamicReconfigureClient(name) {

    ROS_INFO_STREAM("Connecting dynamic reconfigure to " <<topic_name);
    client.reset(new dynamic_reconfigure::Client<T>(topic_name,
                                                          boost::bind(&DynamicReconfigureClient<T>::config_callback, this, _1)));
    // do ntohing else
  }

  void config_callback(const T& cfg) {
    if (callback) {
      // the AnyMessage type always contains a boost::shared_ptr of the type, not the type itself.
      // Because the API of the dynamic_reconfig client is kind of limited, we therefore have to create
      // a copy wrapped by a shared_ptr.  Annoying, but it works.
      auto cfg_ptr = boost::shared_ptr<T const>(new T(cfg));
      callback(AnyMessage(cfg_ptr));
    } else {
      ROS_DEBUG("No callback specified for QtDynamicReconfigureClient");
    }
  }

  bool setConfiguration(const AnyMessage& msg) override {
    T new_config = *(msg.as<T>());
    return client->setConfiguration(new_config);
  }

  std::string getTopicName() const {
    if (client) {
      return client->getName();
    }
    return "";
  }

  void shutdown() override {
    client.reset();
  }

  void setTopicName(ros::NodeHandle& nh, const std::string& topicname) override {
    ROS_INFO_STREAM("Connecting dynamic reconfigure to " <<topicname);
    client.reset(new dynamic_reconfigure::Client<T>(topicname,
                                                    boost::bind(&DynamicReconfigureClient<T>::config_callback, this, _1)));
  }

 private:
  boost::shared_ptr<dynamic_reconfigure::Client<T> >  client;
};

/// This is our QT object that has a public slot that fires every time we get a message
class QtRosDynamicReconfigureClient : public QObject {
  Q_OBJECT
  Q_DISABLE_COPY(QtRosDynamicReconfigureClient);
 public:
  QtRosDynamicReconfigureClient(GenericDynamicReconfigureClient* s);
  virtual ~QtRosDynamicReconfigureClient();

  std::string getName() const;
  std::string getTopicName() const;
  void setTopicName(ros::NodeHandle& nh, const std::string& topicname);

  void shutdown();

 signals:
  /// This signal is generated whenever a new config is received.
  /// HOWEVER!  You must use a Qt::QueuedConnection, same as with the QtRosSubscription
  void received(AnyMessage msg);

 public slots:
  bool setConfiguration(AnyMessage msg);


 private:
  GenericDynamicReconfigureClient* client;
  void on_data(AnyMessage msg);
};

//// This is how the new QtRosDynamicReconfigureClient gets made.
template <typename T>
QtRosDynamicReconfigureClient* CreateDynamicReconfigureClient(ros::NodeHandle& nh, const std::string& name,
                                                              const std::string& topicname) {
  qRegisterMetaType<AnyMessage>();
  return new QtRosDynamicReconfigureClient(new DynamicReconfigureClient<T>(nh, name, topicname));
}

} // namespace ds_rqt_widgets {

#endif //PROJECT_QTROSDYNAMICRECONFIGURECLIENT_H
