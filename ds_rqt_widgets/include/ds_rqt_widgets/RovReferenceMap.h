//
// Created by ivaughn on 3/1/19.
//

#ifndef DS_RQT_WIDGETS_ROVREFERENCEMAP_H
#define DS_RQT_WIDGETS_ROVREFERENCEMAP_H

#include <QWidget>
#include <QPainter>

namespace ds_rqt_widgets {

class RovReferenceMap : public QWidget {
  Q_OBJECT

 public:
  explicit RovReferenceMap(QWidget* parent=0);

};


}

#endif //DS_RQT_WIDGETS_ROVREFERENCEMAP_H
