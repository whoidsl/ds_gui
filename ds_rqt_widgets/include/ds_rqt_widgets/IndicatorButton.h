//
// Created by ivaughn on 2/22/19.
//

#ifndef DS_RQT_WIDGETS_INDICATORBUTTON_H
#define DS_RQT_WIDGETS_INDICATORBUTTON_H

#include <QPushButton>
#include <QColor>

namespace ds_rqt_widgets {


/// This class is for an indicator button with good, unknown, bad, and unavailable values
class IndicatorButton : public QPushButton {
  Q_OBJECT

 public:
  enum Status {
    DISABLED=0,
    BAD=1,
    GOOD=2,
    WARN=3,
    ALERT=4,
    OFF=5,
    UNKNOWN=6,
  };

  struct ColorSpec {
    ColorSpec(QColor _text, QColor _background) {
      text = _text;
      background = _background;
    }

    QColor text;
    QColor background;
  };

  IndicatorButton(QWidget* parent=0);
  IndicatorButton(const QString& text, QWidget* parent=0);
  IndicatorButton(const QIcon& icon, const QString& text, QWidget* parent=0);

  ColorSpec getColorForState(Status state) const;
  void setColorForState(Status state, ColorSpec color);

  Status getState() const;
  ColorSpec getColor() const;

 public slots:
  void setState(int state);

 private:
  Status current_state_;
  std::vector<ColorSpec> colors_;

  void init();

};



}

#endif //DS_RQT_WIDGETS_INDICATORBUTTON_H
