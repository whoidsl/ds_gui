//
// Created by ivaughn on 11/4/20.
//

#ifndef DS_RQT_WIDGETS_INDICATOR_BUTTON_GROUP_H
#define DS_RQT_WIDGETS_INDICATOR_BUTTON_GROUP_H

#include "IndicatorButton.h"
#include <QGridLayout>
#include <QGroupBox>
#include <functional>

namespace ds_rqt_widgets {

class IndicatorButtonGroup : public QGroupBox {
  Q_OBJECT

 public:
  IndicatorButtonGroup(const std::string& long_name, const std::string& short_name,
                       int cols=2, QWidget* parent=NULL);

  void addButton(const std::string& name, int enumval);
  void addButton(const std::string& name, int enumval, const QString& tipText);

  void updateMode(int value);
  void setUnknown();
  int getValue() const;

 signals:
  void button_selected(int value);

 protected:
  int current_value;
  std::string groupName;
  std::vector<std::pair<int, IndicatorButton*>> buttons;
  QGridLayout* layout;
  int columns;
  void send_signal(int enumval);
};

} // namespace ds_rqt_widgets

#endif //DS_RQT_WIDGETS_INDICATOR_BUTTON_GROUP_H
