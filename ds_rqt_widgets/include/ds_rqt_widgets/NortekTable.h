//
// Created by ivaughn on 12/27/19.
//

#ifndef DS_RQT_WIDGETS_NORTEKTABLE_H_
#define DS_RQT_WIDGETS_NORTEKTABLE_H_

#include "SensorTableBase.h"
#include <ds_sensor_msgs/NortekDF21.h>

namespace ds_rqt_widgets {

class NortekTable : public SensorTableBase {
  Q_OBJECT
 public:
  explicit NortekTable(const std::string& name, QWidget *parent=0);

 protected:
  void setup();

 public slots:
  void refresh();
  void updateNortek(AnyMessage df21);

 private:
  ds_sensor_msgs::NortekDF21 msg;
};

} // namespace ds_rqt_widgets

#endif //DS_RQT_WIDGETS_NORTEKTABLE_H_
