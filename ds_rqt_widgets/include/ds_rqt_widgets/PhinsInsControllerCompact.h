//
// Created by ssuman on 4/19/22 starting from PhinsInsController.
//

#ifndef DS_RQT_WIDGETS_PHINSINSCONTROLLERCOMPACT_H
#define DS_RQT_WIDGETS_PHINSINSCONTROLLERCOMPACT_H

#include <QWidget>
#include <QLabel>
#include <QGroupBox>
#include <QSlider>
#include <memory>
#include "QtRosSubscription.h"
#include <ds_sensor_msgs/PhinsStdbin3.h>
#include <ds_sensor_msgs/PhinsConfig.h>
#include <geometry_msgs/Vector3Stamped.h>

#include "IndicatorButton.h"
#include "IndicatorLabel.h"
#include "IndicatorButtonGroup.h"

// Include specific phins namespaces and sensor interfaces
#include "ds_rqt_widgets/PhinsInsController.h"

namespace ds_rqt_widgets {

namespace Ui {
class PhinsInsControllerCompact;
}

  class PhinsInsControllerCompact : public QWidget {
    Q_OBJECT
  public:
    explicit PhinsInsControllerCompact(const std::string& name,
				       const std::string& reconfigure_path,
               const bool& sensor_modes_enabled,
               const bool& zupt_modes_enabled,
               const bool& altitude_modes_enabled,
               const bool& dive_modes_enabled,
               const bool& startup_modes_enabled,
               const bool& usbl_modes_enabled,
               const bool& status_msgs_enabled,
				       const PhinsIns::PhinsModel& model,
				       const PhinsIns::PhinsInputs& inputs,
				       QWidget* parent=0);
    ~PhinsInsControllerCompact();

  signals:
    void newPhinsConfig(AnyMessage config);
    void resetPhins();
    void reinitPhins(int);
    void updateUsblMode(int);

  public slots:
    void updateAges();
    void refresh();
    void updatePhinsbin(AnyMessage phinsbin);
    void updateConfig(AnyMessage config);
    void generateNewConfig(int);
    void executeStartupModeCommand(int);
    void executeUsblModeCommand(int);
    void updateGyroBias(AnyMessage fogest);
    void updateAccelBias(AnyMessage accest);

  protected:
    void setup(const PhinsIns::PhinsInputs& inputs,
               const bool& sensor_modes_enabled,
               const bool& zupt_modes_enabled,
               const bool& altitude_modes_enabled,
               const bool& dive_modes_enabled,
               const bool& startup_modes_enabled,
               const bool& usbl_modes_enabled, 
               const bool& status_msgs_enabled);
    std::string name_;
    bool sensor_modes_enabled;
    bool zupt_modes_enabled;
    bool altitude_modes_enabled;
    bool dive_modes_enabled;
    bool startup_modes_enabled;
    bool usbl_modes_enabled;
    bool status_msgs_enabled;
    PhinsIns::PhinsModel model_;
    ds_sensor_msgs::PhinsStdbin3 msg;
    ds_sensor_msgs::PhinsConfig config_msg;
    geometry_msgs::Vector3Stamped fogest_msg;
    geometry_msgs::Vector3Stamped accest_msg;
    QDateTime last_gui_update_time;

    std::vector<PhinsInsExternalSensor*> external_sensors_;
    QGroupBox* sensors_box_;
    QGridLayout* sensors_layout_;
    //QHBoxLayout* layout_;

    QLabel* diveTextLbl_;
    QLabel* diveLbl_; // red if coarse align, yellow if fine align, green if aligned
    QLabel* fogBiasVecMagTextLbl_;
    QLabel* fogBiasVecMagLbl_; // Magnitude of the fog bias vector magnitude
    QLabel* accBiasVecMagTextLbl_;
    QLabel* accBiasVecMagLbl_; // Magnitude of the acc bias vector magnitude
    QLabel* velMagTextLbl_;
    QLabel* velMagLbl_; // Magnitude of the velocity - may indicate excessive drift issues
    QLabel* messagesTextLbl_;
    QLabel* messagesLbl_;

    QGroupBox* message_box_;
    QLabel* messages_;

    IndicatorButtonGroup* zupt_modes_;
    IndicatorButtonGroup* altitude_modes_;
    IndicatorButtonGroup* dive_modes_;
    IndicatorButtonGroup* startup_modes_;
    IndicatorButtonGroup* usbl_modes_;

    Ui::PhinsInsControllerCompact* ui;

    std::string makeMessages(const ds_sensor_msgs::PhinsStdbin3& msg, const PhinsIns::PhinsModel& MODEL);
    void trigger_subsea_reinit_dialog();
    void trigger_ondeck_reinit_dialog();
    void trigger_freerun_reinit_dialog();
  };

} // namespace ds_rqt_widgets

#endif // DS_RQT_WIDGETS_PHINSINSCONTROLLERCOMPACT_H
