//
// Created by ivandor on 5/1/20.
//

#ifndef DS_RQT_WIDGETS_POWERSWITCHWIDGET_H
#define DS_RQT_WIDGETS_POWERSWITCHWIDGET_H

#include <QFrame>
#include <QLabel>
#include <QPushButton>
#include <QMessageBox>
#include <QGroupBox>
#include <QPalette>
#include <QGridLayout>
#include <QWidget>
#include <ds_rqt_widgets/IndicatorButton.h>

namespace ds_rqt_widgets {

class PowerSwitchWidget : public QWidget {
  Q_OBJECT

  public:
    explicit PowerSwitchWidget(QWidget* parent=0);
    explicit PowerSwitchWidget(std::string name, QWidget* parent=0);
    QLabel* setupLabel(const std::string& text, const QFont& font);
    void setupWidget();
    std::vector<ds_rqt_widgets::IndicatorButton*> pwr_btns;
  
    void updateButton(std::string bName, bool confirm_status);
    QGroupBox* buttonGroup(const char* groupName, std::vector<std::string>(&devices_group), int columns=1);
 protected:
  ds_rqt_widgets::IndicatorButton* new_btn;
  QGridLayout* layout;

  std::string pwr_device;
  int max_width, num_devices;
  bool confirmOn;
};

}

#endif //DS_RQT_WIDGETS_POWERSWITCHWIDGET_H
