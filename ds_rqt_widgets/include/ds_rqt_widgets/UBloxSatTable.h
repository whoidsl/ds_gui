//
// Created by ivaughn on 12/28/19.
//

#ifndef DS_RQT_WIDGETS_UBLOXSATTABLE_H_
#define DS_RQT_WIDGETS_UBLOXSATTABLE_H_

#include <QWidget>
#include <QTableView>
#include <QAbstractTableModel>
#include <QtGui>
#include <QLabel>
#include <QGridLayout>

#include "QtRosSubscription.h"

#include <ds_sensor_msgs/UbloxSatellites.h>
#include <ds_sensor_msgs/UbloxSignals.h>

namespace ds_rqt_widgets {

class UbloxSatTable : public QFrame {
 protected:
  struct UbloxSatEntry {
    UbloxSatEntry();
    void updateSat(const ds_sensor_msgs::UbloxSatellite &_sat);
    void updateSig(const ds_sensor_msgs::UbloxSignal &_sig);

    bool matches(const ds_sensor_msgs::UbloxSatellite &_sat) const;
    bool matches(const ds_sensor_msgs::UbloxSignal &_sig) const;

    bool operator< (const UbloxSatEntry& other) const;

    int update_counter; // starts at zero.  Increments for every update, decrements every time a message comes in
    ds_sensor_msgs::UbloxSatellite sat;
    std::vector<ds_sensor_msgs::UbloxSignal> sig;
  };

  struct UbloxSatRow {
    UbloxSatRow(QWidget* parent=nullptr);
    void setHeaders();
    void refresh(const UbloxSatEntry& data);
    void clear();
    static const int width();

    void addToLayout(QGridLayout* layout, int row, int col);

    QLabel* name;
    QLabel* system;
    QLabel* elevation;
    QLabel* azimuth;
    QLabel* orbit;
    QLabel* sat_signals;
    QLabel* health;
    QLabel* quality;
    QLabel* pseudorange;
    QLabel* doppler;
    QLabel* carrier;

    std::vector<QLabel*> fields;
  };

  Q_OBJECT
 public:
  explicit UbloxSatTable(const std::string& name, QWidget *parent=0);

 protected:
  void setup();

 public slots:
  void refresh();
  void updateAges();
  void updateSats(AnyMessage sats);
  void updateSigs(AnyMessage sigs);

 private:
  std::string name_;
  QGridLayout* layout_;
  QGridLayout* entry_layout_;
  QLabel* title_;
  std::list<UbloxSatEntry> sats;
  std::vector<UbloxSatRow> headers;
  std::vector<UbloxSatRow> rows;
  std::vector<QLabel*> row_spaces;
  QDateTime msgtime_sats_;
  QDateTime msgtime_sigs_;
  QLabel* age_sats_;
  QLabel* age_sigs_;
  QLabel* age_sats_label_;
  QLabel* age_sigs_label_;
};

} // namespace ds_rqt_widgets
#endif //DS_RQT_WIDGETS_UBLOXSATTABLE_H_
