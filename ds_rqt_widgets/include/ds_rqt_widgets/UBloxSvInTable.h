//
// Created by ivaughn on 12/28/19.
//

#ifndef DS_RQT_WIDGETS_UBLOXSVINTABLE_H_
#define DS_RQT_WIDGETS_UBLOXSVINTABLE_H_

#include "SensorTableBase.h"
#include <ds_sensor_msgs/UbloxSurveyIn.h>

namespace ds_rqt_widgets {

class UbloxSvInTable : public SensorTableBase {
 Q_OBJECT
 public:
  explicit UbloxSvInTable(const std::string& name, QWidget *parent=0);

 protected:
  void setup();

 public slots:
  void refresh();
  void updateSvin(AnyMessage svin);

 private:
  ds_sensor_msgs::UbloxSurveyIn msg;
};

} // namespace ds_rqt_widgets

#endif //DS_RQT_WIDGETS_UBLOXSVINTABLE_H_
