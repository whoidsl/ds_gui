//
// Created by ivaughn on 2/25/19.
//

#include "ds_rqt_widgets/DepthWidget.h"

#define WIDTH 200
#define HEIGHT 400

#include <limits>
#include <ros/console.h>

#include <QtWidgets/QMenu>
#include <QtWidgets/QInputDialog>

namespace ds_rqt_widgets {

DepthWidget::DepthWidget(QWidget *parent) : QWidget(parent) {
  depth = 0;
  depth_range = 10;
  depth_setpoint = std::numeric_limits<double>::quiet_NaN();
  depth_limit = std::numeric_limits<double>::quiet_NaN();
  altitude = std::numeric_limits<double>::quiet_NaN();
  headroom = std::numeric_limits<double>::quiet_NaN();

  digits = 1;

  // make the options dialog pop up correctly
  this->setContextMenuPolicy(Qt::CustomContextMenu);
  connect(this, &QWidget::customContextMenuRequested,
      this, &DepthWidget::ShowContextMenu);
}

void DepthWidget::refresh() {
  update();
}

double DepthWidget::getDepth() const {
  return depth;
}

double DepthWidget::getDepthLimit() const {
  return depth_limit;
}

double DepthWidget::getAltitude() const {
  return altitude;
}

double DepthWidget::getHeadroom() const {
  return headroom;
}

double DepthWidget::getDepthRange() const {
  return depth_range;
}

int DepthWidget::getDigits() const {
  return digits;
}

void DepthWidget::saveSettings(const std::string& name,
    qt_gui_cpp::Settings &plugin_settings,
    qt_gui_cpp::Settings &instance_settings) const {

  instance_settings.setValue(QString::fromStdString(name + "_depth_range"), depth_range);
  instance_settings.setValue(QString::fromStdString(name + "_digits"), digits);
}

void DepthWidget::restoreSettings(const std::string& name,
    const qt_gui_cpp::Settings &plugin_settings,
    const qt_gui_cpp::Settings &instance_settings) {

  bool ok;
  double result;
  result = instance_settings.value(QString::fromStdString(name + "_depth_range"), depth_range).toDouble(&ok);
  if (ok) {
    depth_range = result;
  } else {
    ROS_ERROR("DepthWidget: Could not parse rqt_gui setting for depth range!");
  }
  int result_int;
  result_int = instance_settings.value(QString::fromStdString(name + "_digits"), depth_range).toInt(&ok);
  if (ok) {
    digits = result_int;
  } else {
    ROS_ERROR("DepthWidget: Could not parse rqt_gui setting for number of digits!");
  }


}

void DepthWidget::setDepth(double depth_m) {
  depth = depth_m;

  //lock.unlock();
  //update();
}

void DepthWidget::setDepthSetpoint(double setpoint_m) {
  depth_setpoint = setpoint_m;
  // don't update
}

void DepthWidget::setDepthLimit(double max_depth_m) {
  depth_limit = max_depth_m;
}

void DepthWidget::setAltitude(double alt_m) {
  altitude = alt_m;
}

void DepthWidget::setHeadroom(double head_m) {
  headroom = head_m;
}

void DepthWidget::setDepthRange(double range_m) {
  depth_range = range_m;
}

void DepthWidget::setDigits(int depth_digits) {
  if (depth_digits < 0 || depth_digits > 4) {
    ROS_ERROR_STREAM("Depth digits " <<depth_digits << " is outside acceptable range [0,4]");
    return;
  }

  digits = depth_digits;
}

void DepthWidget::resizeEvent(QResizeEvent *event) {
  QWidget::resizeEvent(event);
}

void DepthWidget::paintEvent(QPaintEvent *event) {
  QPainter painter(this);
  draw(painter);
  QWidget::paintEvent(event);
}

void DepthWidget::ShowContextMenu(const QPoint &pos) {

  QMenu contextMenu(tr("Widget Options"), this);

  QAction setDigitsOpt(tr("Set Display Digits"), this);
  connect(&setDigitsOpt, &QAction::triggered, [this]() {
    bool ok;
    int result = QInputDialog::getInt(this, tr("Set DepthWidget Property"),
        tr("Display Digits:"), digits, 0, 4, 1, &ok);
    if (ok) { this->setDigits(result); }
  });
  contextMenu.addAction(&setDigitsOpt);

  QAction setDepthRangeOpt(tr("Set Depth Range"), this);
  connect(&setDepthRangeOpt, &QAction::triggered, [this]() {
    bool ok;
    double result = QInputDialog::getDouble(this, tr("Set DepthWidget Property"),
        tr("Display Range:"), depth_range, 1, 12000, 0, &ok);
    if (ok) { this->setDepthRange(result); }
  });
  contextMenu.addAction(&setDepthRangeOpt);

  contextMenu.exec(mapToGlobal(pos));
}

void DepthWidget::draw(QPainter& painter) {

  // setup the basic stuff
  // we don't have to set the viewport because this can scale in X or Y
  // 0 is now 0-- conveniently near the surface!
  painter.setWindow(0, 0, width(), height());
  double depth_height = height()/8;

  // first, the backdrop
  QPen pen(Qt::white);
  pen.setWidth(1);
  painter.setRenderHint(QPainter::Antialiasing);
  painter.setRenderHint(QPainter::TextAntialiasing);
  painter.setPen(pen);
  painter.setBrush(Qt::black);

  painter.drawRect(0, 0, width(), height());

  // first, we have to pick the draw window

  // first nominal values
  double draw_depth_min = depth - depth_range/2.0;
  double draw_depth_max = depth + depth_range/2.0;
  double draw_depth_range = draw_depth_max - draw_depth_min;

  if (draw_depth_min <= -depth_range/10.0) {
    // in this case, we're near the surface; if that happens, use the full range, but
    // FORCE us near the surface.  If super-shallow, this can fill the screen with
    // seafloor-- but changing the range dynamically drives humans insane.
    // Which is worse.
    draw_depth_min = -depth_range/10.0;
    draw_depth_max = depth_range + draw_depth_min;
  }
  // if we're near the bottom, keep the vehicle at the center of the screen anyway.

  // draw water column
  double water_top = std::max(0.0, draw_depth_min);
  int water_top_px = round((water_top - draw_depth_min)/draw_depth_range*height());
  painter.setPen(Qt::lightGray);
  painter.setBrush(Qt::blue);
  painter.drawRect(0, water_top_px, width(), height()-water_top_px);

  // draw headroom
  if (std::isfinite(headroom)) {
    double headroom_bot = std::min(std::max(depth - headroom, draw_depth_min), draw_depth_max);
    int headroom_bot_px = round((headroom_bot - draw_depth_min)/draw_depth_range*height());

    painter.setPen(Qt::darkCyan);
    painter.setBrush(Qt::darkCyan);
    painter.drawRect(1, water_top_px+1, width()-2, headroom_bot_px - water_top_px - 1);
  }

  // draw seafloor
  if (std::isfinite(altitude)) {
    double seafloor_top = std::min(std::max(depth + altitude, draw_depth_min), draw_depth_max);
    int seafloor_top_px = round((seafloor_top - draw_depth_min)/draw_depth_range*height());

    painter.setPen(Qt::darkGreen);
    painter.setBrush(Qt::darkGreen);
    painter.drawRect(1, seafloor_top_px, width()-2, height()-seafloor_top_px-1);
  }

  // draw scale
  double interval;

  if (draw_depth_range <= 1) {
    interval = 0.1;
  } else if (draw_depth_range <= 10 + 1.0e-6) {
    interval = 1.0;
  } else if (draw_depth_range <= 100 + 1.0e-4) {
    interval = 10.0;
  } else if (draw_depth_range <= 250 + 1.0e-4) {
    interval = 25.0;
  } else if (draw_depth_range <= 500 + 1.0e-4) {
    interval = 50.0;
  } else if (draw_depth_range <= 1000 + 1.0e-4) {
    interval = 100.0;
  } else if (draw_depth_range <= 2500 + 1.0e-4) {
    interval = 250.0;
  } else if (draw_depth_range <= 5000 + 1.0e-4) {
    interval = 500.0;
  } else {
    interval = 1000.0;
  }

  // ok, for sanity we need some constants:
  const int SMALL_INCR = width()/16;

  // major landmark
  const int LEFT_EDGE = 1;
  const int VERT_LINE = width()*7/8;

  // scale are the numbers on the left
  const int SCALE_NUM_START = 0;
  const int SCALE_NUM_WIDTH = width()/2;

  const int TICK_START = SCALE_NUM_START + SCALE_NUM_WIDTH + SMALL_INCR;
  const int TICK_WIDTH = VERT_LINE - TICK_START;

  double tick_start = std::ceil(draw_depth_min / interval)*interval;
  double tick_end = std::floor(draw_depth_max / interval)*interval;

  pen = painter.pen();
  pen.setColor(Qt::lightGray);
  pen.setWidth(2);
  painter.setPen(pen);

  // draw the vertical line
  painter.drawLine(VERT_LINE, 1, VERT_LINE, height()-2);

  QFont font = painter.font();
  font.setBold(false);
  font.setPixelSize(depth_height/2);
  painter.setFont(font);

  // draw all the ticks
  double tick = tick_start;
  while (tick <= tick_end) {
    double y = (tick - draw_depth_min)/ draw_depth_range * height();
    painter.drawLine(TICK_START, y, VERT_LINE, y);

    if (tick >= 0 && fmod(tick, 2.0*interval) < 1.0) {
      painter.drawText(SCALE_NUM_START, y - depth_height/4, SCALE_NUM_WIDTH, depth_height/2,
          Qt::AlignVCenter | Qt::AlignRight,
                       QString::number(tick, 'f', 0));
    }

    tick += interval;
  }

  // draw our depth setpoint
  if (std::isfinite(depth_setpoint)
        && depth_setpoint > draw_depth_min
        && depth_setpoint < draw_depth_max) {
    int setpoint_px = round((depth_setpoint - draw_depth_min)/draw_depth_range*height());

    QPainterPath setpoint_path;
    setpoint_path.moveTo(VERT_LINE, setpoint_px);
    setpoint_path.lineTo(width()-1, setpoint_px-width()/8);
    setpoint_path.lineTo(width()-1, setpoint_px+width()/8);
    setpoint_path.closeSubpath();
    painter.setPen(Qt::lightGray);
    painter.setBrush(Qt::darkRed);
    painter.drawPath(setpoint_path);
  }

  // Actually draw our depth
  int depth_px = round((depth - draw_depth_min)/draw_depth_range*height());

  QPainterPath depth_path;
  depth_path.moveTo(VERT_LINE, depth_px);
  depth_path.lineTo(VERT_LINE - SMALL_INCR, depth_px+depth_height/2);
  depth_path.lineTo(LEFT_EDGE, depth_px+depth_height/2);
  depth_path.lineTo(LEFT_EDGE, depth_px-depth_height/2);
  depth_path.lineTo(VERT_LINE - SMALL_INCR, depth_px-depth_height/2);
  depth_path.closeSubpath();

  font = painter.font();
  font.setBold(true);
  font.setPixelSize(depth_height);
  painter.setFont(font);

  pen = painter.pen();
  pen.setColor(Qt::white);
  pen.setWidth(3);
  painter.setPen(pen);
  painter.setBrush(Qt::black);
  painter.drawPath(depth_path);
  painter.drawText(QRect(LEFT_EDGE, depth_px-depth_height/2,
      VERT_LINE - SMALL_INCR - LEFT_EDGE, depth_height),
    Qt::AlignRight | Qt::AlignVCenter, QString::number(depth, 'f', digits));
}


}

