//
// Created by ivaughn on 12/27/19.
//

#include "ds_rqt_widgets/NortekTable.h"

namespace Field {
const static size_t name = 0;

const static size_t vel_xyz = name + 2;
const static size_t fom_xyz = vel_xyz + 1;

const static size_t vel_beam = fom_xyz + 2;
const static size_t dist_beam = vel_beam + 1;
const static size_t fom_beam = dist_beam + 1;
const static size_t timediff_1 = fom_beam + 1;
const static size_t timediff_2 = timediff_1 + 1;

const static size_t temp = timediff_2 + 2;
const static size_t soundspeed = temp + 1;
const static size_t pressure = soundspeed + 1;
const static size_t error = pressure + 1;
const static size_t status = error + 1;

const static size_t tf_frame = status + 2;
const static size_t latency = tf_frame + 1;
const static size_t interval = latency + 1;
const static size_t age = interval + 1;
const static size_t NUM_ROWS = age+1;
}

namespace ds_rqt_widgets {

NortekTable::NortekTable(const std::string& name, QWidget *parent)
: SensorTableBase(Field::age, name, parent) {
  setup();
}

void NortekTable::updateNortek(AnyMessage df21) {
  msg = *(df21.as<ds_sensor_msgs::NortekDF21>());
  updateMsgTime();
  refresh();
}

void NortekTable::refresh() {
  if (!checkRefresh()) {
    return;
  }

  fields[Field::vel_xyz]->setText(QString("[%1, %2, %3, %4] m/s")
                                     .arg(msg.velX, 6, 'f', 3)
                                     .arg(msg.velY, 6, 'f', 3)
                                     .arg(msg.velZ1, 6, 'f', 3)
                                     .arg(msg.velZ2, 6, 'f', 3));

  fields[Field::fom_xyz]->setText(QString("[%1, %2, %3, %4]")
                                      .arg(msg.fomX, 6, 'f', 4)
                                      .arg(msg.fomY, 6, 'f', 4)
                                      .arg(msg.fomZ1, 6, 'f', 4)
                                      .arg(msg.fomZ2, 6, 'f', 4));

  fields[Field::vel_beam]->setText(QString("[%1, %2, %3, %4] m/s")
                                      .arg(msg.velBeam[0], 6, 'f', 3)
                                       .arg(msg.velBeam[1], 6, 'f', 3)
                                       .arg(msg.velBeam[2], 6, 'f', 3)
                                       .arg(msg.velBeam[3], 6, 'f', 3));

  fields[Field::dist_beam]->setText(QString("[%1, %2, %3, %4] m")
                                       .arg(msg.distBeam[0], 6, 'f', 3)
                                       .arg(msg.distBeam[1], 6, 'f', 3)
                                       .arg(msg.distBeam[2], 6, 'f', 3)
                                       .arg(msg.distBeam[3], 6, 'f', 3));

  fields[Field::fom_beam]->setText(QString("[%1, %2, %3, %4]")
                                        .arg(msg.fomBeam[0], 6, 'f', 4)
                                        .arg(msg.fomBeam[1], 6, 'f', 4)
                                        .arg(msg.fomBeam[2], 6, 'f', 4)
                                        .arg(msg.fomBeam[3], 6, 'f', 4));

  fields[Field::timediff_1]->setText(QString("[%1, %2, %3, %4] ms")
                                       .arg(1000.0*msg.timeDiff1Beam[0], 4, 'f', 0)
                                       .arg(1000.0*msg.timeDiff1Beam[1], 4, 'f', 0)
                                       .arg(1000.0*msg.timeDiff1Beam[2], 4, 'f', 0)
                                       .arg(1000.0*msg.timeDiff1Beam[3], 4, 'f', 0));

  fields[Field::timediff_2]->setText(QString("[%1, %2, %3, %4] ms")
                                         .arg(-1000.0*msg.timeDiff2Beam[0], 4, 'f', 0)
                                         .arg(-1000.0*msg.timeDiff2Beam[1], 4, 'f', 0)
                                         .arg(-1000.0*msg.timeDiff2Beam[2], 4, 'f', 0)
                                         .arg(-1000.0*msg.timeDiff2Beam[3], 4, 'f', 0));

  fields[Field::temp]->setText(QString("%1%2C").arg(msg.temperature, 5, 'f', 2).arg(QChar(0260)));
  fields[Field::soundspeed]->setText(QString("%1 m/s").arg(msg.speed_sound, 6, 'f', 2));
  fields[Field::pressure]->setText(QString("%1 dBar").arg(10.0*msg.pressure, 6, 'f', 2));
  fields[Field::error]->setText(QString("0x%1").arg(msg.error, 8, 16, QChar('0')));
  fields[Field::status]->setText(QString("0x%1").arg(msg.status, 8, 16, QChar('0')));

  fields[Field::tf_frame]->setText(QString::fromStdString(msg.header.frame_id));
  ros::Duration latency = msg.ds_header.io_time - msg.header.stamp;
  fields[Field::latency]->setText(QString("%1 ms").arg(latency.toSec()*1000.0, 6, 'f', 1));
  fields[Field::interval]->setText(QString("%1 ms").arg(msg_interval, 8));
}

void NortekTable::setup() {
  std::vector<std::string> row_labels(Field::NUM_ROWS, " ");

  row_labels[Field::vel_xyz] = "Vel";
  row_labels[Field::fom_xyz] = "FOM";

  row_labels[Field::vel_beam] = "Beam Vel";
  row_labels[Field::dist_beam] = "Beam Dist";
  row_labels[Field::fom_beam] = "Beam FOM";
  row_labels[Field::timediff_1] = "Trigger->Ping";
  row_labels[Field::timediff_2] = "Ping->Msg";

  row_labels[Field::temp] = "Temperature";
  row_labels[Field::soundspeed] = "Sound Vel.";
  row_labels[Field::pressure] = "Pressure";
  row_labels[Field::error] = "Error";
  row_labels[Field::status] = "Status";

  row_labels[Field::tf_frame] = "TF Frame";
  row_labels[Field::latency] = "Latency";
  row_labels[Field::interval] = "Interval";
  row_labels[Field::age] = "Message Age";
  SensorTableBase::setup(row_labels);

  // make the header and have it span both columns
  fields[Field::name]->setText(QString::fromStdString(name_));
  layout->addWidget(fields[Field::name], Field::name, 0, 1, 2);
  fields[Field::name]->setAlignment(Qt::AlignCenter);
}

} // namespace ds_rqt_widgets
