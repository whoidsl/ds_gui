//
// Created by ivaughn on 12/28/19.
//

#include "ds_rqt_widgets/UBloxSatTable.h"

namespace ds_rqt_widgets {

// ////////////////////////////////////////////////////////////////////////// //
// Helpers
// ////////////////////////////////////////////////////////////////////////// //
QString toOrbitString(uint8_t orbit_src) {
  switch(orbit_src) {
    case ds_sensor_msgs::UbloxSatellite::ORBIT_SOURCE_NONE:
      return QString::fromStdString("none");
    case ds_sensor_msgs::UbloxSatellite::ORBIT_SOURCE_EPHEMERIS:
      return QString::fromStdString("ephem");
    case ds_sensor_msgs::UbloxSatellite::ORBIT_SOURCE_ALMANAC:
      return QString::fromStdString("alman");
    case ds_sensor_msgs::UbloxSatellite::ORBIT_SOURCE_ASSISTNOW_OFFLINE:
      return QString::fromStdString("a.off");
    case ds_sensor_msgs::UbloxSatellite::ORBIT_SOURCE_ASSISTNOW_AUTONOMOUS:
      return QString::fromStdString("a.auto");
    case ds_sensor_msgs::UbloxSatellite::ORBIT_SOURCE_OTHER5:
      return QString::fromStdString("other5");
    case ds_sensor_msgs::UbloxSatellite::ORBIT_SOURCE_OTHER6:
      return QString::fromStdString("other6");
    case ds_sensor_msgs::UbloxSatellite::ORBIT_SOURCE_OTHER7:
      return QString::fromStdString("other7");
    default:
      return QString::fromStdString("unkwn");
  }
}

QString delayToString(qint64 msecs_to) {
  auto delay = QTime::fromMSecsSinceStartOfDay(msecs_to);

  if (delay.hour() > 0) {
    return delay.toString("hh:mm:ss.zzz");
  } else if (delay.minute() > 0) {
    return delay.toString("mm:ss.zzz");
  }
  return delay.toString("ss.zzz");
}

QString slashAppend(const QString& base, const QString& toAdd) {
  if (base.isEmpty()) {
    return toAdd;
  }
  return base + "/" + toAdd;
}

// ////////////////////////////////////////////////////////////////////////// //
// UbloxSatTable::SatEntry
// ////////////////////////////////////////////////////////////////////////// //

UbloxSatTable::UbloxSatEntry::UbloxSatEntry() : update_counter(0) {}

void UbloxSatTable::UbloxSatEntry::updateSat(const ds_sensor_msgs::UbloxSatellite &_sat) {
  sat = _sat;
  update_counter = 15; // entry removed after this many missed entries
}

void UbloxSatTable::UbloxSatEntry::updateSig(const ds_sensor_msgs::UbloxSignal &_sig) {
  if (!this->matches(_sig)) {
    return;
  }

  for (auto& s : sig) {
    if (s.sig_id == _sig.sig_id) {
      // we found it! overwrite...
      s = _sig;
      return;
    }
  }

  // matches this satellite, but doesn't match any known signals.  Add it!
  sig.push_back(_sig);
}
bool UbloxSatTable::UbloxSatEntry::matches(const ds_sensor_msgs::UbloxSatellite &_sat) const {
  return (sat.sat_id == _sat.sat_id && sat.gnss_id == _sat.gnss_id);
}

bool UbloxSatTable::UbloxSatEntry::matches(const ds_sensor_msgs::UbloxSignal &_sig) const {
  return (sat.sat_id == _sig.sat_id && sat.gnss_id == _sig.gnss_id);
}

bool UbloxSatTable::UbloxSatEntry::operator<(const UbloxSatTable::UbloxSatEntry& other) const {
  // same service, compare based on number
  if (sat.sat_id[0] == other.sat.sat_id[0]) {
    std::string my_id = sat.sat_id.substr(1);
    std::string other_id = other.sat.sat_id.substr(1);
    return my_id < other_id;
  }

  // Different services.  Enforce the order G<E<B<R
  if (sat.sat_id[0] == 'G') {
    return true;
  }

  if (sat.sat_id[0] == 'E') {
    return other.sat.sat_id[0] != 'G';
  }

  if (sat.sat_id[0] == 'B') {
    return other.sat.sat_id[0] != 'G' && other.sat.sat_id[0] != 'E';
  }

  if (sat.sat_id[0] == 'R') {
    return other.sat.sat_id[0] != 'G'
        && other.sat.sat_id[0] != 'E'
        && other.sat.sat_id[0] != 'B';
  }

  // We dont' appear to belong to one of the 4 big services.  Check the other entry...
  if (other.sat.sat_id[0] == 'G'
        || other.sat.sat_id[0] == 'E'
        || other.sat.sat_id[0] == 'B'
      || other.sat.sat_id[0] == 'R') {
    // AH, It DOES belong to a known service, so give it priority.
    return true;
  }

  // ok, neither we nor the other element belong to one of the 4 primary constellations.  Just sort alphabetically.
  return sat.sat_id[0] < other.sat.sat_id[0];
}

// ////////////////////////////////////////////////////////////////////////// //
// UbloxSatTable::SatEntry
// ////////////////////////////////////////////////////////////////////////// //
UbloxSatTable::UbloxSatRow::UbloxSatRow(QWidget* parent) {
  name = new QLabel(tr(" "), parent);
  system = new QLabel(tr(" "), parent);
  elevation = new QLabel(tr(" "), parent);
  azimuth = new QLabel(tr(" "), parent);
  orbit = new QLabel(tr(" "), parent);
  sat_signals = new QLabel(tr(" "), parent);
  health = new QLabel(tr(" "), parent);
  quality = new QLabel(tr(" "), parent);
  pseudorange = new QLabel(tr(" "), parent);
  doppler = new QLabel(tr(" "), parent);
  carrier = new QLabel(tr(" "), parent);

  fields.push_back(name);
  fields.push_back(system);
  fields.push_back(elevation);
  fields.push_back(azimuth);
  fields.push_back(orbit);
  fields.push_back(sat_signals);
  fields.push_back(health);
  fields.push_back(quality);
  fields.push_back(pseudorange);
  fields.push_back(doppler);
  fields.push_back(carrier);

  QFont fieldFont(QFontDatabase::systemFont(QFontDatabase::FixedFont));
  fieldFont.setPointSize((4*fieldFont.pointSize())/5);
  for (auto& f : fields) {
    f->setFont(fieldFont);
    f->setAlignment(Qt::AlignVCenter | Qt::AlignHCenter);
    f->setContentsMargins(5, 0, 5, 0);
  }
}

void UbloxSatTable::UbloxSatRow::setHeaders() {
  name->setText(QString::fromStdString("ID"));
  system->setText(QString::fromStdString("System"));
  elevation->setText(QString::fromStdString("Elv"));
  azimuth->setText(QString::fromStdString("Az"));
  orbit->setText(QString::fromStdString("Orbit"));
  sat_signals->setText(QString::fromStdString("Signals"));
  health->setText(QString::fromStdString("Health"));
  quality->setText(QString::fromStdString("Quality"));
  pseudorange->setText(QString::fromStdString("PRng"));
  doppler->setText(QString::fromStdString("Dop"));
  carrier->setText(QString::fromStdString("Car"));
}

QString toHealthChar(uint16_t health) {
  switch(health) {
    case ds_sensor_msgs::UbloxSignal::HEALTH_HEALTHY:
      return QString::fromStdString("H");
    case ds_sensor_msgs::UbloxSignal::HEALTH_UNHEALTH:
      return QString::fromStdString("U");
    case ds_sensor_msgs::UbloxSignal::HEALTH_UNKNOWN:
    default:
      return QString::fromStdString("?");
  }
}
QString toQualityChar(uint16_t quality) {
  switch (quality) {
    case ds_sensor_msgs::UbloxSignal::QUALITY_NOSIGNAL:
      return QString::fromStdString("-");
    case ds_sensor_msgs::UbloxSignal::QUALITY_SEARCHING:
      return QString::fromStdString("S");
    case ds_sensor_msgs::UbloxSignal::QUALITY_ACQUIRED:
      return QString::fromStdString("A");
    case ds_sensor_msgs::UbloxSignal::QUALITY_UNUSABLE:
      return QString::fromStdString("U");
    case ds_sensor_msgs::UbloxSignal::QUALITY_CODELOCKED:
      return QString::fromStdString("l");
    case ds_sensor_msgs::UbloxSignal::QUALITY_CODECARRIERTIME_LOCKED_5:
    case ds_sensor_msgs::UbloxSignal::QUALITY_CODECARRIERTIME_LOCKED_6:
    case ds_sensor_msgs::UbloxSignal::QUALITY_CODECARRIERTIME_LOCKED_7:
      return QString::fromStdString("L");
    default:
      return QString::fromStdString("?");
  }
}

void UbloxSatTable::UbloxSatRow::refresh(const UbloxSatEntry& data) {
  name->setText(QString::fromStdString(data.sat.sat_id));
  system->setText(QString::fromStdString(data.sat.gnss_id));
  elevation->setText(QString("%1%2").arg(data.sat.elevation, 3, 'f', 0).arg(QChar(0260)));
  azimuth->setText(QString("%1%2").arg(data.sat.azimuth, 3, 'f', 0).arg(QChar(0260)));
  orbit->setText(toOrbitString(data.sat.oribt_source));

  // these get updated for each signal
  QString s_satsig, s_health, s_quality, s_pseudorange, s_doppler, s_carrier;

  for (const auto& sig : data.sig) {
    // clean up the sigid string to present better
    std::string sigid_str = sig.sig_id;
    auto end_pos = std::remove(sigid_str.begin(), sigid_str.end(), ' ');
    sigid_str.erase(end_pos, sigid_str.end());
    end_pos = std::remove(sigid_str.begin(), sigid_str.end(), '/');
    sigid_str.erase(end_pos, sigid_str.end());

    s_satsig = slashAppend(s_satsig, QString::fromStdString(sigid_str));
    s_health = slashAppend(s_health, toHealthChar(sig.health));
    s_quality = slashAppend(s_quality, toQualityChar(sig.quality_indicator));

    if (sig.pseudorange_corrections_applied) {
      s_pseudorange = slashAppend(s_pseudorange, "C");
    } else if (sig.pseudorange_used) {
      s_pseudorange = slashAppend(s_pseudorange, "O");
    } else {
      s_pseudorange = slashAppend(s_pseudorange, "x");
    }

    if (sig.doppler_corrections_applied) {
      s_doppler = slashAppend(s_doppler, "C");
    } else if (sig.doppler_corrections_applied) {
      s_doppler = slashAppend(s_doppler, "O");
    } else {
      s_doppler = slashAppend(s_doppler, "x");
    }

    if (sig.carrier_range_corrections_applied) {
      s_carrier = slashAppend(s_carrier, "C");
    } else if (sig.carrier_range_corrections_applied) {
      s_carrier = slashAppend(s_carrier, "O");
    } else {
      s_carrier = slashAppend(s_carrier, "x");
    }
  }

  sat_signals->setText(s_satsig);
  health->setText(s_health);
  quality->setText(s_quality);
  pseudorange->setText(s_pseudorange);
  doppler->setText(s_doppler);
  carrier->setText(s_carrier);
}

void UbloxSatTable::UbloxSatRow::clear() {
  name->setText(QString::fromStdString(""));
  system->setText(QString::fromStdString(""));
  elevation->setText(QString::fromStdString(""));
  azimuth->setText(QString::fromStdString(""));
  orbit->setText(QString::fromStdString(""));
  sat_signals->setText(QString::fromStdString(""));
  health->setText(QString::fromStdString(""));
  quality->setText(QString::fromStdString(""));
  pseudorange->setText(QString::fromStdString(""));
  doppler->setText(QString::fromStdString(""));
  carrier->setText(QString::fromStdString(""));
}

const int UbloxSatTable::UbloxSatRow::width() {
  return 11;
}

void UbloxSatTable::UbloxSatRow::addToLayout(QGridLayout *layout, int row, int col) {
  layout->addWidget(name, row, col++);
  layout->addWidget(system, row, col++);
  layout->addWidget(elevation, row, col++);
  layout->addWidget(azimuth, row, col++);
  layout->addWidget(orbit, row, col++);
  layout->addWidget(sat_signals, row, col++);
  layout->addWidget(health, row, col++);
  layout->addWidget(quality, row, col++);
  layout->addWidget(pseudorange, row, col++);
  layout->addWidget(doppler, row, col++);
  layout->addWidget(carrier, row, col++);
}

// ////////////////////////////////////////////////////////////////////////// //
// UbloxSatTable
// ////////////////////////////////////////////////////////////////////////// //

UbloxSatTable::UbloxSatTable(const std::string& name, QWidget *parent)
: QFrame(parent), name_(name) {
  setup();
}

void UbloxSatTable::updateSats(AnyMessage sats_msg) {
  auto new_sats = sats_msg.as<ds_sensor_msgs::UbloxSatellites>();
  msgtime_sats_ = QDateTime::currentDateTimeUtc();

  // start by going through and updating any existing satellites
  for (const auto& new_sat : new_sats->sats) {
    bool found=false;
    for (auto& old_sat : sats) {
      if (old_sat.matches(new_sat)) {
        old_sat.updateSat(new_sat);
        found=true;
        break;
      }
    }
    if (!found) {
      // didn't find it! Add new...
      sats.push_back(UbloxSatEntry());
      sats.back().updateSat(new_sat);
    }
  }

  // go through all existing satellite records and remove any old ones
  auto iter = sats.begin();
  while (iter != sats.end()) {
    iter->update_counter--;
    if (iter->update_counter < 0) {
      // we've missed enough updates; remove this one and move on
      std::cout <<"Removing record for satellite " <<iter->sat.sat_id <<std::endl;
      iter = sats.erase(iter);
    } else {
      ++iter;
    }
  }

  // sort using custom operator
  sats.sort();

  // update the screen
  refresh();
}

void UbloxSatTable::updateSigs(AnyMessage sigs) {
  auto new_sigs = sigs.as<ds_sensor_msgs::UbloxSignals>();
  msgtime_sigs_ = QDateTime::currentDateTimeUtc();

  for (const auto& new_sig : new_sigs->sat_signals) {
    for (auto& old_sat : sats) {
      if (old_sat.matches(new_sig)) {
        old_sat.updateSig(new_sig);
        break;
      }
    }
  }
  refresh();
}

void UbloxSatTable::refresh() {
  auto sat_iter = sats.begin();
  for (size_t i=0; i<rows.size(); i++) {
    if (sat_iter == sats.end()) {
      // we're out of satellites
      rows[i].clear();
    } else {
      rows[i].refresh(*sat_iter);
      sat_iter++;
    }
  }
}

void UbloxSatTable::updateAges() {
  auto now = QDateTime::currentDateTimeUtc();

  if (msgtime_sats_.isNull()) {
    age_sats_->setText(tr("Never"));
  } else {
    age_sats_->setText(delayToString(msgtime_sats_.msecsTo(now)));
  }

  if (msgtime_sigs_.isNull()) {
    age_sigs_->setText(tr("Never"));
  } else {
    age_sigs_->setText(delayToString(msgtime_sigs_.msecsTo(now)));
  }
}

void UbloxSatTable::setup() {

  const QFont labelFont = QFontDatabase::systemFont(QFontDatabase::FixedFont);
  title_ = new QLabel(QString::fromStdString(name_), this);
  title_->setAlignment(Qt::AlignVCenter | Qt::AlignHCenter);
  title_->setFont(labelFont);

  // pick the number of columns here
  entry_layout_ = new QGridLayout();

  // we'll start with a fixed number of rows
  for (size_t c=0; c<2; c++) {
    headers.push_back(UbloxSatRow(this));
    headers.back().setHeaders();
    headers.back().addToLayout(entry_layout_, 0, c*(UbloxSatRow::width()+1));
    for (size_t r=0; r<24; r++) {
      rows.push_back(UbloxSatRow(this));
      rows.back().addToLayout(entry_layout_, r+1, c*(UbloxSatRow::width()+1));
    }

    row_spaces.push_back(new QLabel(tr("    "), this));
    entry_layout_->addWidget(row_spaces.back(), 0, (c+1)*(UbloxSatRow::width()+c));
  }
  // delete the last spacer
  entry_layout_->removeWidget(row_spaces.back());
  delete row_spaces.back();
  row_spaces.pop_back();

  age_sats_label_ = new QLabel(tr("Sat Age:"), this);
  age_sats_label_->setAlignment(Qt::AlignVCenter | Qt::AlignRight);
  age_sats_label_->setFont(labelFont);
  age_sats_ = new QLabel(tr("Never"), this);
  age_sats_->setAlignment(Qt::AlignVCenter | Qt::AlignLeft);
  age_sats_->setFont(labelFont);

  age_sigs_label_ = new QLabel(tr("Sig Age:"), this);
  age_sigs_label_->setAlignment(Qt::AlignVCenter | Qt::AlignRight);
  age_sigs_label_->setFont(labelFont);
  age_sigs_ = new QLabel(tr("Never"), this);
  age_sigs_->setAlignment(Qt::AlignVCenter | Qt::AlignLeft);
  age_sigs_->setFont(labelFont);

  layout_ = new QGridLayout();
  layout_->addWidget(title_, 0, 0, 1, 4);
  layout_->addLayout(entry_layout_, 1, 0, 1, 4);
  layout_->addWidget(age_sats_label_, 2, 0);
  layout_->addWidget(age_sats_, 2, 1);
  layout_->addWidget(age_sigs_label_, 2, 2);
  layout_->addWidget(age_sigs_, 2, 3);

  this->setLayout(layout_);
  this->setFrameStyle(QFrame::StyledPanel | QFrame::Raised);
}

} // namespace ds_rqt_widgets