//
// Created by ivaughn on 6/15/21.
//

#include "ds_rqt_widgets/QtRosDynamicReconfigureClient.h"

namespace ds_rqt_widgets {

// ///////////////////////////////////////////////////////////////////////// //
// GenericDynamicReconfigureClient
// ///////////////////////////////////////////////////////////////////////// //

GenericDynamicReconfigureClient::GenericDynamicReconfigureClient(const std::string& _name) : name(_name) {
  // do nothing
}

GenericDynamicReconfigureClient::~GenericDynamicReconfigureClient() {
  // do nothing
}

void GenericDynamicReconfigureClient::setCallback(const boost::function<void(AnyMessage)> &cb) {
  callback = cb;
}

const std::string& GenericDynamicReconfigureClient::getName() const {
  return name;
}

// ///////////////////////////////////////////////////////////////////////// //
// QtRosDynamicReconfigureClient
// ///////////////////////////////////////////////////////////////////////// //

QtRosDynamicReconfigureClient::QtRosDynamicReconfigureClient(GenericDynamicReconfigureClient* s) : client(s) {
  client->setCallback(boost::bind(&QtRosDynamicReconfigureClient::on_data, this, _1));
}

QtRosDynamicReconfigureClient::~QtRosDynamicReconfigureClient() {
  delete(client);
}

std::string QtRosDynamicReconfigureClient::getName() const {
  return client->getName();
}
std::string QtRosDynamicReconfigureClient::getTopicName() const {
  return client->getTopicName();
}

void QtRosDynamicReconfigureClient::setTopicName(ros::NodeHandle& nh, const std::string& topicname) {
  client->setTopicName(nh, topicname);
}

void QtRosDynamicReconfigureClient::shutdown() {
  client->shutdown();
}

bool QtRosDynamicReconfigureClient::setConfiguration(AnyMessage msg) {
  return client->setConfiguration(msg);
}

void QtRosDynamicReconfigureClient::on_data(AnyMessage msg) {
  emit received(msg);
}

} // namespace ds_rqt_widgets
