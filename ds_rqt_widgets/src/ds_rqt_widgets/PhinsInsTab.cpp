//
// Created by ivaughn on 6/14/21.
//

#include "ds_rqt_widgets/PhinsInsTab.h"
#include <ds_core_msgs/VoidCmd.h>

namespace ds_rqt_widgets {

PhinsInsTab::PhinsInsTab(const std::string& name,
                         const std::string& reconfigure_path,
                         PhinsIns::PhinsModel model, PhinsIns::PhinsInputs inputs,
                         QWidget* parent)
    : QWidget(parent), name_(name), nh_(reconfigure_path) {
  reset_phins_srv_ = nh_.serviceClient<ds_core_msgs::VoidCmd>("phins_restart");
  setup(reconfigure_path, model, inputs);
}

void PhinsInsTab::updatePhinsbin(AnyMessage phinsbin) {
  table_->updatePhinsbin(phinsbin);
  controller_->updatePhinsbin(phinsbin);
}

void PhinsInsTab::updateConfig(AnyMessage phinsconfig) {
  controller_->updateConfig(phinsconfig);
}

void PhinsInsTab::updateGyroBias(AnyMessage fogest) {
  controller_->updateGyroBias(fogest);
}
void PhinsInsTab::updateAccelBias(AnyMessage accest) {
  controller_->updateAccelBias(accest);
}

void PhinsInsTab::updateAges() {
  table_->updateAges();
  controller_->updateAges();
}

void PhinsInsTab::refresh() {
  table_->refresh();
  controller_->refresh();
}

void PhinsInsTab::setup(const std::string& driver_path,
                        PhinsIns::PhinsModel model, PhinsIns::PhinsInputs inputs) {
  table_ = new PhinsInsTable(name_, this);
  controller_ = new PhinsInsController(name_, driver_path, model, inputs);

  layout_ = new QGridLayout();
  layout_->addWidget(table_, 0, 1);
  layout_->addWidget(controller_, 0, 0);

  connect(controller_, &PhinsInsController::resetPhins, this, &PhinsInsTab::resetPhins);

  this->setLayout(layout_);
}

PhinsInsController* PhinsInsTab::controller() {
  return controller_;
}

PhinsInsTable* PhinsInsTab::table() {
  return table_;
}

void PhinsInsTab::resetPhins() {
  ROS_WARN_STREAM("Resetting PHINS at user request!");
  ds_core_msgs::VoidCmd srv;
  if (!reset_phins_srv_.call(srv)) {
    ROS_ERROR_STREAM("ERROR: UNABLE TO RESET PHINS!");
  }
}

} // namespace ds_rqt_widgets
