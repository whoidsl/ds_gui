//
// Created by ivaughn on 6/14/21.
//

#include "ds_rqt_widgets/PhinsInsController.h"

#include <ds_sensor_msgs/PhinsStdbin3.h>
#include <ds_sensor_msgs/PhinsConfig.h>
#include <QMessageBox>
#include <QFontDatabase>

namespace ds_rqt_widgets {


// ///////////////////////////////////////////////////////////////////////// //
// PhinsInsExternalSensor base class
// ///////////////////////////////////////////////////////////////////////// //
PhinsInsExternalSensor::PhinsInsExternalSensor(const std::string& _name,
                                  int _algo_word,
                                  uint32_t _algo_mask_reception,
                                  uint32_t _algo_mask_valid,
                                  uint32_t _algo_mask_waiting,
                                  uint32_t _algo_mask_rejected,
                                  QWidget* _parent) :
                                  QWidget(_parent),
                                  name(_name),
                                  algo_word(_algo_word),
                                  algo_mask_reception(_algo_mask_reception),
                                  algo_mask_valid(_algo_mask_valid),
                                  algo_mask_waiting(_algo_mask_waiting),
                                  algo_mask_rejected (_algo_mask_rejected) {
  reject_filter_mode = 0;
}

int PhinsInsExternalSensor::setup(QGridLayout* layout, int row, int col, bool visible) {
  ROS_INFO_STREAM("Adding PHINS INS External sensor " <<name);

  label = new QLabel(QString::fromStdString(name), this);
  on = new IndicatorButton("On", this);
  off = new IndicatorButton("Off", this);
  force = new IndicatorButton("Force", this);
  reception = new IndicatorLabel("Recv", this);
  valid = new IndicatorLabel("Valid", this);
  waiting = new IndicatorLabel("Wait", this);
  rejected = new IndicatorLabel("Reject", this);

  reception->setContentsMargins(5, 0, 5, 0);
  valid->setContentsMargins(5, 0, 5, 0);
  waiting->setContentsMargins(5, 0, 5, 0);
  rejected->setContentsMargins(5, 0, 5, 0);

  if (visible) {
  layout->addWidget(label, row, col++);
  layout->addWidget(on, row, col++);
  layout->addWidget(off, row, col++);
  layout->addWidget(force, row, col++);
  layout->addWidget(reception, row, col++);
  layout->addWidget(valid, row, col++);
  layout->addWidget(waiting, row, col++);
  layout->addWidget(rejected, row, col++);
  }

  connect(on, &IndicatorButton::released, [this](){this->setRejectionPolicy(2);});
  connect(off, &IndicatorButton::released, [this](){this->setRejectionPolicy(1);});
  connect(force, &IndicatorButton::released, [this](){this->setRejectionPolicy(0);});

  return col;
}


void PhinsInsExternalSensor::refresh(const ds_sensor_msgs::PhinsStdbin3& msg,
                const ds_sensor_msgs::PhinsConfig& config_msg) {
  uint32_t algo_status = msg.ins_algo_status[algo_word-1];

  if (algo_status & algo_mask_reception) {
    reception->setState(ds_rqt_widgets::IndicatorButton::GOOD);
  } else {
    reception->setState(ds_rqt_widgets::IndicatorButton::BAD);
  }

  if (algo_status & algo_mask_valid) {
    valid->setState(ds_rqt_widgets::IndicatorButton::GOOD);
  } else {
    valid->setState(ds_rqt_widgets::IndicatorButton::BAD);
  }

  if (algo_status & algo_mask_waiting) {
    waiting->setState(ds_rqt_widgets::IndicatorButton::WARN);
  } else {
    waiting->setState(ds_rqt_widgets::IndicatorButton::OFF);
  }
  if (algo_status & algo_mask_rejected) {
    rejected->setState(ds_rqt_widgets::IndicatorButton::BAD);
  } else {
    rejected->setState(ds_rqt_widgets::IndicatorButton::OFF);
  }

  reject_filter_mode = getRejectMode(config_msg);
  on->setState(ds_rqt_widgets::IndicatorButton::Status::OFF);
  off->setState(ds_rqt_widgets::IndicatorButton::Status::OFF);
  force->setState(ds_rqt_widgets::IndicatorButton::Status::OFF);
  switch (reject_filter_mode) {
    case 0:
      force->setState(ds_rqt_widgets::IndicatorButton::Status::GOOD);
      break;
    case 1:
      off->setState(ds_rqt_widgets::IndicatorButton::Status::GOOD);
      break;
    case 2:
      on->setState(ds_rqt_widgets::IndicatorButton::Status::GOOD);
      break;
  }
}

void PhinsInsExternalSensor::updateConfig(ds_sensor_msgs::PhinsConfig& config_msg) const {
  this->setRejectMode(config_msg, reject_filter_mode);
}

void PhinsInsExternalSensor::setRejectionPolicy(int value) {
  reject_filter_mode = value;
  ROS_INFO_STREAM("Setting sensor " <<name <<" to rejection policy " <<value);
  emit rejectModeUpdated(value);
}

// ///////////////////////////////////////////////////////////////////////// //
// Individual sensor classes
// ///////////////////////////////////////////////////////////////////////// //

// not the prettiest way to do this, but it'll work
class Gps1Sensor : public PhinsInsExternalSensor {
 public:
  explicit Gps1Sensor(QWidget* parent=0) : PhinsInsExternalSensor("GPS 1", 1,
                                                     0x00001000, 0x00002000,
                                                     0x00004000, 0x00008000, parent) {}
  int getRejectMode(const ds_sensor_msgs::PhinsConfig& config_msg) const override {
    return config_msg.gps1Rejection;
  }
  void setRejectMode(ds_sensor_msgs::PhinsConfig& config_msg, int mode) const override {
    config_msg.gps1Rejection = mode;
  }
};

class Gps2Sensor : public PhinsInsExternalSensor {
 public:
  explicit Gps2Sensor(QWidget* parent=0) : PhinsInsExternalSensor("GPS 2", 2,
                                                                  0x00000010, 0x00000020,
                                                                  0x00000040, 0x00000080, parent) {}
  int getRejectMode(const ds_sensor_msgs::PhinsConfig& config_msg) const override {
    return config_msg.gps2Rejection;
  }
  void setRejectMode(ds_sensor_msgs::PhinsConfig& config_msg, int mode) const override {
    config_msg.gps2Rejection = mode;
  }
};

class Dvl1Sensor : public PhinsInsExternalSensor {
 public:
  explicit Dvl1Sensor(QWidget* parent=0) : PhinsInsExternalSensor("DVL 1", 1,
                                                                  0x00000100, 0x00000200,
                                                                  0x00000400, 0x00000800, parent) {}
  int getRejectMode(const ds_sensor_msgs::PhinsConfig& config_msg) const override {
    return config_msg.dvl1Rejection;
  }
  void setRejectMode(ds_sensor_msgs::PhinsConfig& config_msg, int mode) const override {
    config_msg.dvl1Rejection = mode;
  }
};

class Dvl2Sensor : public PhinsInsExternalSensor {
 public:
  explicit Dvl2Sensor(QWidget* parent=0) : PhinsInsExternalSensor("DVL 2", 4,
                                                                  0x00000001, 0x00000002,
                                                                  0x00000004, 0x00000008, parent) {}
  int getRejectMode(const ds_sensor_msgs::PhinsConfig& config_msg) const override {
    return config_msg.dvl2Rejection;
  }
  void setRejectMode(ds_sensor_msgs::PhinsConfig& config_msg, int mode) const override {
    config_msg.dvl2Rejection = mode;
  }
};

class DvlWt1Sensor : public PhinsInsExternalSensor {
 public:
  explicit DvlWt1Sensor(QWidget* parent=0) : PhinsInsExternalSensor("DVL WT 1", 2,
                                                                  0x00000001, 0x00000002,
                                                                  0x00000004, 0x00000008, parent) {}
  int getRejectMode(const ds_sensor_msgs::PhinsConfig& config_msg) const override {
    return config_msg.dvlWt1Rejection;
  }
  void setRejectMode(ds_sensor_msgs::PhinsConfig& config_msg, int mode) const override {
    config_msg.dvlWt1Rejection = mode;
  }
};

class DvlWt2Sensor : public PhinsInsExternalSensor {
 public:
  explicit DvlWt2Sensor(QWidget* parent=0) : PhinsInsExternalSensor("DVL WT 2", 4,
                                                                    0x00000010, 0x00000020,
                                                                    0x00000040, 0x00000080, parent) {}
  int getRejectMode(const ds_sensor_msgs::PhinsConfig& config_msg) const override {
    return config_msg.dvlWt2Rejection;
  }
  void setRejectMode(ds_sensor_msgs::PhinsConfig& config_msg, int mode) const override {
    config_msg.dvlWt2Rejection = mode;
  }
};

class DepthSensor : public PhinsInsExternalSensor {
 public:
  explicit DepthSensor(QWidget* parent=0) : PhinsInsExternalSensor("Depth", 1,
                                                                   0x00100000, 0x00200000,
                                                                   0x00400000, 0x00800000, parent) {}
  int getRejectMode(const ds_sensor_msgs::PhinsConfig& config_msg) const override {
    return config_msg.depthRejection;
  }
  void setRejectMode(ds_sensor_msgs::PhinsConfig& config_msg, int mode) const override {
    config_msg.depthRejection = mode;
  }
};

class UsblSensor : public PhinsInsExternalSensor {
 public:
  explicit UsblSensor(QWidget* parent=0) : PhinsInsExternalSensor("USBL", 1,
                                                                   0x00010000, 0x00020000,
                                                                   0x00040000, 0x00080000, parent) {}
  int getRejectMode(const ds_sensor_msgs::PhinsConfig& config_msg) const override {
    return config_msg.usblRejection;
  }
  void setRejectMode(ds_sensor_msgs::PhinsConfig& config_msg, int mode) const override {
    config_msg.usblRejection = mode;
  }
};

// ///////////////////////////////////////////////////////////////////////// //
// PhinsInsController
// ///////////////////////////////////////////////////////////////////////// //

PhinsInsController::PhinsInsController(const std::string& name,
                                       const std::string& reconfigure_path,
                                       const PhinsIns::PhinsModel& model,
                                       const PhinsIns::PhinsInputs& inputs,
                                       QWidget* parent) : QWidget(parent), model_(model){
                                    setup(inputs);
}

void PhinsInsController::updateAges() {

}

void PhinsInsController::refresh() {
  // don't update too fast
  QDateTime now = QDateTime::currentDateTimeUtc();
  qint64 msecs_to = last_gui_update_time.msecsTo(now);
  if (last_gui_update_time.isNull() || msecs_to > 100) {
    last_gui_update_time = now;
  } else {
    return;
  }

  zupt_modes_->updateMode(config_msg.zuptMode);
  altitude_modes_->updateMode(config_msg.altMode);

  for (const auto& sensor : external_sensors_) {
    sensor->refresh(msg, config_msg);
  }

  // update alignment info
  std::stringstream alignment_msg;
  if (msg.ins_algo_status[0] & 0x00000001) {
    alignment_msg <<"NAV    ";
  }
  if (msg.ins_algo_status[0] & 0x00000002) {
    alignment_msg <<"ALIGN_COARSE    ";
  }
  if (msg.ins_algo_status[0] & 0x00000004) {
    alignment_msg <<"ALIGN_FINE    ";
  }
  if (msg.ins_algo_status[0] & 0x00000008) {
    alignment_msg <<"DEADRECK";
  }
  alignment_status_->setText(QString::fromStdString(alignment_msg.str()));

  bias_fog_field_->setText(QString("[ %1 %2 %3 ] m%4/hr")
                               .arg(fogest_msg.vector.x, 11, 'f', 3)
                               .arg(fogest_msg.vector.y, 11, 'f', 3)
                               .arg(fogest_msg.vector.z, 11, 'f', 3)
                               .arg(QChar(0260)));

  bias_acc_field_->setText(QString("[ %1 %2 %3 ] %4g")
                               .arg(accest_msg.vector.x, 11, 'f', 3)
                               .arg(accest_msg.vector.y, 11, 'f', 3)
                               .arg(accest_msg.vector.z, 11, 'f', 3)
                               .arg(QChar(0x00b5)));

  messages_->setText(QString::fromStdString(makeMessages(msg, model_)));
}

void PhinsInsController::updatePhinsbin(AnyMessage phinsbin) {
  msg = *(phinsbin.as<ds_sensor_msgs::PhinsStdbin3>());
  refresh();
}

void PhinsInsController::updateConfig(AnyMessage phinscfg) {
  config_msg = *(phinscfg.as<ds_sensor_msgs::PhinsConfig>());
  //refresh(); // don't refresh for config biases-- save that for phinsbin
}

void PhinsInsController::updateGyroBias(AnyMessage fogest) {
  fogest_msg = *(fogest.as<geometry_msgs::Vector3Stamped>());
  //refresh(); // don't refresh for gyro biases-- save that for phinsbin
}
void PhinsInsController::updateAccelBias(AnyMessage accest) {
  accest_msg = *(accest.as<geometry_msgs::Vector3Stamped>());
  //refresh(); // don't refresh for accelerometer biases-- save that for phinsbin
}

void PhinsInsController::generateNewConfig(int) {
  // make a temporary copy
  ds_sensor_msgs::PhinsConfig newConfig(config_msg);

  newConfig.zuptMode = zupt_modes_->getValue();
  newConfig.altMode = altitude_modes_->getValue();

  for (const auto sensor : external_sensors_) {
    sensor->updateConfig(newConfig);
  }

  boost::shared_ptr<const ds_sensor_msgs::PhinsConfig> config_to_send(
      new const ds_sensor_msgs::PhinsConfig(newConfig));
  emit newPhinsConfig(AnyMessage(config_to_send));
}

void PhinsInsController::setup(const PhinsIns::PhinsInputs& inputs) {
  layout_ = new QHBoxLayout();
  sensors_box_ = new QGroupBox(tr("External Sensors"), this);
  sensors_layout_ = new QGridLayout();
  sensors_box_->setLayout(sensors_layout_);
  sensors_layout_->setAlignment(Qt::AlignTop);

  info_layout_ = new QVBoxLayout();
  info_layout_->setAlignment(Qt::AlignTop);
  const QFont fixedFont = QFontDatabase::systemFont(QFontDatabase::FixedFont);

  // external sensors
  if (PhinsIns::isSet(inputs, PhinsIns::GPS1)) {
    external_sensors_.push_back(new Gps1Sensor(this));
  }
  if (PhinsIns::isSet(inputs, PhinsIns::GPS2)) {
    external_sensors_.push_back(new Gps2Sensor(this));
  }
  if (PhinsIns::isSet(inputs, PhinsIns::DVL1)) {
    external_sensors_.push_back(new Dvl1Sensor(this));
  }
  if (PhinsIns::isSet(inputs, PhinsIns::DVLWT1)) {
    external_sensors_.push_back(new DvlWt1Sensor(this));
  }
  if (PhinsIns::isSet(inputs, PhinsIns::DVL2)) {
    external_sensors_.push_back(new Dvl2Sensor(this));
  }
  if (PhinsIns::isSet(inputs, PhinsIns::DVLWT2)) {
    external_sensors_.push_back(new DvlWt2Sensor(this));
  }
  if (PhinsIns::isSet(inputs, PhinsIns::DEPTH)) {
    external_sensors_.push_back(new DepthSensor(this));
  }
  if (PhinsIns::isSet(inputs, PhinsIns::USBL)) {
    external_sensors_.push_back(new UsblSensor(this));
  }

  int row=0;
  int after_sensors_col;
  for (const auto& sensor : external_sensors_) {
    after_sensors_col = sensor->setup(sensors_layout_, row++, 0); // all will have the same number of columns
    connect(sensor, &PhinsInsExternalSensor::rejectModeUpdated, this, &PhinsInsController::generateNewConfig);
  }

  // informational widgets
  // zupt mode widget
  zupt_modes_ = new IndicatorButtonGroup("Zero-Update Mode", "ZUPT", 6, this);
  zupt_modes_->addButton("Off", 0);
  zupt_modes_->addButton("Static 10m/s", 1);
  zupt_modes_->addButton("Static 0.1m/s", 2);
  zupt_modes_->addButton("Auto 0.01m/s", 3);
  zupt_modes_->addButton("Auto Bench", 4);
  zupt_modes_->addButton("Position", 5);
  connect(zupt_modes_, &IndicatorButtonGroup::button_selected, this, &PhinsInsController::generateNewConfig);
  info_layout_->addWidget(zupt_modes_);

  // altitude modes
  altitude_modes_ = new IndicatorButtonGroup("Altitude Mode", "ALT", 6, this);
  altitude_modes_->addButton("Stabilize", 0);
  altitude_modes_->addButton("GPS", 1);
  altitude_modes_->addButton("Depth", 2);
  altitude_modes_->addButton("Hydro", 3);
  connect(altitude_modes_, &IndicatorButtonGroup::button_selected, this, &PhinsInsController::generateNewConfig);
  info_layout_->addWidget(altitude_modes_);

  // Alignment status
  alignment_box_ = new QGroupBox(tr("Alignment Status"), this);
  alignment_status_ = new QLabel(tr("?"), this);
  alignment_status_->setAlignment(Qt::AlignCenter);
  QHBoxLayout* alignment_layout = new QHBoxLayout();
  alignment_layout->addWidget(alignment_status_, Qt::AlignCenter);
  alignment_box_->setLayout(alignment_layout);
  info_layout_->addWidget(alignment_box_);

  // FOG biases
  bias_fog_box_ = new QGroupBox(tr("Gyro Biases"), this);
  bias_fog_field_ = new QLabel(tr("?"), this);
  bias_fog_field_->setAlignment(Qt::AlignCenter);
  bias_fog_field_->setFont(fixedFont);
  QHBoxLayout* bias_fog_layout = new QHBoxLayout();
  bias_fog_layout->addWidget(bias_fog_field_, Qt::AlignCenter);
  bias_fog_box_->setLayout(bias_fog_layout);
  info_layout_->addWidget(bias_fog_box_);

  // ACC biases
  bias_acc_box_ = new QGroupBox(tr("Accelerometer Biases"), this);
  bias_acc_field_ = new QLabel(tr("?"), this);
  bias_acc_field_->setAlignment(Qt::AlignCenter);
  bias_acc_field_->setFont(fixedFont);
  QHBoxLayout* bias_acc_layout = new QHBoxLayout();
  bias_acc_layout->addWidget(bias_acc_field_, Qt::AlignCenter);
  bias_acc_box_->setLayout(bias_acc_layout);
  info_layout_->addWidget(bias_acc_box_);

  // Restart button
  restart_but_ = new QPushButton("Restart", this);
  connect(restart_but_, &QPushButton::released, this, &PhinsInsController::trigger_restart_dialog);
  info_layout_->addWidget(restart_but_);

  // Overall message layout
  message_box_ = new QGroupBox(tr("Messages"), this);
  messages_ = new QLabel(tr(""), this);
  messages_->setAlignment(Qt::AlignLeft | Qt::AlignTop);
  messages_->setFont(fixedFont);
  QHBoxLayout* message_layout = new QHBoxLayout();
  message_layout->addWidget(messages_, Qt::AlignLeft | Qt::AlignTop);
  message_box_->setLayout(message_layout);
  messages_->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Expanding);

  // build final layout
  layout_->addWidget(sensors_box_);
  layout_->addLayout(info_layout_);
  layout_->addWidget(message_box_);
  this->setLayout(layout_);
}

void PhinsInsController::trigger_restart_dialog() {
  QMessageBox msgBox;
  msgBox.setWindowTitle("Confirm PHINS restart?");
  msgBox.setText(QString::fromStdString("Are you SURE you want to restart the PHINS? \nThis requires good data and 5 minutes!"));
  msgBox.setStandardButtons(QMessageBox::Yes);
  msgBox.addButton(QMessageBox::No);
  if (msgBox.exec() == QMessageBox::Yes) {
    emit resetPhins();
  }
}

std::string PhinsInsController::makeMessages(const ds_sensor_msgs::PhinsStdbin3& msg, const PhinsIns::PhinsModel& MODEL) {
  std::stringstream ret;

  // super-high-level conditions
  if (msg.ins_user_status & 0x80000000) {
    ret <<"** FAILED **\n";
  }
  if (msg.ins_user_status & 0x40000000) {
    ret <<"** DEGRADED **\n";
  }
  if (msg.ins_user_status & 0x08000000) {
    ret <<"Aligning\n";
  }
  if (msg.ins_user_status & 0x04000000) {
    ret <<"Heading/Roll/Pitch INVALID\n";
  }
  // high-level hardware errors
  if (msg.sensor_status[1] & 0x80000000) {
    ret <<"HARDWARE FAILED\n";
  }
  if (msg.sensor_status[1] & 0x40000000) {
    ret <<"HARDWARE DEGRADED\n";
  }
  if (msg.sensor_status[1] & 0x00000001) {
    ret <<"FOG X ERROR\n";
  }
  if (msg.sensor_status[1] & 0x00000002) {
    ret <<"FOG Y ERROR\n";
  }
  if (msg.sensor_status[1] & 0x00000004) {
    ret <<"FOG Z ERROR\n";
  }
  if (msg.sensor_status[1] & 0x00000008) {
    ret <<"FOG SOURCE ERROR\n";
  }
  if (msg.sensor_status[1] & 0x00000010) {
    ret <<"ACC X ERROR\n";
  }
  if (msg.sensor_status[1] & 0x00000020) {
    ret <<"ACC Y ERROR\n";
  }
  if (msg.sensor_status[1] & 0x00000040) {
    ret <<"ACC Z ERROR\n";
  }
  if (msg.sensor_status[1] & 0x00000080) {
    ret <<"TEMPERATURE ERROR\n";
  }
  if (msg.sensor_status[1] & 0x00000100) {
    ret <<"DSP OVERLOAD\n";
  }
  if (msg.sensor_status[1] & 0x00001000) {
    ret <<"Modelisation error\n";
  }

  // Critical algorithm failures
  if (msg.ins_algo_status[2] & 0x10000000) {
    ret <<"FIRMWARE INCOMPATIBLE WITH HARDWARE\n";
  }
  if (msg.ins_algo_status[0] & 0x10000000) {
    ret <<"ALTITUDE SATURATION\n";
  }
  if (msg.ins_algo_status[0] & 0x20000000) {
    ret <<"SPEED SATURATION\n";
  }
  if (msg.ins_algo_status[0] & 0x40000000) {
    ret <<"Interpolation Missed\n";
  }
  if (msg.ins_algo_status[2] & 0x00040000) {
    ret <<"Fast alignment failed\n";
  }
  if (msg.ins_algo_status[2] & 0x00200000) {
    ret <<"External sensor data outdated\n";
  }
  if (msg.ins_algo_status[2] & 0x00400000) {
    ret <<"Sensor used before calibration\n";
  }
  if (msg.ins_algo_status[2] & 0x00800000) {
    ret <<"Fast alignment rejected\n";
  }

  // run modes
  if (msg.ins_system_status[1] & 0x00200000) {
    ret <<"Wait for position\n";
  }
  if (msg.ins_system_status[1] & 0x00010000) {
    ret <<"SIMULATION MODE\n";
  }
  if (msg.ins_algo_status[1] & 0x00800000) {
    ret <<"IMU EMULATION MODE\n";
  }
  if (msg.ins_system_status[1] & 0x00800000) {
    ret <<"Polar Mode\n";
  }
  if (msg.ins_system_status[1] & 0x01000000) {
    ret <<"Internal Log ACTIVE\n";
  }
  if (msg.ins_system_status[1] & 0x04000000) {
    ret <<"Vertical deflection received\n";
  }
  if (msg.ins_system_status[2] & 0x00000004) {
    ret <<"Advanced filtering mode enabled\n";
  }
  if (msg.ins_system_status[1] & 0x20000000) {
    ret <<"Running in R&D Mode\n";
  }
  if (msg.ins_system_status[1] & 0x40000000) {
    ret <<"Configuration Saved\n";
  }

  // detailed hardware errors
  if (msg.ins_system_status[1] & 0x00040000) {
    ret <<"DSP INCOMPATIBILITY\n";
  }
  if (msg.ins_system_status[1] & 0x08000000) {
    ret <<"MPC OVERLOAD\n";
  }
  if (msg.ins_system_status[1] & 0x10000000) {
    ret <<"POWER SUPPLY FAILED\n";
  }
  if (msg.sensor_status[0] & 0x00000001) {
    ret <<"LOSS OF RAW DATA\n";
  }
  if (msg.sensor_status[0] & 0x00000002) {
    ret <<"FOG LASER POWER SUPPLY ERROR\n";
  }
  if (msg.sensor_status[0] & 0x00000004) {
    ret <<"FOG LASER DIODE OFF\n";
  }
  if (msg.sensor_status[0] & 0x00000008) {
    ret <<"FOG LASER NOT IN POWER CONTROL MODE\n";
  }
  if (msg.sensor_status[0] & 0x00000010) {
    ret <<"ACC X SATURATION ERROR\n";
  }
  if (msg.sensor_status[0] & 0x00000020) {
    ret <<"ACC Y SATURATION ERROR\n";
  }
  if (msg.sensor_status[0] & 0x00000040) {
    ret <<"ACC Z SATURATION ERROR\n";
  }
  if (msg.sensor_status[0] & 0x00000080) {
    ret <<"ACC X ACQUISITION ERROR\n";
  }
  if (msg.sensor_status[0] & 0x00000100) {
    ret <<"ACC Y ACQUISITION ERROR\n";
  }
  if (msg.sensor_status[0] & 0x00000200) {
    ret <<"ACC Z ACQUISITION ERROR\n";
  }
  if (msg.sensor_status[0] & 0x00000400) {
    ret <<"FOG X SATURATION ERROR\n";
  }
  if (msg.sensor_status[0] & 0x00000800) {
    ret <<"FOG Y SATURATION ERROR\n";
  }
  if (msg.sensor_status[0] & 0x00001000) {
    ret <<"FOG Z SATURATION ERROR\n";
  }
  if (msg.sensor_status[0] & 0x00002000) {
    ret <<"FOG X VPI VOLTAGE CONTROL ERROR\n";
  }
  if (msg.sensor_status[0] & 0x00004000) {
    ret <<"FOG Y VPI VOLTAGE CONTROL ERROR\n";
  }
  if (msg.sensor_status[0] & 0x00008000) {
    ret <<"FOG Z VPI VOLTAGE CONTROL ERROR\n";
  }
  if (msg.sensor_status[0] & 0x00010000) {
    ret <<"FOG X LOW POWER\n";
  }
  if (msg.sensor_status[0] & 0x00020000) {
    ret <<"FOG Y LOW POWER\n";
  }
  if (msg.sensor_status[0] & 0x00040000) {
    ret <<"FOG Z LOW POWER\n";
  }
  if (msg.sensor_status[0] & 0x00080000) {
    ret <<"FOG X ACQ ERROR\n";
  }
  if (msg.sensor_status[0] & 0x00100000) {
    ret <<"FOG Y ACQ ERROR\n";
  }
  if (msg.sensor_status[0] & 0x00200000) {
    ret <<"FOG Z ACQ ERROR\n";
  }
  if (msg.sensor_status[0] & 0x00400000) {
    ret <<"FOG X CRC ERROR\n";
  }
  if (msg.sensor_status[0] & 0x00800000) {
    ret <<"FOG Y CRC ERROR\n";
  }
  if (msg.sensor_status[0] & 0x01000000) {
    ret <<"FOG Z CRC ERROR\n";
  }
  if (msg.sensor_status[1] & 0x00000200) {
    ret <<"CAN_ACC_X Initialization Error\n";
  }
  if (msg.sensor_status[1] & 0x00000400) {
    ret <<"CAN_ACC_Y Initialization Error\n";
  }
  if (msg.sensor_status[1] & 0x00000800) {
    ret <<"CAN_ACC_Z Initialization Error\n";
  }
  if (msg.sensor_status[0] & 0x02000000) {
    ret <<"TEMPERATURE ACQUISITION ERROR\n";
  }
  if (msg.sensor_status[0] & 0x04000000) {
    ret <<"TEMPERATURE THRESHOLD ERROR\n";
  }
  if (msg.sensor_status[0] & 0x08000000) {
    ret <<"Temperature Rate Too High\n";
  }
  if (msg.sensor_status[0] & 0x10000000) {
    ret <<"Sensor data FIFO WARN\n";
  }
  if (msg.sensor_status[0] & 0x20000000) {
    ret <<"SENSOR DATA FIFO FULL ERROR\n";
  }
  if (msg.sensor_status[0] & 0x40000000) {
    ret <<"SOURCE POWER ERROR\n";
  }
  if (msg.sensor_status[0] & 0x80000000) {
    ret <<"SOURCE DATA RECEPTION ERROR\n";
  }
  // I/O Errors
  if (msg.ins_system_status[0] & 0x00000001) {
    ret <<"SERIAL ERROR on REPEATER\n";
  }
  if (msg.ins_system_status[0] & 0x00400000) {
    ret <<"ETHERNET OUTPUT FULL\n";
  }
  if (msg.ins_system_status[0] & 0x00000002) {
    if (MODEL == PhinsIns::COMPACT_C7) {
      ret <<"Invalid error for PHINS type C7\n";
    } else {
      ret << "INPUT A ERROR\n";
    }
  }
  if (msg.ins_system_status[0] & 0x00000004) {
    if (MODEL == PhinsIns::COMPACT_C7) {
      ret <<"Invalid error for PHINS type C7\n";
    } else {
      ret << "INPUT B ERROR\n";
    }
  }
  if (msg.ins_system_status[0] & 0x00000008) {
    if (MODEL == PhinsIns::COMPACT_C7) {
      ret <<"Invalid error for PHINS type C7\n";
    } else {
      ret << "INPUT C ERROR\n";
    }
  }
  if (msg.ins_system_status[0] & 0x00000010) {
    if (MODEL == PhinsIns::COMPACT_C7) {
      ret << "INPUT A ERROR\n";
    } else {
      ret << "INPUT D ERROR\n";
    }
  }
  if (msg.ins_system_status[0] & 0x00000020) {
    if (MODEL == PhinsIns::COMPACT_C7) {
      ret << "INPUT B ERROR\n";
    } else {
      ret << "INPUT E ERROR\n";
    }
  }
  if (msg.ins_system_status[0] & 0x00000040) {
    if (MODEL == PhinsIns::COMPACT_C7) {
      ret << "INPUT C ERROR\n";
    } else {
      ret << "INPUT F ERROR\n";
    }
  }
  if (msg.ins_system_status[0] & 0x00000080) {
    if (MODEL == PhinsIns::COMPACT_C7) {
      ret << "INPUT D ERROR\n";
    } else {
      ret << "INPUT G ERROR\n";
    }
  }
  if (msg.ins_system_status[0] & 0x00010000) {
    ret <<"OUTPUT R FULL\n";
  }
  if (msg.ins_system_status[0] & 0x00020000) {
    ret << "OUTPUT A FULL\n";
  }
  if (msg.ins_system_status[0] & 0x00040000) {
    ret << "OUTPUT B FULL\n";
  }
  if (msg.ins_system_status[0] & 0x00080000) {
    ret << "OUTPUT C FULL\n";
  }
  if (msg.ins_system_status[0] & 0x00100000) {
    ret << "OUTPUT D FULL\n";
  }
  if (msg.ins_system_status[0] & 0x00200000) {
    ret <<"OUTPUT E FULL\n";
  }

  // timing info
  if (msg.ins_system_status[0] & 0x01000000) {
    ret <<"Internal Time Used\n";
  }
  if (msg.ins_system_status[1] & 0x00000200) {
    ret <<"UTC Data Detected\n";
  }
  if (msg.ins_system_status[1] & 0x00000400) {
    ret <<"Altitude Data Detected\n";
  }
  if (msg.ins_system_status[1] & 0x00000800) {
    ret <<"PPS Data Detected\n";
  }
  if (msg.ins_system_status[2] & 0x00000001) {
    ret <<"UTC2 detected\n";
  }
  if (msg.ins_system_status[2] & 0x00000002) {
    ret <<"PPS2 detected\n";
  }
  if (msg.ins_system_status[2] & 0x00000008) {
    ret <<"NTP sync in progress\n";
  }
  if (msg.ins_system_status[2] & 0x00000010) {
    ret <<"NTP received\n";
  }
  if (msg.ins_system_status[2] & 0x00000020) {
    ret <<"NTP sync successful\n";
  }
  if (msg.ins_system_status[2] & 0x00000040) {
    ret <<"NTP sync failed\n";
  }

  // normal algorithmic conditions
  if (msg.ins_algo_status[0] & 0x40000000) {
    ret <<"Heave Initialization\n";
  }
  if (msg.ins_algo_status[1] & 0x00100000) {
    ret <<"Static Convergence ON\n";
  }
  if (msg.ins_algo_status[1] & 0x00200000) {
    ret <<"Static Convergence ended; NAV OK\n";
  }
  if (msg.ins_algo_status[2] & 0x00000001) {
    ret <<"Sound Velocity Recv\n";
  }
  if (msg.ins_algo_status[2] & 0x00000002) {
    ret <<"Sound Velocity Valid\n";
  }
  if (msg.ins_algo_status[2] & 0x00000004) {
    ret <<"Sound Velocity Waiting\n";
  }
  if (msg.ins_algo_status[2] & 0x00000008) {
    ret <<"Sound Velocity Rejected\n";
  }
  if (msg.ins_algo_status[2] & 0x00020000) {
    ret <<"DVL Calibration Check Active\n";
  }
  if (msg.ins_algo_status[2] & 0x00080000) {
    ret <<"Relative speed ZUP enabled\n";
  }
  if (msg.ins_algo_status[2] & 0x00100000) {
    ret <<"Relative speed ZUP valid\n";
  }
  if (msg.ins_algo_status[2] & 0x08000000) {
    ret <<"Polar data valid\n";
  }
  if (msg.ins_algo_status[3] & 0x00000100) {
    ret <<"DVL Distance Traveled Valid\n";
  }
  if (msg.ins_algo_status[3] & 0x00000200) {
    ret <<"DVL Calibration NONE\n";
  }
  if (msg.ins_algo_status[3] & 0x00000400) {
    ret <<"DVL Rough Calibration\n";
  }
  if (msg.ins_algo_status[3] & 0x00000800) {
    ret <<"DVL Fine Calibration\n";
  }
  if (msg.ins_algo_status[3] & 0x00001000) {
    ret <<"DVL Calibration Check in Progress\n";
  }

  // normal I/O activity notification
  if (msg.ins_system_status[0] & 0x04000000) {
    ret <<"Ethernet activity\n";
  }
  if (msg.ins_system_status[0] & 0x00000100) {
    ret <<"Input R activity\n";
  }
  if (msg.ins_system_status[0] & 0x00000200) {
    if (MODEL == PhinsIns::COMPACT_C7) {
      ret <<"Invalid activity for PHINS type C7\n";
    } else {
      ret << "Input A activity\n";
    }
  }
  if (msg.ins_system_status[0] & 0x00000400) {
    if (MODEL == PhinsIns::COMPACT_C7) {
      ret << "Invalid activity for PHINS type C7\n";
    } else {
      ret << "Input B activity\n";
    }
  }
  if (msg.ins_system_status[0] & 0x00000800) {
    if (MODEL == PhinsIns::COMPACT_C7) {
      ret << "Invalid activity for PHINS type C7\n";
    } else {
      ret << "Input C activity\n";
    }
  }
  if (msg.ins_system_status[0] & 0x00001000) {
    if (MODEL == PhinsIns::COMPACT_C7) {
      ret << "Input A activity\n";
    } else {
      ret << "Input D activity\n";
    }
  }
  if (msg.ins_system_status[0] & 0x00002000) {
    if (MODEL == PhinsIns::COMPACT_C7) {
      ret << "Input B activity\n";
    } else {
      ret << "Input E activity\n";
    }
  }
  if (msg.ins_system_status[0] & 0x00004000) {
    if (MODEL == PhinsIns::COMPACT_C7) {
      ret << "Input C activity\n";
    } else {
      ret << "Input F activity\n";
    }
  }
  if (msg.ins_system_status[0] & 0x00008000) {
    if (MODEL == PhinsIns::COMPACT_C7) {
      ret << "Input D activity\n";
    } else {
      ret << "Input G activity\n";
    }
  }
  if (msg.ins_system_status[0] & 0x08000000) {
    ret <<"PulseIn A activity\n";
  }
  if (msg.ins_system_status[0] & 0x10000000) {
    ret <<"PulseIn B activity\n";
  }
  if (msg.ins_system_status[0] & 0x20000000) {
    ret <<"PulseIn C activity\n";
  }
  if (msg.ins_system_status[0] & 0x40000000) {
    ret <<"PulseIn D activity\n";
  }

  return ret.str();
}

} // namespace ds_rqt_widgets


























































































