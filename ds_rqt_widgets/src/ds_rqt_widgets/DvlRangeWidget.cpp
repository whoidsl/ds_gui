//
// Created by ivaughn on 2/25/19.
//

#include "ds_rqt_widgets/DvlRangeWidget.h"


#include <limits>
#include <Eigen/Dense>

#include <tf2_ros/transform_listener.h>
#include <tf2_eigen/tf2_eigen.h>

#include <ds_sensor_msgs/Dvl.h>
#include <QtWidgets/QMenu>
#include <QtWidgets/QInputDialog>

#define DEFAULT_SIZE 200

namespace ds_rqt_widgets {

DvlRangeWidget::DvlRangeWidget(QWidget *parent) : QWidget(parent) {
  range_combined = recalcCombined();

  min_range = 0;
  max_range = 100.0;
  tf_ok = false;

  body_frame_id = "base_link";

  // make the options dialog pop up correctly
  this->setContextMenuPolicy(Qt::CustomContextMenu);
  connect(this, &QWidget::customContextMenuRequested,
      this, &DvlRangeWidget::ShowContextMenu);
}

void DvlRangeWidget::refresh() {
  update();
}

double DvlRangeWidget::getRangeCombined() const {
  return range_combined;
}

double DvlRangeWidget::getMinRange() const {
  return min_range;
}

double DvlRangeWidget::getMaxRange() const {
  return max_range;
}

const std::string& DvlRangeWidget::getDvlFrameId() const {
  return dvl_frame_id;
}

const std::string& DvlRangeWidget::getBodyFrameId() const {
  return body_frame_id;
}

bool DvlRangeWidget::getTfOk() const {
  return tf_ok;
}

void DvlRangeWidget::updateFromMessage(AnyMessage raw_msg) {
  auto msg = raw_msg.as<ds_sensor_msgs::Dvl>();

  // check if we need to rebuild our various caches
  if (!tf_ok
      || dvl_frame_id != msg->header.frame_id
      || msg->range.size() != ranges.size()) {

    std::cout <<"Updating DVL range/beam angle cache!" <<std::endl;

    dvl_frame_id = msg->header.frame_id;

    ranges.resize(msg->range.size());
    range2alt.resize(msg->range.size(), 1.0);

    // this will update size of
    // range -> altitude conversion,
    // and the beam angles.
    updateBeamAngles(msg);
  }

  // update range
  for (size_t i=0; i<ranges.size(); i++) {
    ranges[i] = msg->range[i];
  }
  range_combined = recalcCombined();

  // update GUI
  //update();
}

const std::vector<double>& DvlRangeWidget::getRanges() const {
  return ranges;
}

std::vector<double>& DvlRangeWidget::getRanges() {
  return ranges;
}

const std::vector<double>& DvlRangeWidget::getRange2Alt() const {
  return range2alt;
}

std::vector<double>& DvlRangeWidget::getRange2Alt() {
  return range2alt;
}

void DvlRangeWidget::setMinRange(double range) {
  min_range = range;
}

void DvlRangeWidget::setMaxRange(double range) {
  max_range = range;
}

void DvlRangeWidget::setDvlFrameId(const std::string& frame) {
  dvl_frame_id = frame;
  tf_ok = false;
}

void DvlRangeWidget::setBodyFrameId(const std::string& frame) {
  body_frame_id = frame;
  tf_ok = false;
}

void DvlRangeWidget::resizeEvent(QResizeEvent *event) {
  QWidget::resizeEvent(event);
}

void DvlRangeWidget::paintEvent(QPaintEvent *event) {
  QPainter painter(this);
  draw(painter);
  QWidget::paintEvent(event);
}

void DvlRangeWidget::ShowContextMenu(const QPoint &pos) {

  QMenu contextMenu(tr("Widget Options"), this);

  QAction setMinRangeOpt(tr("Set Min Range"), this);
  connect(&setMinRangeOpt, &QAction::triggered, [this]() {
    bool ok;
    double result = QInputDialog::getDouble(this, tr("Set DvlRangeWidget Property"),
        tr("Min Range:"), min_range, 0, max_range, 2, &ok);
    if (ok) { this->setMinRange(result); }
  });
  contextMenu.addAction(&setMinRangeOpt);

  QAction setMaxRangeOpt(tr("Set Max Range"), this);
  connect(&setMaxRangeOpt, &QAction::triggered, [this]() {
    bool ok;
    double result = QInputDialog::getDouble(this, tr("Set DvlRangeWidget Property"),
        tr("Max Range:"), max_range, min_range, 12000, 2, &ok);
    if (ok) { this->setMaxRange(result); }
  });
  contextMenu.addAction(&setMaxRangeOpt);

  QAction setBodyFrameOpt(tr("Set Body Frame"), this);
  connect(&setBodyFrameOpt, &QAction::triggered, [this]() {
    bool ok;
    QString result = QInputDialog::getText(this, tr("Set DvlRangeWidget Property"),
                                           tr("Body frame name"), QLineEdit::Normal,
                                           tr(body_frame_id.c_str()), &ok);
    if (ok) {
      this->setBodyFrameId(result.toStdString());
    }
  });
  contextMenu.addAction(&setBodyFrameOpt);

  contextMenu.exec(mapToGlobal(pos));
}

void DvlRangeWidget::saveSettings(const std::string& name,
    qt_gui_cpp::Settings &plugin_settings,
    qt_gui_cpp::Settings &instance_settings) const {

  instance_settings.setValue(QString::fromStdString(name + "_min_range"), min_range);
  instance_settings.setValue(QString::fromStdString(name + "_max_range"), max_range);
  instance_settings.setValue(QString::fromStdString(name + "_body_frame"),
      QString::fromStdString(body_frame_id));

}
void DvlRangeWidget::restoreSettings(const std::string& name,
    const qt_gui_cpp::Settings &plugin_settings,
    const qt_gui_cpp::Settings &instance_settings) {

  bool ok;
  double result;
  result = instance_settings.value(QString::fromStdString(name + "_min_range"), min_range).toDouble(&ok);
  if (ok) {
    min_range = result;
  } else {
    ROS_ERROR("DvlRangeWidget: Could not parse rqt_gui setting for min range!");
  }

  result = instance_settings.value(QString::fromStdString(name + "_max_range"), max_range).toDouble(&ok);
  if (ok) {
    max_range = result;
  } else {
    ROS_ERROR("DvlRangeWidget: Could not parse rqt_gui setting for max range!");
  }

  QString result_str;
  result_str = instance_settings.value(QString::fromStdString(name + "_body_frame"),
                             QString::fromStdString(body_frame_id)).toString();
  body_frame_id = result_str.toStdString();

  tf_ok = false;
}

QSize DvlRangeWidget::sizeHint() const {
  return QSize(DEFAULT_SIZE, DEFAULT_SIZE);
}

double DvlRangeWidget::recalcCombined() const {
  double accum = 0;
  int ngood = 0;

  for (size_t i=0; i<ranges.size(); i++) {
    if (std::isfinite(ranges[i])) {
      ngood++;
      accum += (ranges[i] * range2alt[i]);
    }
  }

  if (ngood == 0) {
    return std::numeric_limits<double>::quiet_NaN();
  }

  return accum / static_cast<double>(ngood);
}

void DvlRangeWidget::updateBeamAngles(const ds_sensor_msgs::DvlConstPtr& msg) {
  //std::cout <<"Attempting to initialize based on beam info "
  //          <<"\"" <<dvl_frame_id <<"\" --> \"" <<body_frame_id <<"\"..." <<std::endl;

  double beamwidth_deg = 30;
  tf2_ros::Buffer tfBuffer;
  tf2_ros::TransformListener tfListener(tfBuffer);

  try {
    geometry_msgs::TransformStamped tf_tform = tfBuffer.lookupTransform(
      dvl_frame_id, body_frame_id, ros::Time(0), ros::Duration(0.5));

    std::cout <<"Setting beam orientations based on transform from "
              <<"\"" <<dvl_frame_id <<"\" --> \"" <<body_frame_id <<"\"" <<std::endl;

    range2alt.resize(0);
    beams.clear();

    Eigen::Affine3d tform = tf2::transformToEigen(tf_tform);

    for (size_t i=0; i<msg->beam_unit_vec.size(); i++) {
      // There's no conversion for Vector3, because obviously that would be useful.
      Eigen::Vector3d beam_unit;
      const geometry_msgs::Vector3& msg_unit_vec = msg->beam_unit_vec[i];
      beam_unit <<msg_unit_vec.x, msg_unit_vec.y, msg_unit_vec.z;

      Eigen::Vector3d body_unit = tform.rotation() * beam_unit;

      range2alt.push_back(fabs(body_unit(2)));
      double az = atan2(body_unit(1), body_unit(0));
      beams.push_back(BeamIndicator(90+az*180.0/M_PI, beamwidth_deg));

      /*
      std::cout <<"Beam: " <<i <<", range: " <<msg->range[i] <<" orig. unit: ("
          <<msg_unit_vec.x <<"," <<msg_unit_vec.x <<"," <<msg_unit_vec.x <<")"
          <<" -> body ("
          <<body_unit(0) <<"," <<body_unit(1) <<"," <<body_unit(2) <<")"
          <<" r2a: " <<range2alt.back() <<" az: " <<az*180.0/M_PI <<std::endl;
          */

    }

    tf_ok = true;
  } catch (tf2::TransformException err) {

    tf_ok = false;
  }
}

void DvlRangeWidget::setupBeams() {
  const double beamwidth = 30;

  beams.push_back(BeamIndicator(45, beamwidth));  // fwd port
  beams.push_back(BeamIndicator(135, beamwidth)); // fwd stbd
  beams.push_back(BeamIndicator(225, beamwidth)); // aft stbd
  beams.push_back(BeamIndicator(315, beamwidth)); // aft port
}

void DvlRangeWidget::draw(QPainter& painter) {
    // First, setup the painter coordinate system;
  // it will be centered on the widget's center, and run from
  // -/+50
  int side = qMin(width(), height());
  painter.setViewport((width() - side)/2, (height()-side)/2, side, side);
  painter.setWindow(-DEFAULT_SIZE/2, -DEFAULT_SIZE/2, DEFAULT_SIZE, DEFAULT_SIZE);

  QRect win = painter.window();

  // prepare to draw stuff
  QPen pen(Qt::white);
  pen.setWidth(1);

  painter.setRenderHint(QPainter::Antialiasing);
  painter.setRenderHint(QPainter::TextAntialiasing);
  painter.setPen(pen);

  // draw the background circle
  painter.setBrush(Qt::black);
  // defined as a BOX, not as a center + radius
  painter.drawEllipse(-DEFAULT_SIZE/2, -DEFAULT_SIZE/2, DEFAULT_SIZE, DEFAULT_SIZE);

  if (!tf_ok) {
    const int TEXT_SIZE = DEFAULT_SIZE/2;

    QFont font = painter.font();
    font.setPixelSize(TEXT_SIZE/4);
    font.setBold(true);
    painter.setFont(font);
    painter.setPen(Qt::red);

    painter.drawText(QRect(-TEXT_SIZE*4, -TEXT_SIZE, TEXT_SIZE*8, TEXT_SIZE*2),
        Qt::AlignCenter, QString::fromStdString("Bad\nTF or\nData"));
    return;
  }

  // draw each range
  for (size_t i=0; i<beams.size(); i++) {
    drawBeam(painter, beams[i], ranges[i]);
  }

  // black out the middle

  painter.setPen(Qt::black);
  painter.setBrush(Qt::black);
  painter.drawEllipse(-DEFAULT_SIZE/4, -DEFAULT_SIZE/4, DEFAULT_SIZE/2, DEFAULT_SIZE/2);

  const int TEXT_SIZE = DEFAULT_SIZE/5;

  QFont font = painter.font();
  font.setPixelSize(TEXT_SIZE);
  font.setBold(true);
  painter.setFont(font);
  painter.setPen(Qt::white);

  QString text;
  if (std::isfinite(range_combined)) {
    text = QString::number(range_combined, 'f', 1);
  } else {
    text="BAD";
  }

  painter.drawText(QRect(-TEXT_SIZE*2, -TEXT_SIZE/2, TEXT_SIZE*4, TEXT_SIZE),
      Qt::AlignCenter, text);
}

void DvlRangeWidget::drawBeam(QPainter& painter,
    const DvlRangeWidget::BeamIndicator& beam, double range) {

  QPainterPath rangePath(QPointF(0.0, 0.0));

  QString text;
  double range_fraction;
  if (std::isfinite(range)) {
    range_fraction = std::min(fabs(range) / max_range, 1.0);
    range_fraction = range_fraction/2.0 + 0.5; // at 0 range, we want 0.5

    if (range < min_range) {
      // turn the background red
      painter.setPen(Qt::red);
      painter.setBrush(Qt::red);
      painter.drawPie(-DEFAULT_SIZE/2+1, -DEFAULT_SIZE/2+1, DEFAULT_SIZE-2, DEFAULT_SIZE-2,
            beam.angle_sixteenths - beam.beamwidth_sixteenths/2.0,
            beam.beamwidth_sixteenths);

    }

    painter.setPen(Qt::blue);
    painter.setBrush(Qt::blue);
    text = QString::number(range, 'f', 1);

  } else {
    range_fraction = 1.0;
    painter.setPen(Qt::darkGray);
    painter.setBrush(Qt::darkGray);
    text="BAD";
  }

  // draw the actual beam
  painter.drawPie(std::round((-DEFAULT_SIZE/2+1)*range_fraction),
                  std::round((-DEFAULT_SIZE/2+1)*range_fraction),
                  std::round((DEFAULT_SIZE-2)*range_fraction),
                  std::round((DEFAULT_SIZE-2)*range_fraction),
      beam.angle_sixteenths - beam.beamwidth_sixteenths/2.0, beam.beamwidth_sixteenths);

  const int TEXT_SIZE = DEFAULT_SIZE/10;

  // add the per-beam range label
  QFont font = painter.font();
  font.setPixelSize(TEXT_SIZE);
  font.setBold(false);
  painter.setFont(font);
  painter.setPen(Qt::lightGray);

  painter.drawText(QRect(DEFAULT_SIZE*beam.center_edge_x*4/10-TEXT_SIZE*2,
            DEFAULT_SIZE*beam.center_edge_y*4/10-TEXT_SIZE/2,
            TEXT_SIZE*4, TEXT_SIZE),
      Qt::AlignCenter, text);
}

DvlRangeWidget::BeamIndicator::BeamIndicator(double angle, double beamwidth) {
  angle_deg = angle;
  beamwidth_deg = beamwidth;

  // Some of the QT functions use integer sixteenths of a degree; cache that here.
  angle_sixteenths = std::round(16.0 * angle);
  beamwidth_sixteenths = std::round(16.0 * beamwidth);

  center_edge_x = cos(M_PI/180.0*angle);
  center_edge_y = -sin(M_PI/180.0*angle);
}

}

