//
// Created by ivaughn on 12/28/19.
//

#include "ds_rqt_widgets/UBloxSvInTable.h"

namespace Field {
const static size_t name = 0;
const static size_t status = name + 1;
const static size_t accuracy = status + 1;
const static size_t duration = accuracy + 1;
const static size_t observations = duration + 1;
const static size_t ecef_x = observations + 1;
const static size_t ecef_y = ecef_x + 1;
const static size_t ecef_z = ecef_y + 1;
const static size_t gps_time = ecef_z + 1;
const static size_t tf_frame = gps_time + 1;
const static size_t interval = gps_time + 1;
const static size_t age = interval + 1;
const static size_t NUM_ROWS = age+1;
}

namespace ds_rqt_widgets {

UbloxSvInTable::UbloxSvInTable(const std::string& name, QWidget *parent)
    : SensorTableBase(Field::age, name, parent) {
  setup();
}

void UbloxSvInTable::updateSvin(AnyMessage svin) {
  msg = *(svin.as<ds_sensor_msgs::UbloxSurveyIn>());
  updateMsgTime();
  refresh();
}

void UbloxSvInTable::refresh() {
  if (!checkRefresh()) {
    return;
  }

  std::string status_str = "UNKNOWN";
  if (msg.valid) {
    status_str = "Valid";
  } else {
    status_str = "Invalid";
  }
  if (msg.active) {
    status_str += "/Active";
  } else {
    status_str += "/Inactive";
  }

  fields[Field::status]->setText(QString::fromStdString(status_str));
  fields[Field::accuracy]->setText(QString("%1 m").arg(msg.surveyin_accuracy, 5, 'f', 9));
  fields[Field::duration]->setText(QString("%1 s").arg(msg.duration, 6));
  fields[Field::observations]->setText(QString("%1 m").arg(msg.observations, 9));
  fields[Field::ecef_x]->setText(QString("%1 m").arg(msg.mean_ecef.x, 11, 'f', 4));
  fields[Field::ecef_y]->setText(QString("%1 m").arg(msg.mean_ecef.y, 11, 'f', 4));
  fields[Field::ecef_z]->setText(QString("%1 m").arg(msg.mean_ecef.z, 11, 'f', 4));
  fields[Field::gps_time]->setText(QString("%1 s")
                                       .arg(static_cast<double>(msg.time_of_week)/1000.0, 10, 'f', 3));
  fields[Field::tf_frame]->setText(QString::fromStdString(msg.header.frame_id));
  fields[Field::interval]->setText(QString("%1 ms").arg(msg_interval, 8));
}

void UbloxSvInTable::setup() {
  std::vector<std::string> row_labels(Field::NUM_ROWS, " ");

  row_labels[Field::status] = "Status";
  row_labels[Field::accuracy] = "Accuracy";
  row_labels[Field::duration] = "Duration";
  row_labels[Field::observations] = "Observations";
  row_labels[Field::ecef_x] = "ECEF, X";
  row_labels[Field::ecef_y] = "ECEF, Y";
  row_labels[Field::ecef_z] = "ECEF, Z";
  row_labels[Field::gps_time] = "GPS Time";
  row_labels[Field::tf_frame] = "TF Frame";
  row_labels[Field::interval] = "Interval";
  row_labels[Field::age] = "Message Age";
  SensorTableBase::setup(row_labels);

  // make the header and have it span both columns
  fields[Field::name]->setText(QString::fromStdString(name_));
  layout->addWidget(fields[Field::name], Field::name, 0, 1, 2);
  fields[Field::name]->setAlignment(Qt::AlignCenter);
}

} // namespace ds_rqt_widgets