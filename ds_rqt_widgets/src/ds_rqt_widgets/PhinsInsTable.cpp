//
// Created by ivaughn on 6/14/21.
//

#include "ds_rqt_widgets/PhinsInsTable.h"
#include "ds_sensor_msgs/PhinsStdbin3.h"

namespace Field {

const static size_t name = 0;
const static size_t hpr = name+2;
const static size_t hpr_stddev = hpr+ 1;
const static size_t hpr_rate = hpr_stddev+ 1;

const static size_t lat = hpr_rate + 2;
const static size_t lon = lat + 1;
const static size_t alt = lon + 1;

const static size_t vel = alt + 2;
const static size_t vel_stddev = vel + 1;

const static size_t sensor_status = vel_stddev + 2;
const static size_t algo_status_1 = sensor_status + 1;
const static size_t algo_status_2 = algo_status_1 + 1;
const static size_t system_status = algo_status_2 + 1;
const static size_t user_status = system_status + 1;

const static size_t tf_frame = user_status + 2;
const static size_t latency = tf_frame + 1;
const static size_t interval = latency + 1;
const static size_t age = interval + 1;
const static size_t NUM_ROWS = age+1;

} // namespace Field

namespace ds_rqt_widgets {

PhinsInsTable::PhinsInsTable(const std::string& name, QWidget *parent) : SensorTableBase(Field::age, name, parent) {
  setup();
}

void PhinsInsTable::updatePhinsbin(AnyMessage phinsbin) {
  msg = *(phinsbin.as<ds_sensor_msgs::PhinsStdbin3>());
  updateMsgTime();
  refresh();
}

void PhinsInsTable::refresh() {
  if (!checkRefresh()) {
    return;
  }
  fields[Field::hpr]->setText(QString("[ %1 %2 %3 ] %4")
                                  .arg(msg.heading, 7, 'f', 3)
                                  .arg(msg.pitch, 7, 'f', 3)
                                  .arg(msg.roll, 7, 'f', 3).arg(QChar(0260)));

  fields[Field::hpr_stddev]->setText(QString("[ %1 %2 %3 ] %4")
                                  .arg(msg.heading_stddev, 7, 'f', 3)
                                  .arg(msg.pitch_stddev, 7, 'f', 3)
                                  .arg(msg.roll_stddev, 7, 'f', 3).arg(QChar(0260)));

  fields[Field::hpr_rate]->setText(QString("[ %1 %2 %3 ] %4/s")
                                  .arg(msg.heading_rate, 7, 'f', 3)
                                  .arg(msg.pitch_rate, 7, 'f', 3)
                                  .arg(msg.roll_rate, 7, 'f', 3).arg(QChar(0260)));

  fields[Field::lat]->setText(QString("%1%2 +/- %3m")
                                  .arg(msg.latitude, 12, 'f', 7)
                                  .arg(QChar(0260))
                                  .arg(sqrt(msg.position_cov[0]), 7, 'f', 3));

  fields[Field::lon]->setText(QString("%1%2 +/- %3m")
                                  .arg(msg.longitude, 12, 'f', 7)
                                  .arg(QChar(0260))
                                  .arg(sqrt(msg.position_cov[3]), 7, 'f', 3));

  fields[Field::alt]->setText(QString("%1m +/- %3m")
                                  .arg(msg.altitude, 12, 'f', 3)
                                  .arg(msg.altitude_stddev, 7, 'f', 3));

  fields[Field::vel]->setText(QString("[ %1 %2 %3 ] m/s")
                                  .arg(msg.velocity_NEU[0], 7, 'f', 3)
                                  .arg(msg.velocity_NEU[1], 7, 'f', 3)
                                  .arg(msg.velocity_NEU[2], 7, 'f', 3));

  fields[Field::vel_stddev]->setText(QString("[ %1 %2 %3 ] m/s")
                                  .arg(msg.velocity_stddev_NEU[0], 7, 'f', 3)
                                  .arg(msg.velocity_stddev_NEU[1], 7, 'f', 3)
                                  .arg(msg.velocity_stddev_NEU[2], 7, 'f', 3));

  fields[Field::sensor_status]->setText(QString("%1 %2")
                                         .arg(msg.sensor_status[0], 8, 16, QChar('0'))
                                         .arg(msg.sensor_status[1], 8, 16, QChar('0')));

  fields[Field::algo_status_1]->setText(QString("%1 %2")
                                         .arg(msg.ins_algo_status[0], 8, 16, QChar('0'))
                                         .arg(msg.ins_algo_status[1], 8, 16, QChar('0')));

  fields[Field::algo_status_2]->setText(QString("%1 %2")
                                         .arg(msg.ins_algo_status[2], 8, 16, QChar('0'))
                                         .arg(msg.ins_algo_status[3], 8, 16, QChar('0')));

  fields[Field::system_status]->setText(QString("%1 %2 %3")
                                            .arg(msg.ins_system_status[0], 8, 16, QChar('0'))
                                            .arg(msg.ins_system_status[1], 8, 16, QChar('0'))
                                            .arg(msg.ins_system_status[2], 8, 16, QChar('0')));

  fields[Field::user_status]->setText(QString("%1").arg(msg.ins_user_status, 8, 16, QChar('0')));

  fields[Field::tf_frame]->setText(QString::fromStdString(msg.header.frame_id));
  ros::Duration latency = msg.ds_header.io_time - msg.header.stamp;
  fields[Field::latency]->setText(QString("%1 ms").arg(latency.toSec()*1000.0, 6, 'f', 1));

  fields[Field::interval]->setText(QString("%1 ms").arg(msg_interval, 8));
}

void PhinsInsTable::setup() {
  std::vector<std::string> row_labels(Field::NUM_ROWS, " ");

  row_labels[Field::hpr] = "Hdg/Pitch/Roll";
  row_labels[Field::hpr_stddev] = "Std. Dev.";
  row_labels[Field::hpr_rate] = "Rates";

  row_labels[Field::lat] = "Latitude";
  row_labels[Field::lon] = "Longitude";
  row_labels[Field::alt] = "Altitude";

  row_labels[Field::vel] = "North/East/Down Vel.";
  row_labels[Field::vel_stddev] = "Vel. Std. Dev.";

  row_labels[Field::sensor_status] = "Sensor Status";
  row_labels[Field::algo_status_1] = "Algo Status 1/2";
  row_labels[Field::algo_status_2] = "Algo Status 3/4";
  row_labels[Field::system_status] = "Sys. Status";
  row_labels[Field::user_status] = "User Status";

  row_labels[Field::tf_frame] = "TF Frame";
  row_labels[Field::latency] = "Latency";
  row_labels[Field::interval] = "Interval";
  row_labels[Field::age] = "Message Age";

  SensorTableBase::setup(row_labels);

  // make the header and have it span both columns
  fields[Field::name]->setText(QString::fromStdString(name_));
  layout->addWidget(fields[Field::name], Field::name, 0, 1, 2);
  fields[Field::name]->setAlignment(Qt::AlignCenter);
}

} // namespace ds_rqt_widgets
