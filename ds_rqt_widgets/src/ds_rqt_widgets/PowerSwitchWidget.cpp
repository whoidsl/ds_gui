//
// Created by ivandor on 5/1/20.
//

#include "ds_rqt_widgets/PowerSwitchWidget.h"

#include <iostream>

namespace ds_rqt_widgets {

const static int MAX_WIDTH = 10;

PowerSwitchWidget::PowerSwitchWidget(QWidget *parent) : QWidget(parent) {
}

PowerSwitchWidget::PowerSwitchWidget(std::string name, QWidget* parent) : PowerSwitchWidget(parent) {
}

QLabel* PowerSwitchWidget::setupLabel(const std::string& text, const QFont& font) {
    QLabel* ret = new QLabel(tr(text.c_str()), this);
    ret->setAlignment(Qt::AlignCenter);
    ret->setFont(font);

    return ret;
}

void PowerSwitchWidget::setupWidget() {
    QFont label_font;
    label_font.setBold(false);
    QFont data_font;
    data_font.setBold(true);
    layout = new QGridLayout();

    this->setLayout(layout);
}

QGroupBox *PowerSwitchWidget::buttonGroup(const char* groupName, std::vector<std::string>&devices_group, int columns) {
  QGroupBox *groupBox = new QGroupBox(tr(groupName));
  QGridLayout* box_layout = new QGridLayout();
  int row=0;
  int col=0;
  for (size_t i=0;i<devices_group.size();i++) {
    //ROS_WARN_STREAM("devices: "<<devices_group[i].c_str());
    new_btn = new ds_rqt_widgets::IndicatorButton(QString::fromUtf8(devices_group[i].c_str()), this);
    //new_btn->setGeometry(QRect(QPoint(100,100),QSize(200,50)));
    new_btn->setState(ds_rqt_widgets::IndicatorButton::Status::OFF);
    //connect(new_btn, SIGNAL (clicked()), this, SLOT(clickedButton()));
    pwr_btns.push_back(new_btn);
    box_layout->addWidget(new_btn, row, col);
    col++;
    if (col >= columns) {
      col=0;
      row++;
    }
    //vbox->addStretch(1);
  }
  groupBox->setLayout(box_layout);
  return groupBox;
}
void PowerSwitchWidget::updateButton(std::string bName, bool confirm_status) {
  QString bStr = QString::fromUtf8(bName.c_str());
  int size_btn = pwr_btns.size();
  //ROS_WARN_STREAM("btn: "<<bName<<" | status: "<<confirm_status);
  for (size_t i=0;i<pwr_btns.size();i++) {
    if (confirm_status){
      if (bName == pwr_btns[i]->text().toStdString()) {
        pwr_btns[i]->setState(ds_rqt_widgets::IndicatorButton::Status::GOOD);
      }
    }
    else {
      if (bName == pwr_btns[i]->text().toStdString()) {
        pwr_btns[i]->setState(ds_rqt_widgets::IndicatorButton::Status::OFF);
      }
    }
  }
}
}