//
// Created by ivaughn on 12/27/19.
//

#include "ds_rqt_widgets/SensorTableBase.h"

namespace ds_rqt_widgets {

SensorTableBase::SensorTableBase(int age_field_idx, std::string name, QWidget *parent) :
    age_field_idx_(age_field_idx),
    name_(name),
    QFrame(parent)
{
}

bool SensorTableBase::checkRefresh() {
  QDateTime now = QDateTime::currentDateTimeUtc();
  qint64 msecs_to = last_gui_update_time.msecsTo(now);

  if (last_gui_update_time.isNull() || msecs_to > 100) {
    last_gui_update_time = now;
    return true;
  }
  return false;
}

void SensorTableBase::updateMsgTime() {
  auto now = QDateTime::currentDateTimeUtc();

  msg_interval = msg_time.msecsTo(now);
  msg_time = now;
}

void SensorTableBase::updateAges() {
  auto now = QDateTime::currentDateTimeUtc();
  QLabel* age_field = fields[age_field_idx_];
  if (msg_time.isNull()) {
    age_field->setText(tr("Never"));
    return;
  }
  // we have to do the math this clunky way because the QTime libraries don't seem to have been designed
  // with arithmatic in mind because.... who knows.
  qint64 msecs_to = msg_time.msecsTo(now);
  auto delay = QTime::fromMSecsSinceStartOfDay(msecs_to);

  if (delay.hour() > 0) {
    age_field->setText(delay.toString("hh:mm:ss.zzz"));
  } else if (delay.minute() > 0) {
    age_field->setText(delay.toString("mm:ss.zzz"));
  } else {
    age_field->setText(delay.toString("ss.zzz"));
  }
}

const std::string& SensorTableBase::name() const {
  return name_;
}

void SensorTableBase::setup(const std::vector<std::string>& row_labels) {
  layout = new QGridLayout();

  const QFont fixedFont = QFontDatabase::systemFont(QFontDatabase::FixedFont);

  // fill out column headers
  headers.resize(row_labels.size());
  fields.resize(row_labels.size());
  for (size_t f=0; f < headers.size(); f++) {
    headers[f] = new QLabel(QString::fromStdString(row_labels[f]), this);
    //headers[f]->setSizePolicy(QSizePolicy(QSizePolicy::Policy::Maximum, QSizePolicy::Policy::Maximum));
    headers[f]->setContentsMargins(20, 0, 20, 0);
    headers[f]->setAlignment(Qt::AlignVCenter | Qt::AlignRight);
    headers[f]->setFont(fixedFont);

    layout->addWidget(headers[f], f, 0);

    fields[f] = new QLabel(tr(" "), this);
    //fields[f]->setSizePolicy(QSizePolicy(QSizePolicy::Policy::Maximum, QSizePolicy::Policy::Maximum));
    fields[f]->setContentsMargins(20, 0, 20, 0);
    fields[f]->setAlignment(Qt::AlignVCenter | Qt::AlignLeft);
    fields[f]->setFont(fixedFont);
    layout->addWidget(fields[f], f, 1);
  }

  layout->setRowStretch(row_labels.size()+1, 1);
  layout->setColumnStretch(1, 1);
  this->setLayout(layout);

  this->setFrameStyle(QFrame::StyledPanel | QFrame::Raised);
}

} // namespace ds_rqt_widgets
