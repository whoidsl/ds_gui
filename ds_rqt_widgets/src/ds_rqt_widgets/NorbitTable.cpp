//
// Created by ivandor on 3/19/20.
//

#include "ds_rqt_widgets/NorbitTable.h"

namespace Field {
    const static size_t name = 0;
    const static size_t snd_vel = name+2;
    const static size_t num_beams = snd_vel + 1;
    const static size_t ping_num = num_beams + 1;
    const static size_t percent_good = ping_num + 1;
    const static size_t max_depth = percent_good + 1;
    const static size_t min_depth = max_depth + 1;
    const static size_t center_depth = min_depth + 1;
    const static size_t ping_time = center_depth + 1;
    const static size_t ping_rate = ping_time + 1;

    const static size_t tf_frame = ping_rate + 2;
    const static size_t latency = tf_frame + 1;
    const static size_t interval = latency + 1;
    const static size_t age = interval + 1;
    const static size_t NUM_ROWS = age+1;
}

namespace ds_rqt_widgets {

    NorbitTable::NorbitTable(const std::string& name, QWidget *parent)
            : SensorTableBase(Field::age, name, parent) {
        setup();
    }

    void NorbitTable::updateNorbit(AnyMessage raw) {
        msg = *(raw.as<ds_multibeam_msgs::NorbitMB>());
        updateMsgTime();
        refresh();
    }

    void NorbitTable::refresh() {
        if (!checkRefresh()) {
            return;
        }

        fields[Field::snd_vel]->setText(QString("%1 m/s").arg(msg.snd_velocity, 6, 'f', 2));

        fields[Field::num_beams]->setText(QString("%4").arg(msg.num_beams));
        fields[Field::ping_num]->setText(QString("%4").arg(msg.ping_num));
        fields[Field::percent_good]->setText(QString("%4%").arg(msg.percent_good));
        fields[Field::max_depth]->setText(QString("%4").arg(msg.max_depth));
        fields[Field::min_depth]->setText(QString("%4").arg(msg.min_depth));
        fields[Field::center_depth]->setText(QString("%4").arg(msg.center_depth));
        fields[Field::ping_time]->setText(QString("%1").arg(msg.ping_time, 4, 'f', 3));
        fields[Field::ping_rate]->setText(QString("%1").arg(msg.ping_rate, 4, 'f', 3));

        fields[Field::tf_frame]->setText(QString::fromStdString(msg.header.frame_id));
        ros::Duration latency = msg.ds_header.io_time - msg.header.stamp;
        fields[Field::latency]->setText(QString("%1 ms").arg(latency.toSec()*1000.0, 6, 'f', 1));

        fields[Field::interval]->setText(QString("%1 ms").arg(msg_interval, 8));

    }

    void NorbitTable::setup() {
        std::vector<std::string> row_labels(Field::NUM_ROWS, " ");

        row_labels[Field::snd_vel] = "Sound Vel.";
        row_labels[Field::num_beams] = "Num Beams";
        row_labels[Field::ping_num] = "Ping Num";

        row_labels[Field::percent_good] = "% Good";
        row_labels[Field::max_depth] = "Max Depth";
        row_labels[Field::min_depth] = "Min Depth";
        row_labels[Field::center_depth] = "CTR Depth";
        row_labels[Field::ping_time] = "Ping Time";
        row_labels[Field::ping_rate] = "Ping Rate";

        row_labels[Field::tf_frame] = "TF Frame";
        row_labels[Field::latency] = "Latency";
        row_labels[Field::interval] = "Interval";
        row_labels[Field::age] = "Message Age";
        SensorTableBase::setup(row_labels);

        // make the header and have it span both columns
        fields[Field::name]->setText(QString::fromStdString(name_));
        layout->addWidget(fields[Field::name], Field::name, 0, 1, 2);
        fields[Field::name]->setAlignment(Qt::AlignCenter);
    }

} // namespace ds_rqt_widgets
