//
// Created by ivaughn on 2/4/19.
//

#include "ds_rqt_widgets/QtRosSubscriptionWidget.h"

#include <algorithm>

namespace ds_rqt_widgets {

QtRosSubscriptionWidget::QtRosSubscriptionWidget(QWidget* parent, ros::NodeHandle& nh,
    QtRosSubscriptionManager* _subs) : QWidget(parent), node_handle(nh), sub_manager(_subs) {
  setupWidget();
}

QtRosSubscriptionWidget::~QtRosSubscriptionWidget() {
  // Memory management for our subwidgets is handled by QT's parent/child model
}

void QtRosSubscriptionWidget::setupWidget() {

  const std::vector<QtRosSubscription*>& subscriptions = sub_manager->getSubscriptions();

  // Create the list of topics
  sub_name_labels.resize(subscriptions.size());
  sub_topics.resize(subscriptions.size());
  layout = new QGridLayout(this);

  for (size_t i=0; i<sub_topics.size(); i++) {
    // Create the topic label
    sub_name_labels[i] = new QLabel(QString::fromStdString(subscriptions[i]->getName()), this);

    // Create the combo box
    sub_topics[i] = new QComboBox(this);
    sub_topics[i]->setEditable(true);
    sub_topics[i]->setSizePolicy(QSizePolicy::Policy::Expanding, QSizePolicy::Policy::Minimum);
    sub_topics[i]->setMinimumSize(200, 20);

    // Add to the layout
    layout->addWidget(sub_name_labels[i], i, 0);
    layout->addWidget(sub_topics[i], i, 1);


    // Wire up our connections
    QtRosSubscription* sub = subscriptions[i];
    connect(sub_topics[i], static_cast<void (QComboBox::*)(const QString&)>(&QComboBox::currentIndexChanged),
        [this, sub](const QString& text) { this->onTopicChanged(text, sub); });
  }

  // Add the refresh button
  refresh_but = new QPushButton(QString::fromStdString("Refresh"), this);
  connect(refresh_but, &QPushButton::released, this, &QtRosSubscriptionWidget::refreshTopics);
  refreshTopics(); // ... and also refresh
  layout->addWidget(refresh_but, sub_topics.size(), 0);

  // set the layout to the output
  this->setLayout(layout);

  this->show();
}

void QtRosSubscriptionWidget::onTopicChanged(const QString& text, QtRosSubscription* sub) {
  std::cout <<"Setting subscription named \"" <<sub->getName() <<"\" to \"" <<text.toStdString() <<"\"\n";

  sub->setTopicName(node_handle, text.toStdString(), 10);
}

void QtRosSubscriptionWidget::refreshTopics() {
  const std::vector<QtRosSubscription*>& subscriptions = sub_manager->getSubscriptions();


  for (size_t i=0; i<sub_topics.size(); i++) {
    sub_topics[i]->blockSignals(true); // prevent extra firings
    sub_topics[i]->clear();

    // pre-populate with a list of topics of the correct type
    std::vector<std::string> possible_topics = subscriptions[i]->getTopics(node_handle);
    std::sort(possible_topics.begin(), possible_topics.end());
    int curr_idx = -1;
    for (size_t j=0; j<possible_topics.size(); j++) {
      sub_topics[i]->insertItem(j, QString::fromStdString(possible_topics[j]));
      if (possible_topics[j] == subscriptions[i]->getTopicName()) {
        curr_idx = j;
      }
    }

    // if we didn't find the topic we're currently listening among the list of valid topics, go
    // ahead and add ours at the end
    if (curr_idx < 0) {
      //std::cout <<"No match for subscription \"" <<subscriptions[i]->getTopicName() <<"\"\n";
      curr_idx = possible_topics.size();
      sub_topics[i]->insertItem(curr_idx, QString::fromStdString(subscriptions[i]->getTopicName()));
    }
    // Set the combo box to point at our current topic
    sub_topics[i]->setCurrentIndex(curr_idx);

    sub_topics[i]->blockSignals(false);
  }

}

// ///////////////////////////////////////////////////////////////////////// //
// QtRosSubscriptionPopout
// ///////////////////////////////////////////////////////////////////////// //
QtRosSubscriptionPopout::QtRosSubscriptionPopout(QWidget* parent, ros::NodeHandle& nh, QtRosSubscriptionManager* subs)
: QWidget(parent),
node_handle(nh),
sub_manager(subs),
widget(nullptr)
{
  // do nothing else
}

void QtRosSubscriptionPopout::popup_create() {
  // Only create if it doesn't already exist
  if (!widget) {
    widget = new QtRosSubscriptionWidget(this, node_handle, sub_manager);
    widget->setWindowFlags(Qt::Window);
    widget->setWindowTitle(QString::fromStdString("Topics"));
    widget->setAttribute(Qt::WA_DeleteOnClose);

    connect(widget, &QObject::destroyed, [this]() {this->widget = nullptr;});

    widget->show();
  }
}

void QtRosSubscriptionPopout::popup_close() {
  if (widget) {
    widget->close();
  }
}

}