//
// Created by ivaughn on 12/27/19.
//

#include <ds_sensor_msgs/DepthPressure.h>

#include "ds_rqt_widgets/DepthTable.h"
namespace Field {
const static size_t name = 0;
const static size_t depth = name + 2;
const static size_t median_depth = depth + 1;
const static size_t pressure = median_depth + 1;
const static size_t pressure_covar = pressure + 1;
const static size_t tare = pressure_covar + 2;
const static size_t latitude = tare + 1;
const static size_t pressure_raw = latitude + 1;
const static size_t tf_frame = pressure_raw + 2;
const static size_t interval = tf_frame + 1;
const static size_t age = interval + 1;
const static size_t NUM_ROWS = age+1;
}

namespace ds_rqt_widgets {

DepthTable::DepthTable(const std::string& name, QWidget *parent)
: SensorTableBase(Field::age, name, parent) {
  setup();
}

void DepthTable::updateDepth(AnyMessage depth) {
  msg = *(depth.as<ds_sensor_msgs::DepthPressure>());
  updateMsgTime();
  refresh();
}

void DepthTable::refresh() {
  if (!checkRefresh()) {
    return;
  }

  fields[Field::depth]->setText(QString("%1 m").arg(msg.depth, 8, 'f', 2));
  fields[Field::median_depth]->setText(QString("%1 m").arg(msg.median_depth, 8, 'f', 2));
  fields[Field::pressure]->setText(QString("%1 dbar").arg(msg.pressure, 8, 'f', 2));
  fields[Field::pressure_covar]->setText(QString("%1 dbar").arg(msg.pressure_covar, 8, 'f', 2));
  fields[Field::tare]->setText(QString("%1 dbar").arg(msg.tare, 8, 'f', 2));
  fields[Field::latitude]->setText(QString("%1%2").arg(msg.latitude, 10, 'f', 6).arg(QChar(0260)));
  std::string unitname("UNKNOWN");
  switch (msg.pressure_raw_unit) {
    case ds_sensor_msgs::DepthPressure::UNIT_PRESSURE_PSI:
      unitname = "PSI";
      break;
    case ds_sensor_msgs::DepthPressure::UNIT_PRESSURE_DBAR:
      unitname = "dBar";
      break;
    case ds_sensor_msgs::DepthPressure::UNIT_PRESSURE_HPA:
      unitname = "HPa";
      break;
    case ds_sensor_msgs::DepthPressure::UNIT_PRESSURE_BAR:
      unitname = "bar";
      break;
    case ds_sensor_msgs::DepthPressure::UNIT_PRESSURE_KPA:
      unitname = "kPa";
      break;
    case ds_sensor_msgs::DepthPressure::UNIT_PRESSURE_MPA:
      unitname = "MPa";
      break;
    case ds_sensor_msgs::DepthPressure::UNIT_PRESSURE_INHG:
      unitname = "inHg";
      break;
    case ds_sensor_msgs::DepthPressure::UNIT_PRESSURE_MMHG:
      unitname = "mmHg";
      break;
    case ds_sensor_msgs::DepthPressure::UNIT_PRESSURE_MH2O:
      unitname = "mH2O";
      break;
  }
  fields[Field::pressure_raw]->setText(QString("%1 %2").arg(msg.pressure_raw, 8, 'g', 2)
  .arg(QString::fromStdString(unitname)));

  fields[Field::tf_frame]->setText(QString::fromStdString(msg.header.frame_id));
  fields[Field::interval]->setText(QString("%1 ms").arg(msg_interval, 8));
}

void DepthTable::setup() {
  std::vector<std::string> row_labels(Field::NUM_ROWS, " ");

  row_labels[Field::depth] = "Depth";
  row_labels[Field::median_depth] = "Median Depth";
  row_labels[Field::pressure] = "Pressure";
  row_labels[Field::pressure_covar] = "Pressure Covar";
  row_labels[Field::tare] = "Tare";
  row_labels[Field::latitude] = "Latitude";
  row_labels[Field::pressure_raw] = "Raw Pressure";
  row_labels[Field::tf_frame] = "TF Frame";
  row_labels[Field::interval] = "Interval";
  row_labels[Field::age] = "Message Age";

  SensorTableBase::setup(row_labels);

  // make the header and have it span both columns
  fields[Field::name]->setText(QString::fromStdString(name_));
  layout->addWidget(fields[Field::name], Field::name, 0, 1, 2);
  fields[Field::name]->setAlignment(Qt::AlignCenter);
}

} // namespace ds_rqt_widgets
