//
// Created by ivaughn on 12/27/19.
//

#include "ds_rqt_widgets/UstrainTable.h"
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>

namespace Field {
const static size_t name = 0;
const static size_t hpr = name+1;

const static size_t body_rates = hpr + 2;
const static size_t body_accel = body_rates + 1;
const static size_t body_mag = body_accel + 1;
const static size_t mag_mag = body_mag + 1;

const static size_t int_body_rates = mag_mag + 2;
const static size_t int_body_accel = int_body_rates + 1;
const static size_t quaternion = int_body_accel + 1;
const static size_t gps_time = quaternion + 1;

const static size_t tf_frame = gps_time + 2;
const static size_t latency = tf_frame + 1;
const static size_t interval = latency + 1;
const static size_t age = interval + 1;
const static size_t NUM_ROWS = age+1;
}

namespace ds_rqt_widgets {

UstrainTable::UstrainTable(const std::string& name, QWidget *parent)
: SensorTableBase(Field::age, name, parent) {
  setup();
}

void UstrainTable::updateUstrain(AnyMessage memsimu) {
  msg = *(memsimu.as<ds_sensor_msgs::MemsImu>());
  updateMsgTime();
  refresh();
}

void UstrainTable::refresh() {
  if (!checkRefresh()) {
    return;
  }

  double heading, pitch, roll;

  tf2::Quaternion quat_tf;
  tf2::fromMsg(msg.orientation, quat_tf);
  tf2::Matrix3x3 mat_tf(quat_tf);
  mat_tf.getEulerYPR(heading, pitch, roll);

  fields[Field::hpr]->setText(QString("[ %1 %2 %3 ] %4")
                                  .arg(180.0/M_PI*heading, 7, 'f', 3)
                                  .arg(180.0/M_PI*pitch, 7, 'f', 3)
                                  .arg(180.0/M_PI*roll, 7, 'f', 3).arg(QChar(0260)));

  fields[Field::body_rates]->setText(QString("[ %1 %2 %3 ] %4/s")
                                       .arg(180.0/M_PI*msg.angular_velocity.x, 8, 'f', 4)
                                       .arg(180.0/M_PI*msg.angular_velocity.y, 8, 'f', 4)
                                       .arg(180.0/M_PI*msg.angular_velocity.z, 8, 'f', 4)
                                       .arg(QChar(0260)));

  fields[Field::body_accel]->setText(QString("[ %1 %2 %3 ] m/s^2")
                                         .arg(msg.linear_acceleration.x, 8, 'f', 4)
                                         .arg(msg.linear_acceleration.y, 8, 'f', 4)
                                         .arg(msg.linear_acceleration.z, 8, 'f', 4));

  fields[Field::body_mag]->setText(QString("[ %1 %2 %3 ] Gauss")
                                         .arg(msg.magnetometer.x, 7, 'f', 4)
                                         .arg(msg.magnetometer.y, 7, 'f', 4)
                                         .arg(msg.magnetometer.z, 7, 'f', 4));

  double magmag = sqrt(msg.magnetometer.x*msg.magnetometer.x
                           + msg.magnetometer.y*msg.magnetometer.y
                           + msg.magnetometer.z*msg.magnetometer.z);
  fields[Field::mag_mag]->setText(QString("%1 Gauss")
                                      .arg(magmag, 7, 'f', 4));

  fields[Field::int_body_rates]->setText(QString("[ %1 %2 %3 ] %4")
                                         .arg(180.0/M_PI*msg.angular_delta.x, 8, 'f', 4)
                                         .arg(180.0/M_PI*msg.angular_delta.y, 8, 'f', 4)
                                         .arg(180.0/M_PI*msg.angular_delta.z, 8, 'f', 4)
                                         .arg(QChar(0260)));

  fields[Field::int_body_accel]->setText(QString("[ %1 %2 %3 ] m/s^2")
                                         .arg(msg.linear_delta.x, 8, 'f', 4)
                                         .arg(msg.linear_delta.y, 8, 'f', 4)
                                         .arg(msg.linear_delta.z, 8, 'f', 4));

  fields[Field::quaternion]->setText(QString("[ %1 %2 %3 %4]")
                                         .arg(msg.orientation.w, 6, 'f', 4)
                                         .arg(msg.orientation.x, 6, 'f', 4)
                                         .arg(msg.orientation.y, 6, 'f', 4)
                                         .arg(msg.orientation.z, 6, 'f', 4));

  fields[Field::gps_time]->setText(QString("%1 s / %2 wks")
                                       .arg(msg.gps_time_of_week, 10, 'f', 3)
                                       .arg(msg.gps_week, 4, 10, QChar(' ')));

  fields[Field::tf_frame]->setText(QString::fromStdString(msg.header.frame_id));
  ros::Duration latency = msg.ds_header.io_time - msg.header.stamp;
  fields[Field::latency]->setText(QString("%1 ms").arg(latency.toSec()*1000.0, 6, 'f', 1));

  fields[Field::interval]->setText(QString("%1 ms").arg(msg_interval, 8));
}

void UstrainTable::setup() {
  std::vector<std::string> row_labels(Field::NUM_ROWS, " ");

  row_labels[Field::hpr] = "Hdg/Pitch/Roll";

  row_labels[Field::body_rates] = "Body Rates";
  row_labels[Field::body_accel] = "Body Accel";
  row_labels[Field::body_mag] = "Body Magnetometer";
  row_labels[Field::mag_mag] = "Maggie Magnitude";

  row_labels[Field::int_body_rates] = "Body Rates";
  row_labels[Field::int_body_accel] = "Body Accel";
  row_labels[Field::quaternion] = "Quaternion";
  row_labels[Field::gps_time] = "GPS Time";

  row_labels[Field::tf_frame] = "TF Frame";
  row_labels[Field::latency] = "Latency";
  row_labels[Field::interval] = "Interval";
  row_labels[Field::age] = "Message Age";

  SensorTableBase::setup(row_labels);

  // make the header and have it span both columns
  fields[Field::name]->setText(QString::fromStdString(name_));
  layout->addWidget(fields[Field::name], Field::name, 0, 1, 2);
  fields[Field::name]->setAlignment(Qt::AlignCenter);
}

} // namespace ds_rqt_widgets
