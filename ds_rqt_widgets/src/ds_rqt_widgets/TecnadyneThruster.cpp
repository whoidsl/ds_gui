//
// Created by ivaughn on 10/7/20.
//

#include "ds_rqt_widgets/TecnadyneThruster.h"

namespace ds_rqt_widgets {

TecnadyneThrusterTable::TecnadyneThrusterTable(const std::vector<std::string>& names, QWidget* parent)
    : QFrame(parent), thruster_names(names) {
  setup();
}

void TecnadyneThrusterTable::updateThruster(AnyMessage thrust_msg, int thrusternum) {
  auto msg = thrust_msg.as<ds_actuator_msgs::Tecnadyne561>();

  if (thrusternum > thrusters.size()) {
    ROS_ERROR_STREAM(
        "Thruster message for thruster " << thrusternum + 1 << " but only " << thrusters.size() << " expected by GUI!");
    return;
  }

  thruster_msgs[thrusternum] = *msg;
  msg_times[thrusternum] = QDateTime::currentDateTimeUtc();
}


void TecnadyneThrusterTable::tick() {
 auto now = QDateTime::currentDateTimeUtc();

  for (size_t i=0; i<thrusters.size(); i++) {
    auto& thruster = thrusters[i];
    const auto& msg = thruster_msgs[i];

    if (msg_times[i].isNull()) {
      thruster.msg_age->setText(tr("Never"));
      continue;
    }

    thruster.prop_pwm->setText(QString("%1").arg(msg.prop_pwm, 6));
    thruster.tach_rpm->setText(QString("%1 RPM").arg(msg.tach_rpm, 6, 'f', 0));
    thruster.tach_error->setText(QString("%1").arg(msg.tach_err_count, 6));
    thruster.speed_loop_interval->setText(QString("%1").arg(msg.speed_loop_interval, 6));
    thruster.voltage_volts->setText(QString("%1 V").arg(msg.voltage_volts, 6, 'f', 1));
    thruster.current_amps->setText(QString("%1 A").arg(msg.current_amps, 6, 'f', 1));
    thruster.fault_status->setText(QString("0x%1").arg(msg.fault_status, 4, 16, QChar('0')));
    thruster.reset_status->setText(QString("0x%1").arg(msg.reset_status, 4, 16, QChar('0')));

    // we have to do the math this clunky way because the QTime libraries don't seem to have been designed
    // with arithmatic in mind because.... who knows.
    qint64 msecs_to = msg_times[i].msecsTo(now);
    auto delay = QTime::fromMSecsSinceStartOfDay(msecs_to);

    if (delay.hour() > 0) {
      thruster.msg_age->setText(delay.toString("hh:mm:ss.zzz"));
    } else if (delay.minute() > 0) {
      thruster.msg_age->setText(delay.toString("mm:ss.zzz"));
    } else {
      thruster.msg_age->setText(delay.toString("ss.zzz"));
    }
  }
}

void TecnadyneThrusterTable::setup() {

  layout = new QGridLayout();
  const int HDR_ROWS = 1;
  int row_name = 0;
  int row_pwm = row_name+1;
  int row_rpm = row_pwm+1;
  int row_error = row_rpm+2;
  int row_speed_loop = row_error+1;
  int row_voltage = row_speed_loop+2;
  int row_current = row_voltage+1;
  int row_fault = row_current+2;
  int row_reset = row_fault+1;
  int row_age = row_reset+2;
  int row_stretch = row_age+1;


  // start with some headers
  header_prop_pwm = new QLabel(tr("PWM"), this);
  header_tach_rpm = new QLabel(tr("Tach RPM"), this);
  header_tach_error = new QLabel(tr("Tach Error"), this);
  header_speed_loop_interval = new QLabel(tr("Speed Loop Error"), this);
  header_voltage_volts = new QLabel(tr("Voltage"), this);
  header_current_amps = new QLabel(tr("Current"), this);
  header_fault_status = new QLabel(tr("Fault Status"), this);
  header_reset_status = new QLabel(tr("Reset Status"), this);
  header_msg_age = new QLabel(tr("Message Age"), this);

  setupLabel(header_prop_pwm, true);
  setupLabel(header_tach_rpm, true);
  setupLabel(header_tach_error, true);
  setupLabel(header_speed_loop_interval, true);
  setupLabel(header_voltage_volts, true);
  setupLabel(header_current_amps, true);
  setupLabel(header_fault_status, true);
  setupLabel(header_reset_status, true);
  setupLabel(header_msg_age, true);

  layout->addWidget(header_prop_pwm, row_pwm, 0);
  layout->addWidget(header_tach_rpm, row_rpm, 0);
  layout->addWidget(header_tach_error, row_error, 0);
  layout->addWidget(header_speed_loop_interval, row_speed_loop, 0);
  layout->addWidget(header_voltage_volts, row_voltage, 0);
  layout->addWidget(header_current_amps, row_current, 0);
  layout->addWidget(header_fault_status, row_fault, 0);
  layout->addWidget(header_reset_status, row_reset, 0);
  layout->addWidget(header_msg_age, row_age, 0);

  // add all the thrusters
  thrusters.resize(thruster_names.size());
  thruster_msgs.resize(thruster_names.size());
  msg_times.resize(thruster_names.size());
  for (size_t i=0; i<thrusters.size(); i++) {
    thrusters[i].name = new QLabel(QString::fromStdString(thruster_names[i]), this);
    thrusters[i].prop_pwm = new QLabel(tr("?"), this);
    thrusters[i].tach_rpm = new QLabel(tr("?"), this);
    thrusters[i].tach_error = new QLabel(tr("?"), this);
    thrusters[i].speed_loop_interval = new QLabel(tr("?"), this);
    thrusters[i].voltage_volts = new QLabel(tr("?"), this);
    thrusters[i].current_amps = new QLabel(tr("?"), this);
    thrusters[i].fault_status = new QLabel(tr("?"), this);
    thrusters[i].reset_status = new QLabel(tr("?"), this);
    thrusters[i].msg_age = new QLabel(tr("Never"), this);

    setupLabel(thrusters[i].name);
    thrusters[i].name->setAlignment(Qt::AlignVCenter | Qt::AlignHCenter);
    setupLabel(thrusters[i].prop_pwm);
    setupLabel(thrusters[i].tach_rpm);
    setupLabel(thrusters[i].tach_error);
    setupLabel(thrusters[i].speed_loop_interval);
    setupLabel(thrusters[i].voltage_volts);
    setupLabel(thrusters[i].current_amps);
    setupLabel(thrusters[i].fault_status);
    setupLabel(thrusters[i].reset_status);
    setupLabel(thrusters[i].msg_age);

    layout->addWidget(thrusters[i].name, row_name, i+HDR_ROWS);
    layout->addWidget(thrusters[i].prop_pwm, row_pwm, i+HDR_ROWS);
    layout->addWidget(thrusters[i].tach_rpm, row_rpm, i+HDR_ROWS);
    layout->addWidget(thrusters[i].tach_error, row_error, i+HDR_ROWS);
    layout->addWidget(thrusters[i].speed_loop_interval, row_speed_loop, i+HDR_ROWS);
    layout->addWidget(thrusters[i].voltage_volts, row_voltage, i+HDR_ROWS);
    layout->addWidget(thrusters[i].current_amps, row_current, i+HDR_ROWS);
    layout->addWidget(thrusters[i].fault_status, row_fault, i+HDR_ROWS);
    layout->addWidget(thrusters[i].reset_status, row_reset, i+HDR_ROWS);
    layout->addWidget(thrusters[i].msg_age, row_age, i+HDR_ROWS);
  }
  this->setLayout(layout);

  layout->setRowStretch(row_stretch, 1);
  layout->setColumnStretch(thrusters.size(), 1);
  this->setFrameStyle(QFrame::StyledPanel | QFrame::Raised);
}

void TecnadyneThrusterTable::setupLabel(QLabel* lbl, bool header) {
  const QFont fixedFont = QFontDatabase::systemFont(QFontDatabase::FixedFont);
  lbl->setContentsMargins(20, 0, 20, 0);
  if (header) {
    lbl->setAlignment(Qt::AlignVCenter | Qt::AlignRight);
  } else {
    lbl->setAlignment(Qt::AlignVCenter | Qt::AlignLeft);
  }
  lbl->setFont(fixedFont);
}

} // namespace ds_rqt_widgets
