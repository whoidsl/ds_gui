#include "ds_rqt_widgets/ArtificialHorizon.h"

namespace ds_rqt_widgets {

ArtificialHorizon::ArtificialHorizon(QWidget *parent) :
    QWidget(parent) {
   pitch = 0.0;
   roll = 0.0;
}

void ArtificialHorizon::resizeEvent(QResizeEvent *event) {
   QWidget::resizeEvent(event);
}

void ArtificialHorizon::paintEvent(QPaintEvent *event) {
   drawBackground();
   drawPitchLines();

   QWidget::paintEvent(event);
}

void ArtificialHorizon::drawPitchLines() {
   QPainter painter(this);
   painter.setRenderHint(QPainter::Antialiasing);
   QPen pen(Qt::white);
   int side = qMin(width(), height());

   painter.setViewport((width() - side) / 2, (height() - side) / 2,
                       side, side);
   painter.setWindow(-50, -50, 100, 100);

   QRegion clipRegion(-45, -45, 90, 90, QRegion::Ellipse);
   painter.setClipRegion(clipRegion);

   painter.rotate(-roll);

   double distance = 0.0;
   if (fabs(pitch) > 0.05) {
      double pitchPercent = pitch / 25.0;
      distance = pitchPercent * 45;

   }

   // now draw the pitch lines
#define ONE_WIDTH 4
#define TEN_WIDTH 8
   //painter.translate(0.0,distance);
   painter.setPen(pen);
   painter.drawLine(-46, 0, 46, 0); // the zero line
   painter.drawLine(-TEN_WIDTH, -40, TEN_WIDTH, -40);
   painter.drawLine(-ONE_WIDTH, -38, ONE_WIDTH, -38);
   painter.drawLine(-ONE_WIDTH, -36, ONE_WIDTH, -36);
   painter.drawLine(-ONE_WIDTH, -34, ONE_WIDTH, -34);
   painter.drawLine(-ONE_WIDTH, -32, ONE_WIDTH, -32);
   painter.drawLine(-TEN_WIDTH, -30, TEN_WIDTH, -30);
   painter.drawLine(-ONE_WIDTH, -28, ONE_WIDTH, -28);
   painter.drawLine(-ONE_WIDTH, -26, ONE_WIDTH, -26);
   painter.drawLine(-ONE_WIDTH, -24, ONE_WIDTH, -24);
   painter.drawLine(-ONE_WIDTH, -22, ONE_WIDTH, -22);
   painter.drawLine(-TEN_WIDTH, -20, TEN_WIDTH, -20);
   painter.drawLine(-ONE_WIDTH, -18, ONE_WIDTH, -18);
   painter.drawLine(-ONE_WIDTH, -16, ONE_WIDTH, -16);
   painter.drawLine(-ONE_WIDTH, -14, ONE_WIDTH, -14);
   painter.drawLine(-ONE_WIDTH, -12, ONE_WIDTH, -12);
   painter.drawLine(-TEN_WIDTH, -10, TEN_WIDTH, -10);
   painter.drawLine(-ONE_WIDTH, -8, ONE_WIDTH, -8);
   painter.drawLine(-ONE_WIDTH, -6, ONE_WIDTH, -6);
   painter.drawLine(-ONE_WIDTH, -4, ONE_WIDTH, -4);
   painter.drawLine(-ONE_WIDTH, -2, ONE_WIDTH, -2);
   painter.drawLine(-TEN_WIDTH, 40, TEN_WIDTH, 40);
   painter.drawLine(-ONE_WIDTH, 38, ONE_WIDTH, 38);
   painter.drawLine(-ONE_WIDTH, 36, ONE_WIDTH, 36);
   painter.drawLine(-ONE_WIDTH, 34, ONE_WIDTH, 34);
   painter.drawLine(-ONE_WIDTH, 32, ONE_WIDTH, 32);
   painter.drawLine(-TEN_WIDTH, 30, TEN_WIDTH, 30);
   painter.drawLine(-ONE_WIDTH, 28, ONE_WIDTH, 28);
   painter.drawLine(-ONE_WIDTH, 26, ONE_WIDTH, 26);
   painter.drawLine(-ONE_WIDTH, 24, ONE_WIDTH, 24);
   painter.drawLine(-ONE_WIDTH, 22, ONE_WIDTH, 22);
   painter.drawLine(-TEN_WIDTH, 20, TEN_WIDTH, 20);
   painter.drawLine(-ONE_WIDTH, 18, ONE_WIDTH, 18);
   painter.drawLine(-ONE_WIDTH, 16, ONE_WIDTH, 16);
   painter.drawLine(-ONE_WIDTH, 14, ONE_WIDTH, 14);
   painter.drawLine(-ONE_WIDTH, 12, ONE_WIDTH, 12);
   painter.drawLine(-TEN_WIDTH, 10, TEN_WIDTH, 10);
   painter.drawLine(-ONE_WIDTH, 8, ONE_WIDTH, 8);
   painter.drawLine(-ONE_WIDTH, 6, ONE_WIDTH, 6);
   painter.drawLine(-ONE_WIDTH, 4, ONE_WIDTH, 4);
   painter.drawLine(-ONE_WIDTH, 2, ONE_WIDTH, 2);

   QFont textFont = painter.font();
   textFont.setPointSize(5);
   painter.setFont(textFont);

   painter.setFont(textFont);
#define TENS_X_NEGATIVE_OFFSET -18
#define TENS_X_POSITIVE_OFFSET 10

   painter.drawText(TENS_X_NEGATIVE_OFFSET, -45, 10, 10,
                    Qt::AlignVCenter | Qt::AlignLeft,
                    "-20");
   painter.drawText(TENS_X_POSITIVE_OFFSET, -45, 10, 10,
                    Qt::AlignVCenter | Qt::AlignLeft,
                    "-20");

   painter.drawText(TENS_X_NEGATIVE_OFFSET, -25, 10, 10,
                    Qt::AlignVCenter | Qt::AlignLeft,
                    "-10");
   painter.drawText(TENS_X_POSITIVE_OFFSET, -25, 10, 10,
                    Qt::AlignVCenter | Qt::AlignLeft,
                    "-10");
#define NEGATIVE_TENS_X_NEGATIVE_OFFSET -20
#define NEGATIVE_TENS_X_POSITIVE_OFFSET 10

   painter.drawText(NEGATIVE_TENS_X_NEGATIVE_OFFSET, 35, 10, 10,
                    Qt::AlignVCenter | Qt::AlignLeft,
                    "20");
   painter.drawText(NEGATIVE_TENS_X_POSITIVE_OFFSET, 35, 10, 10,
                    Qt::AlignVCenter | Qt::AlignLeft,
                    "20");

   painter.drawText(NEGATIVE_TENS_X_NEGATIVE_OFFSET, 15, 10, 10,
                    Qt::AlignVCenter | Qt::AlignLeft,
                    "10");
   painter.drawText(NEGATIVE_TENS_X_POSITIVE_OFFSET, 15, 10, 10,
                    Qt::AlignVCenter | Qt::AlignLeft,
                    "10");

}

void ArtificialHorizon::drawBackground(void) {
   QPainter painter(this);
   painter.setRenderHint(QPainter::Antialiasing);

   QPen thickPen(palette().foreground(), 1.5);
   QPen thinPen(palette().foreground(), 1.0);
   QPen reallyThinPen(palette().foreground(), 0.5);

   // should draw rings around this like I have around the compass

   QColor thinPenColor;
   thinPenColor = Qt::white;

   QPen pen(Qt::white);
   int side = qMin(width(), height());

   painter.setViewport((width() - side) / 2, (height() - side) / 2,
                       side, side);
   painter.setWindow(-50, -50, 100, 100);

   QRadialGradient circleGradient;
   /* Draw external circle */
   circleGradient = QRadialGradient(QPointF(-50, -50), 150, QPointF(-50, -50));
   circleGradient.setColorAt(0, QColor(224, 224, 224));
   circleGradient.setColorAt(1, QColor(28, 28, 28));
   painter.setPen(pen);
   painter.setBrush(QBrush(circleGradient));
   painter.drawEllipse(QPoint(0, 0), 48, 48);
   /* Draw inner circle */
   circleGradient = QRadialGradient(QPointF(0, 0), 150);
   circleGradient.setColorAt(0, QColor(224, 224, 224));
   circleGradient.setColorAt(1, QColor(28, 28, 28));
   painter.setPen(Qt::NoPen);
   painter.setBrush(QBrush(circleGradient));
   painter.drawEllipse(QPoint(0, 0), 45, 45);



   // Draw upper horizon
   QRadialGradient gradient(QPointF(0, 0), 50);
   double targetW = 250;
   double targetH = 250;
   //gradient.setSpread(QGradient::PadSpread);
   gradient.setColorAt(1, QColor(51, 25, 0));
   gradient.setColorAt(0, QColor(225, 169, 95));
   QBrush gbrush(gradient);
   painter.setPen(QPen(QColor(0, 0, 0, 50), targetW * 0.004));
   painter.setBrush(gbrush);

   //QRegion  clipRegion(-45,-45,90,90,QRegion::Ellipse);
   //painter.setClipRegion(clipRegion);

   painter.rotate(-roll);

   // now figure out how far down or up to draw the chord, based upon the pitch angle
   double angle = 0.0;
   double distance = 0.0;
   if (fabs(pitch) > 0.05) {
      double pitchPercent = pitch / 25.0;
      distance = pitchPercent * 45;
      angle = -57.2958 * asin(distance / 45.0);
   }
   //painter.drawChord(-46,-46 , 92, 92, 30*16, 120);
   painter.drawChord(-45, -45, 90, 90, angle * 16, -(180 + 2 * angle) * 16);

   // have to figure out how to put pitch in
   //painter.drawRect(-targetW/2.0,-targetH/2.0,targetW, targetH/2.0 +pitch/2.0);
   // Draw lower ground
   gradient.setColorAt(0, QColor(0, 0, 128));
   gradient.setColorAt(1, QColor(0, 0, 32));

   QBrush gbrush2(gradient);
   painter.setBrush(gbrush2);
   painter.drawChord(-45, -45, 90, 90, angle * 16, (-2 * angle + 180) * 16);
   //painter.drawRect(-targetW/2.0,targetH/2.0 , targetW, -(targetH/2.0 - pitch/2.0));



}

void ArtificialHorizon::setPitch(double theValue) {
   pitch = theValue;

}

void ArtificialHorizon::setRoll(double theValue) {
   roll = theValue;

}

void ArtificialHorizon::refresh() {
   update();
}

}

