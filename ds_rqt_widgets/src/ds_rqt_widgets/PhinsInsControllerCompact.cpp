//
// Created by ssuman on 4/19/22 starting from PhinsInsController.
//

#include "ds_rqt_widgets/PhinsInsControllerCompact.h"

#include <ds_sensor_msgs/PhinsStdbin3.h>
#include <ds_sensor_msgs/PhinsConfig.h>
#include "ui_PhinsInsControllerCompact.h"
#include <QMessageBox>
#include <QFontDatabase>

namespace ds_rqt_widgets {

  class Gps1Sensor : public PhinsInsExternalSensor {
  public:
    explicit Gps1Sensor(QWidget* parent=0) : PhinsInsExternalSensor("GPS 1", 1,
								    0x00001000, 0x00002000,
								    0x00004000, 0x00008000, parent) {}
    int getRejectMode(const ds_sensor_msgs::PhinsConfig& config_msg) const override {
      return config_msg.gps1Rejection;
    }
    void setRejectMode(ds_sensor_msgs::PhinsConfig& config_msg, int mode) const override {
      config_msg.gps1Rejection = mode;
    }
  };

  class Gps2Sensor : public PhinsInsExternalSensor {
  public:
    explicit Gps2Sensor(QWidget* parent=0) : PhinsInsExternalSensor("GPS 2", 2,
								    0x00000010, 0x00000020,
								    0x00000040, 0x00000080, parent) {}
    int getRejectMode(const ds_sensor_msgs::PhinsConfig& config_msg) const override {
      return config_msg.gps2Rejection;
    }
    void setRejectMode(ds_sensor_msgs::PhinsConfig& config_msg, int mode) const override {
      config_msg.gps2Rejection = mode;
    }
  };

  class Dvl1Sensor : public PhinsInsExternalSensor {
  public:
    explicit Dvl1Sensor(QWidget* parent=0) : PhinsInsExternalSensor("DVL 1", 1,
								    0x00000100, 0x00000200,
								    0x00000400, 0x00000800, parent) {}
    int getRejectMode(const ds_sensor_msgs::PhinsConfig& config_msg) const override {
      return config_msg.dvl1Rejection;
    }
    void setRejectMode(ds_sensor_msgs::PhinsConfig& config_msg, int mode) const override {
      config_msg.dvl1Rejection = mode;
    }
  };

  class Dvl2Sensor : public PhinsInsExternalSensor {
  public:
    explicit Dvl2Sensor(QWidget* parent=0) : PhinsInsExternalSensor("DVL 2", 4,
								    0x00000001, 0x00000002,
								    0x00000004, 0x00000008, parent) {}
    int getRejectMode(const ds_sensor_msgs::PhinsConfig& config_msg) const override {
      return config_msg.dvl2Rejection;
    }
    void setRejectMode(ds_sensor_msgs::PhinsConfig& config_msg, int mode) const override {
      config_msg.dvl2Rejection = mode;
    }
  };

  class DvlWt1Sensor : public PhinsInsExternalSensor {
  public:
    explicit DvlWt1Sensor(QWidget* parent=0) : PhinsInsExternalSensor("DVL WT 1", 2,
								      0x00000001, 0x00000002,
								      0x00000004, 0x00000008, parent) {}
    int getRejectMode(const ds_sensor_msgs::PhinsConfig& config_msg) const override {
      return config_msg.dvlWt1Rejection;
    }
    void setRejectMode(ds_sensor_msgs::PhinsConfig& config_msg, int mode) const override {
      config_msg.dvlWt1Rejection = mode;
    }
  };

  class DvlWt2Sensor : public PhinsInsExternalSensor {
  public:
    explicit DvlWt2Sensor(QWidget* parent=0) : PhinsInsExternalSensor("DVL WT 2", 4,
								      0x00000010, 0x00000020,
								      0x00000040, 0x00000080, parent) {}
    int getRejectMode(const ds_sensor_msgs::PhinsConfig& config_msg) const override {
      return config_msg.dvlWt2Rejection;
    }
    void setRejectMode(ds_sensor_msgs::PhinsConfig& config_msg, int mode) const override {
      config_msg.dvlWt2Rejection = mode;
    }
  };

  class DepthSensor : public PhinsInsExternalSensor {
  public:
    explicit DepthSensor(QWidget* parent=0) : PhinsInsExternalSensor("Depth", 1,
								     0x00100000, 0x00200000,
								     0x00400000, 0x00800000, parent) {}
    int getRejectMode(const ds_sensor_msgs::PhinsConfig& config_msg) const override {
      return config_msg.depthRejection;
    }
    void setRejectMode(ds_sensor_msgs::PhinsConfig& config_msg, int mode) const override {
      config_msg.depthRejection = mode;
    }
  };

  class UsblSensor : public PhinsInsExternalSensor {
  public:
    explicit UsblSensor(QWidget* parent=0) : PhinsInsExternalSensor("USBL", 1,
								    0x00010000, 0x00020000,
								    0x00040000, 0x00080000, parent) {}
    int getRejectMode(const ds_sensor_msgs::PhinsConfig& config_msg) const override {
      return config_msg.usblRejection;
    }
    void setRejectMode(ds_sensor_msgs::PhinsConfig& config_msg, int mode) const override {
      config_msg.usblRejection = mode;
    }
  };

  
  // ///////////////////////////////////////////////////////////////////////// //
  // PhinsInsControllerCompact
  // ///////////////////////////////////////////////////////////////////////// //

  PhinsInsControllerCompact::PhinsInsControllerCompact(const std::string& name,
						       const std::string& reconfigure_path,
                   const bool& sensor_modes_enabled,
                   const bool& zupt_modes_enabled,
                   const bool& altitude_modes_enabled,
                   const bool& dive_modes_enabled,
                   const bool& startup_modes_enabled,
                   const bool& usbl_modes_enabled,
                   const bool& status_msgs_enabled,
						       const PhinsIns::PhinsModel& model,
						       const PhinsIns::PhinsInputs& inputs,
						       QWidget* parent) : QWidget(parent), model_(model), ui(new(Ui::PhinsInsControllerCompact)){
    ui->setupUi(this);                
    setup(inputs, sensor_modes_enabled, zupt_modes_enabled, altitude_modes_enabled, dive_modes_enabled, startup_modes_enabled, usbl_modes_enabled, status_msgs_enabled);
  }

  PhinsInsControllerCompact::~PhinsInsControllerCompact(){
    delete ui;
  }

  void PhinsInsControllerCompact::updateAges() {

  }

  void PhinsInsControllerCompact::refresh() {
    // don't update too fast
    QDateTime now = QDateTime::currentDateTimeUtc();
    qint64 msecs_to = last_gui_update_time.msecsTo(now);
    if (last_gui_update_time.isNull() || msecs_to > 100) {
      last_gui_update_time = now;
    } else {
      return;
    }

    zupt_modes_->updateMode(config_msg.zuptMode);
    altitude_modes_->updateMode(config_msg.altMode);
    dive_modes_->updateMode(config_msg.diveMode);

    for (const auto& sensor : external_sensors_) {
      sensor->refresh(msg, config_msg);
    }

    // update alignment info
    std::stringstream alignment_msg;
    if (msg.ins_algo_status[0] & 0x00000001) {
      alignment_msg <<"NAV ";
    }
    if (msg.ins_algo_status[0] & 0x00000002) {
      alignment_msg <<"ALIGN_COARSE ";
    }
    if (msg.ins_algo_status[0] & 0x00000004) {
      alignment_msg <<"ALIGN_FINE ";
    }
    if (msg.ins_algo_status[0] & 0x00000008) {
      alignment_msg <<"DEADRECK";
    }

    diveLbl_->setText(QString::fromStdString(alignment_msg.str()));

    fogBiasVecMagLbl_->setText(QString("%1 m%2/hr")
    			     .arg(sqrt(fogest_msg.vector.x*fogest_msg.vector.x +
				       fogest_msg.vector.y*fogest_msg.vector.y +
				       fogest_msg.vector.z*fogest_msg.vector.z), 11, 'f', 3)
    			     .arg(QChar(0260)));

    accBiasVecMagLbl_->setText(QString("%1 %2g")
			       .arg(sqrt(accest_msg.vector.x*accest_msg.vector.x +
					 accest_msg.vector.y*accest_msg.vector.y +
					 accest_msg.vector.z*accest_msg.vector.z), 11, 'f', 3)
			       .arg(QChar(0x00b5)));

    velMagLbl_->setText(QString("%1 m/s")
			       .arg(sqrt(msg.body_velocity_XVn[0]*msg.body_velocity_XVn[0] +
					 msg.body_velocity_XVn[1]*msg.body_velocity_XVn[1] +
					 msg.body_velocity_XVn[2]*msg.body_velocity_XVn[2]), 11, 'f', 3));

    //messagesLbl_->setText(QString::fromStdString(makeMessages(msg, model_)));
    messages_->setText(QString::fromStdString(makeMessages(msg, model_)));
  }

  void PhinsInsControllerCompact::updatePhinsbin(AnyMessage phinsbin) {
    msg = *(phinsbin.as<ds_sensor_msgs::PhinsStdbin3>());
    refresh();
  }

  void PhinsInsControllerCompact::updateConfig(AnyMessage phinscfg) {
    config_msg = *(phinscfg.as<ds_sensor_msgs::PhinsConfig>());
    //refresh(); // don't refresh for config biases-- save that for phinsbin
  }

  void PhinsInsControllerCompact::updateGyroBias(AnyMessage fogest) {
    fogest_msg = *(fogest.as<geometry_msgs::Vector3Stamped>());
    //refresh(); // don't refresh for gyro biases-- save that for phinsbin
  }
  void PhinsInsControllerCompact::updateAccelBias(AnyMessage accest) {
    accest_msg = *(accest.as<geometry_msgs::Vector3Stamped>());
    //refresh(); // don't refresh for accelerometer biases-- save that for phinsbin
  }

  void PhinsInsControllerCompact::executeStartupModeCommand(int) {
    int command = startup_modes_->getValue();

    if (command == 0)
      {
	// Trigger On Deck reinit
	trigger_ondeck_reinit_dialog();
      }
    else if (command == 1)
      {
	// Trigger Subsea reinit
	trigger_subsea_reinit_dialog();
      }
  // Trigger Freerun reunit
    else if (command ==2)
    {
      trigger_freerun_reinit_dialog();
    }
  }

  void PhinsInsControllerCompact::executeUsblModeCommand(int) {
    int command = usbl_modes_->getValue() + 1;
    usbl_modes_->updateMode(usbl_modes_->getValue());
    emit updateUsblMode(command);
  }

  void PhinsInsControllerCompact::generateNewConfig(int) {
    // make a temporary copy
    ds_sensor_msgs::PhinsConfig newConfig(config_msg);

    newConfig.zuptMode = zupt_modes_->getValue();
    newConfig.altMode = altitude_modes_->getValue();
    newConfig.diveMode = dive_modes_->getValue();

    for (const auto sensor : external_sensors_) {
      sensor->updateConfig(newConfig);
    }

    boost::shared_ptr<const ds_sensor_msgs::PhinsConfig> config_to_send(
									new const ds_sensor_msgs::PhinsConfig(newConfig));
    emit newPhinsConfig(AnyMessage(config_to_send));
  }

  void PhinsInsControllerCompact::setup(const PhinsIns::PhinsInputs& inputs, 
                                        const bool& sensor_modes_enabled,
                                        const bool& zupt_modes_enabled,
                                        const bool& altitude_modes_enabled,
                                        const bool& dive_modes_enabled,
                                        const bool& startup_modes_enabled, 
                                        const bool& usbl_modes_enabled, 
                                        const bool& status_msgs_enabled) {
    std::cout <<"Sensor Modes Enabled val: "<<sensor_modes_enabled <<std::endl;
    const QFont fixedFont = QFontDatabase::systemFont(QFontDatabase::FixedFont);
    QFont smallFont = QFontDatabase::systemFont(QFontDatabase::FixedFont);
    smallFont.setPointSize(6);
    QFont bigFont = QFontDatabase::systemFont(QFontDatabase::FixedFont);
    bigFont.setPointSize(10);

    if (sensor_modes_enabled) {
    // external sensors
    if (PhinsIns::isSet(inputs, PhinsIns::GPS1)) {
      external_sensors_.push_back(new Gps1Sensor(this));
    }
    if (PhinsIns::isSet(inputs, PhinsIns::GPS2)) {
      external_sensors_.push_back(new Gps2Sensor(this));
    }
    if (PhinsIns::isSet(inputs, PhinsIns::DVL1)) {
      external_sensors_.push_back(new Dvl1Sensor(this));
    }
    if (PhinsIns::isSet(inputs, PhinsIns::DVLWT1)) {
      external_sensors_.push_back(new DvlWt1Sensor(this));
    }
    if (PhinsIns::isSet(inputs, PhinsIns::DVL2)) {
      external_sensors_.push_back(new Dvl2Sensor(this));
    }
    if (PhinsIns::isSet(inputs, PhinsIns::DVLWT2)) {
      external_sensors_.push_back(new DvlWt2Sensor(this));
    }
    if (PhinsIns::isSet(inputs, PhinsIns::DEPTH)) {
      external_sensors_.push_back(new DepthSensor(this));
    }
    if (PhinsIns::isSet(inputs, PhinsIns::USBL)) {
      external_sensors_.push_back(new UsblSensor(this));
    }

    int row=0;
    int after_sensors_col;

    for (const auto& sensor : external_sensors_) {
      after_sensors_col = sensor->setup(ui->sensors_layout, row++, 0, sensor_modes_enabled); // all will have the same number of columns
      connect(sensor, &PhinsInsExternalSensor::rejectModeUpdated, this, &PhinsInsControllerCompact::generateNewConfig);
    }
    }

    // informational widgets
    // zupt mode widget
    zupt_modes_ = new IndicatorButtonGroup("", "", 6, this);
    //zupt_modes_->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
    QString zupt_modes_tooltip_ = "zero velocity update (ZUPT) algorithm suppresses the error growth of the INS.\nEnabling a ZUPT mode is equivalent to sending a speed sensor forced to 0m/s in the INS.\n\nStatic 10: Sends a 0m/s speed input with 10m/s standard deviation\n\nStatic .1: Sends a 0m/s speed input with 0.1m/s standard deviation\n\nAuto .01: Enables a null speed entry with 0.01m/s std. dev. when no rotation >10°/h present\n\nBench: Enables a null speed entry (0.01m/s std. dev.) & a null rotation (10°/h) when no rotation >10°/h present\n\nPosition: Manual GPS position input is repeated before each Kalman observation with current std. dev. of the manual GPS";
    zupt_modes_->addButton("Off", 0);
    zupt_modes_->addButton("Static 10", 1);
    zupt_modes_->addButton("Static .1", 2);
    zupt_modes_->addButton("Auto .01", 3);
    zupt_modes_->addButton("Bench", 4);
    zupt_modes_->addButton("Position", 5);
    //zupt_modes_->addButton("Position", 5, zupt_modes_tooltip_);
    connect(zupt_modes_, &IndicatorButtonGroup::button_selected, this, &PhinsInsControllerCompact::generateNewConfig);
    
    if (zupt_modes_enabled) {
      ui->info_layout->addWidget(zupt_modes_);
    }

    // altitude modes
    altitude_modes_ = new IndicatorButtonGroup("", "", 6, this);
    //altitude_modes_->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
    QString altitude_modes_tooltip_ = "Stabilize: Alt stabilized around 0\n\nGPS: Alt from GPS\n\nDepth: Alt from Depth Sensor\n\nHydro: Alt from tide + heave";
    altitude_modes_->addButton("Stabilize", 0);
    altitude_modes_->addButton("GPS", 1);
    altitude_modes_->addButton("Depth", 2);
    altitude_modes_->addButton("Hydro", 3);
    //altitude_modes_->addButton("Hydro", 3, altitude_modes_tooltip_);
    connect(altitude_modes_, &IndicatorButtonGroup::button_selected, this, &PhinsInsControllerCompact::generateNewConfig);
    if (altitude_modes_enabled) {
      ui->info_layout->addWidget(altitude_modes_);
    }

    // dive modes
    dive_modes_ = new IndicatorButtonGroup("Dive Mode", "", 3, this);
    QString dive_modes_tooltip_ = "On Deck: Selects GPS input to INS & turns off DVL/Depth/Etc.\n\nIn Water: Turns on DVL/Depth/USBL for dive ops\n\nManual: Unlocks ability to turn inputs on/off individually";
    dive_modes_->addButton("On Deck", 0);
    dive_modes_->addButton("In Water", 1);
    if (sensor_modes_enabled) {
      dive_modes_->addButton("Manual", 2, dive_modes_tooltip_);
    }
    connect(dive_modes_, &IndicatorButtonGroup::button_selected, this, &PhinsInsControllerCompact::generateNewConfig);
    if (dive_modes_enabled) {
      ui->info_layout->addWidget(dive_modes_);
    // Default is manual which is 2 so update the status light on startup
    //config_msg.diveMode = 2;
    //dive_modes_->updateMode(2);
    }

    // startup modes
    startup_modes_ = new IndicatorButtonGroup("Startup Mode", "", 3, this);
    QString startup_modes_tooltip_ = "Wait for Pos: Require GPS input on startup to calibrate\n\nRestore Last Pos: Use last known position to calibrate\n\nImmediate Run: Run without calibration";
    startup_modes_->addButton("Wait For Pos", 0);
    startup_modes_->addButton("Restore Last Pos",1);
    startup_modes_->addButton("Immediate Run", 2);
    //startup_modes_->addButton("Immediate Run", 2, startup_modes_tooltip_);    
    connect(startup_modes_, &IndicatorButtonGroup::button_selected, this, &PhinsInsControllerCompact::executeStartupModeCommand);
    if (startup_modes_enabled) {
      ui->info_layout->addWidget(startup_modes_);
    }
    // USBL slider
    usbl_modes_ = new IndicatorButtonGroup("", "", 5, this);
    QString usbl_modes_tooltip_ = "Precise: Prioritize accurate nav (no weighting)\n\n<-: x2\n\nBalanced:Hold position while maintaining nav accuracy (x3)\n\n->: x4\n\nStable: Prioritize position keeping (x5 USBL covariance)";
    usbl_modes_->addButton("Precise", 0);
    usbl_modes_->addButton("<-", 1);
    usbl_modes_->addButton("Balanced", 2);
    usbl_modes_->addButton("->", 3);
    usbl_modes_->addButton("Stable", 4);
    //usbl_modes_->addButton("Stable", 4, usbl_modes_tooltip_);
    connect(usbl_modes_, &IndicatorButtonGroup::button_selected, this, &PhinsInsControllerCompact::executeUsblModeCommand);
    if (usbl_modes_enabled) {
      ui->info_layout->addWidget(usbl_modes_);
    }

    message_box_ = new QGroupBox(tr("Status Msgs"), this);
    messages_ = new QLabel(tr(""), this);
    messages_->setAlignment(Qt::AlignLeft | Qt::AlignTop);
    messages_->setFont(fixedFont);
    messages_->setStyleSheet("QLabel { color : red; }");
    QHBoxLayout* message_layout = new QHBoxLayout();
    message_layout->addWidget(messages_, Qt::AlignLeft | Qt::AlignTop);
    message_box_->setLayout(message_layout);
    messages_->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Expanding);
    if (status_msgs_enabled) {
      ui->info_layout->addWidget(message_box_);
    }

    // Here we put a horizontal layout that contains:
    // - a horizontal layout with the "Launch" and "Dive" buttons which toggle the appropriate states
    //   for the sensor rejection modes, zupt, and altitude mode. Maybe this is not OK because it may depend on the vehicle
    // - a horizontal layout with fog and acc bias warnings, and max velocity warning
    //   The acc and fog bias warnings are based on thresholds on the magnitude of the 3D bias vectors
    //   Thresholds will be empirical and determined over time by looking at dives.
    //   Also a dive flag that is red if coarse_align, yellow if fine_align, and green if fully aligned

    diveTextLbl_ = new QLabel(tr("DIVE"), this);
    diveTextLbl_->setFont(smallFont);
    diveLbl_ = new QLabel(tr("N/A"), this); // red if coarse align, yellow if fine align, green if aligned
    diveLbl_->setToolTip("Red = Coarse Align, Yellow=Fine Align, Green=Aligned");
    diveLbl_->setFont(bigFont);
    fogBiasVecMagTextLbl_ = new QLabel(tr("FOG"), this);
    fogBiasVecMagTextLbl_->setFont(smallFont);
    fogBiasVecMagLbl_ = new QLabel(tr("N/A"), this); // Magnitude of the fog bias vector magnitude
    fogBiasVecMagLbl_->setToolTip("Magnitude of the FOG Bias Vector - may indicate gyro estimation issues");
    fogBiasVecMagLbl_->setFont(bigFont);
    accBiasVecMagTextLbl_ = new QLabel(tr("ACC"), this);
    accBiasVecMagTextLbl_->setFont(smallFont);
    accBiasVecMagLbl_ = new QLabel(tr("N/A"), this); // Magnitude of the acc bias vector magnitude
    accBiasVecMagLbl_->setToolTip("Magnitude of the ACC Bias Vector - may indicate accelerometer issues");
    accBiasVecMagLbl_->setFont(bigFont);
    velMagTextLbl_ = new QLabel(tr("VEL"), this);
    velMagTextLbl_->setFont(smallFont);
    velMagLbl_ = new QLabel(tr("N/A"), this); // Magnitude of the velocity - may indicate excessive drift issues
    velMagLbl_->setToolTip("Magnitude of velocity - may indicate drift issues");
    velMagLbl_->setFont(bigFont);

    ui->status_layout->addWidget(diveTextLbl_,0,0, Qt::AlignLeft);
    ui->status_layout->addWidget(diveLbl_,1,0, Qt::AlignLeft);
    ui->status_layout->addWidget(fogBiasVecMagTextLbl_,0,1, Qt::AlignLeft);
    ui->status_layout->addWidget(fogBiasVecMagLbl_,1,1, Qt::AlignLeft);
    ui->status_layout->addWidget(accBiasVecMagTextLbl_,0,2, Qt::AlignLeft);
    ui->status_layout->addWidget(accBiasVecMagLbl_,1,2, Qt::AlignLeft);
    ui->status_layout->addWidget(velMagTextLbl_,0,3, Qt::AlignLeft);
    ui->status_layout->addWidget(velMagLbl_,1,3, Qt::AlignLeft);

  }

  void PhinsInsControllerCompact::trigger_ondeck_reinit_dialog() {
    QMessageBox msgBox;
    msgBox.setWindowTitle("Confirm PHINS on deck reinit?");
    msgBox.setText(QString::fromStdString("Are you SURE you want to reinit the PHINS ON DECK? \nThis sets the PHINS to wait for GPS position!"));
    msgBox.setStandardButtons(QMessageBox::Yes);
    msgBox.addButton(QMessageBox::No);
    if (msgBox.exec() == QMessageBox::Yes) {
      emit reinitPhins(0);
    }
  }

  void PhinsInsControllerCompact::trigger_subsea_reinit_dialog() {
    QMessageBox msgBox;
    msgBox.setWindowTitle("Confirm PHINS subsea reinit?");
    msgBox.setText(QString::fromStdString("Are you SURE you want to reinit the PHINS SUBSEA? \nThis sends the last known position to the Phins!"));
    msgBox.setStandardButtons(QMessageBox::Yes);
    msgBox.addButton(QMessageBox::No);
    if (msgBox.exec() == QMessageBox::Yes) {
      emit reinitPhins(1);
    }
  }

  void PhinsInsControllerCompact::trigger_freerun_reinit_dialog() {
    QMessageBox msgBox;
    msgBox.setWindowTitle("Confirm PHINS FREERUN reinit?");
    msgBox.setText(QString::fromStdString("Are you SURE you want to reinit PHINS in IMMEDIATE RUN? \n\nThis sets the PHINS to run UNCALIBRATED AND IS ONLY TO BE USED IN AN EMERGENCY.\n\nSelect 'Wait For Pos' at end of dive to return PHINS to normal config."));
    msgBox.setStandardButtons(QMessageBox::Yes);
    msgBox.addButton(QMessageBox::No);
    if (msgBox.exec() == QMessageBox::Yes) {
      emit reinitPhins(2);
    }
  }

  std::string PhinsInsControllerCompact::makeMessages(const ds_sensor_msgs::PhinsStdbin3& msg, const PhinsIns::PhinsModel& MODEL) {
    std::stringstream ret;

    // super-high-level conditions
    if (msg.ins_user_status & 0x80000000) {
      ret <<"** !!! FAILED !!! **\n\n\n ** CHECK PHINS WEBPAGE **\n";
    }
    if (msg.ins_user_status & 0x40000000) {
      ret <<"** DEGRADED **\n";
    }
    if (msg.ins_user_status & 0x08000000) {
      ret <<"Aligning\n";
    }
    if (msg.ins_user_status & 0x04000000) {
      ret <<"Heading/Roll/Pitch INVALID\n";
    }
    // high-level hardware errors
    if (msg.sensor_status[1] & 0x80000000) {
      ret <<"HARDWARE FAILED\n";
    }
    if (msg.sensor_status[1] & 0x40000000) {
      ret <<"HARDWARE DEGRADED\n";
    }
    if (msg.sensor_status[1] & 0x00000001) {
      ret <<"FOG X ERROR\n";
    }
    if (msg.sensor_status[1] & 0x00000002) {
      ret <<"FOG Y ERROR\n";
    }
    if (msg.sensor_status[1] & 0x00000004) {
      ret <<"FOG Z ERROR\n";
    }
    if (msg.sensor_status[1] & 0x00000008) {
      ret <<"FOG SOURCE ERROR\n";
    }
    if (msg.sensor_status[1] & 0x00000010) {
      ret <<"ACC X ERROR\n";
    }
    if (msg.sensor_status[1] & 0x00000020) {
      ret <<"ACC Y ERROR\n";
    }
    if (msg.sensor_status[1] & 0x00000040) {
      ret <<"ACC Z ERROR\n";
    }
    if (msg.sensor_status[1] & 0x00000080) {
      ret <<"TEMPERATURE ERROR\n";
    }
    if (msg.sensor_status[1] & 0x00000100) {
      //ret <<"DSP OVERLOAD\n";
    }
    if (msg.sensor_status[1] & 0x00001000) {
      ret <<"Modelisation error\n";
    }

    // Sensor rejections
    if (msg.ins_algo_status[0] & 0x00008000) {
      ret <<"GPS Rejected\n";
    }
    if (msg.ins_algo_status[0] & 0x00080000) {
      ret <<"USBL Rejected\n";
    }
    if (msg.ins_algo_status[0] & 0x00800000) {
      ret <<"Depth Rejected\n";
    }
    if (msg.ins_algo_status[0] & 0x00000800) {
      ret <<"DVL BT Rejected\n";
    }
    if (msg.ins_algo_status[1] & 0x00000008) {
      ret <<"DVL WT Rejected\n";
    }
    if (msg.ins_algo_status[1] & 0x00000080) {
      ret <<"GPS2 Rejected\n";
    }
    if (msg.ins_algo_status[1] & 0x00008000) {
      ret <<"Altitude Rejected\n";
    }
    if (msg.ins_algo_status[1] & 0x08000000) {
      ret <<"EM LOG Rejected\n";
    }    
    if (msg.ins_algo_status[1] & 0x80000000) {
      ret <<"Manual POS Rejected\n";
    }
    if (msg.ins_algo_status[2] & 0x00000080) {
      ret <<"EM Log 2 Rejected\n";
    }
    if (msg.ins_algo_status[2] & 0x00000800) {
      ret <<"USBL 2 Rejected\n";
    }
    if (msg.ins_algo_status[3] & 0x00000008) {
      ret <<"DVL 2 BT Rejected\n";
    }
    if (msg.ins_algo_status[3] & 0x00000080) {
      ret <<"DVL 2 WT Rejected\n";
    }        
    // Critical algorithm failures
    if (msg.ins_algo_status[2] & 0x10000000) {
      ret <<"FIRMWARE INCOMPATIBLE WITH HARDWARE\n";
    }
    if (msg.ins_algo_status[0] & 0x10000000) {
      ret <<"ALTITUDE SATURATION\n";
    }
    if (msg.ins_algo_status[0] & 0x20000000) {
      ret <<"SPEED SATURATION\n";
    }
    if (msg.ins_algo_status[0] & 0x40000000) {
      ret <<"Interpolation Missed\n";
    }
    /*
    if (msg.ins_algo_status[0] & 0x00200000 && !(msg.ins_algo_status[0] & 0x00010000)) {
      ret <<"Valid depth with no USBL Received, CHECK USBL INPUT\n";
    }
    */
    if (msg.ins_algo_status[2] & 0x00040000) {
      ret <<"Fast alignment failed\n";
    }
    if (msg.ins_algo_status[2] & 0x00200000) {
      ret <<"External sensor data outdated\n";
    }
    if (msg.ins_algo_status[2] & 0x00400000) {
      ret <<"Sensor used before calibration\n";
    }
    if (msg.ins_algo_status[2] & 0x00800000) {
      ret <<"Fast alignment rejected\n";
    }

    // run modes
    if (msg.ins_system_status[1] & 0x00200000) {
      ret <<"Wait for position\n";
    }
    if (msg.ins_system_status[1] & 0x00010000) {
      ret <<"SIMULATION MODE\n";
    }
    if (msg.ins_algo_status[1] & 0x00800000) {
      ret <<"IMU EMULATION MODE\n";
    }
    if (msg.ins_system_status[1] & 0x00800000) {
      ret <<"Polar Mode\n";
    }
    if (msg.ins_system_status[1] & 0x01000000) {
      ret <<"Internal Log ACTIVE\n";
    }
    if (msg.ins_system_status[1] & 0x04000000) {
      ret <<"Vertical deflection received\n";
    }
    if (msg.ins_system_status[2] & 0x00000004) {
      ret <<"Advanced filtering mode enabled\n";
    }
    if (msg.ins_system_status[1] & 0x20000000) {
      ret <<"Running in R&D Mode\n";
    }
    if (msg.ins_system_status[1] & 0x40000000) {
      ret <<"Configuration Saved\n";
    }

    // detailed hardware errors
    if (msg.ins_system_status[1] & 0x00040000) {
      ret <<"DSP INCOMPATIBILITY\n";
    }
    if (msg.ins_system_status[1] & 0x08000000) {
      //ret <<"MPC OVERLOAD\n";
    }
    if (msg.ins_system_status[1] & 0x10000000) {
      ret <<"POWER SUPPLY FAILED\n";
    }
    if (msg.sensor_status[0] & 0x00000001) {
      ret <<"LOSS OF RAW DATA\n";
    }
    if (msg.sensor_status[0] & 0x00000002) {
      ret <<"FOG LASER POWER SUPPLY ERROR\n";
    }
    if (msg.sensor_status[0] & 0x00000004) {
      ret <<"FOG LASER DIODE OFF\n";
    }
    if (msg.sensor_status[0] & 0x00000008) {
      ret <<"FOG LASER NOT IN POWER CONTROL MODE\n";
    }
    if (msg.sensor_status[0] & 0x00000010) {
      ret <<"ACC X SATURATION ERROR\n";
    }
    if (msg.sensor_status[0] & 0x00000020) {
      ret <<"ACC Y SATURATION ERROR\n";
    }
    if (msg.sensor_status[0] & 0x00000040) {
      ret <<"ACC Z SATURATION ERROR\n";
    }
    if (msg.sensor_status[0] & 0x00000080) {
      ret <<"ACC X ACQUISITION ERROR\n";
    }
    if (msg.sensor_status[0] & 0x00000100) {
      ret <<"ACC Y ACQUISITION ERROR\n";
    }
    if (msg.sensor_status[0] & 0x00000200) {
      ret <<"ACC Z ACQUISITION ERROR\n";
    }
    if (msg.sensor_status[0] & 0x00000400) {
      ret <<"FOG X SATURATION ERROR\n";
    }
    if (msg.sensor_status[0] & 0x00000800) {
      ret <<"FOG Y SATURATION ERROR\n";
    }
    if (msg.sensor_status[0] & 0x00001000) {
      ret <<"FOG Z SATURATION ERROR\n";
    }
    if (msg.sensor_status[0] & 0x00002000) {
      ret <<"FOG X VPI VOLTAGE CONTROL ERROR\n";
    }
    if (msg.sensor_status[0] & 0x00004000) {
      ret <<"FOG Y VPI VOLTAGE CONTROL ERROR\n";
    }
    if (msg.sensor_status[0] & 0x00008000) {
      ret <<"FOG Z VPI VOLTAGE CONTROL ERROR\n";
    }
    if (msg.sensor_status[0] & 0x00010000) {
      ret <<"FOG X LOW POWER\n";
    }
    if (msg.sensor_status[0] & 0x00020000) {
      ret <<"FOG Y LOW POWER\n";
    }
    if (msg.sensor_status[0] & 0x00040000) {
      ret <<"FOG Z LOW POWER\n";
    }
    if (msg.sensor_status[0] & 0x00080000) {
      ret <<"FOG X ACQ ERROR\n";
    }
    if (msg.sensor_status[0] & 0x00100000) {
      ret <<"FOG Y ACQ ERROR\n";
    }
    if (msg.sensor_status[0] & 0x00200000) {
      ret <<"FOG Z ACQ ERROR\n";
    }
    if (msg.sensor_status[0] & 0x00400000) {
      ret <<"FOG X CRC ERROR\n";
    }
    if (msg.sensor_status[0] & 0x00800000) {
      ret <<"FOG Y CRC ERROR\n";
    }
    if (msg.sensor_status[0] & 0x01000000) {
      ret <<"FOG Z CRC ERROR\n";
    }
    if (msg.sensor_status[1] & 0x00000200) {
      ret <<"CAN_ACC_X Initialization Error\n";
    }
    if (msg.sensor_status[1] & 0x00000400) {
      ret <<"CAN_ACC_Y Initialization Error\n";
    }
    if (msg.sensor_status[1] & 0x00000800) {
      ret <<"CAN_ACC_Z Initialization Error\n";
    }
    if (msg.sensor_status[0] & 0x02000000) {
      ret <<"TEMPERATURE ACQUISITION ERROR\n";
    }
    if (msg.sensor_status[0] & 0x04000000) {
      ret <<"TEMPERATURE THRESHOLD ERROR\n";
    }
    if (msg.sensor_status[0] & 0x08000000) {
      ret <<"Temperature Rate Too High\n";
    }
    if (msg.sensor_status[0] & 0x10000000) {
      ret <<"Sensor data FIFO WARN\n";
    }
    if (msg.sensor_status[0] & 0x20000000) {
      ret <<"SENSOR DATA FIFO FULL ERROR\n";
    }
    if (msg.sensor_status[0] & 0x40000000) {
      ret <<"SOURCE POWER ERROR\n";
    }
    if (msg.sensor_status[0] & 0x80000000) {
      ret <<"SOURCE DATA RECEPTION ERROR\n";
    }
    // I/O Errors
    if (msg.ins_system_status[0] & 0x00000001) {
      ret <<"SERIAL ERROR on REPEATER\n";
    }
    if (msg.ins_system_status[0] & 0x00400000) {
      ret <<"ETHERNET OUTPUT FULL\n";
    }
    if (msg.ins_system_status[0] & 0x00000002) {
      if (MODEL == PhinsIns::COMPACT_C7) {
	ret <<"Invalid error for PHINS type C7\n";
      } else {
	ret << "INPUT A ERROR\n";
      }
    }
    if (msg.ins_system_status[0] & 0x00000004) {
      if (MODEL == PhinsIns::COMPACT_C7) {
	ret <<"Invalid error for PHINS type C7\n";
      } else {
	ret << "INPUT B ERROR\n";
      }
    }
    if (msg.ins_system_status[0] & 0x00000008) {
      if (MODEL == PhinsIns::COMPACT_C7) {
	ret <<"Invalid error for PHINS type C7\n";
      } else {
	ret << "INPUT C ERROR\n";
      }
    }
    if (msg.ins_system_status[0] & 0x00000010) {
      if (MODEL == PhinsIns::COMPACT_C7) {
	ret << "INPUT A ERROR\n";
      } else {
	ret << "INPUT D ERROR\n";
      }
    }
    if (msg.ins_system_status[0] & 0x00000020) {
      if (MODEL == PhinsIns::COMPACT_C7) {
	ret << "INPUT B ERROR\n";
      } else {
	ret << "INPUT E ERROR\n";
      }
    }
    if (msg.ins_system_status[0] & 0x00000040) {
      if (MODEL == PhinsIns::COMPACT_C7) {
	ret << "INPUT C ERROR\n";
      } else {
	ret << "INPUT F ERROR\n";
      }
    }
    if (msg.ins_system_status[0] & 0x00000080) {
      if (MODEL == PhinsIns::COMPACT_C7) {
	ret << "INPUT D ERROR\n";
      } else {
	ret << "INPUT G ERROR\n";
      }
    }
    if (msg.ins_system_status[0] & 0x00010000) {
      //ret <<"OUTPUT R FULL\n";
    }
    if (msg.ins_system_status[0] & 0x00020000) {
      ret << "OUTPUT A FULL\n";
    }
    if (msg.ins_system_status[0] & 0x00040000) {
      ret << "OUTPUT B FULL\n";
    }
    if (msg.ins_system_status[0] & 0x00080000) {
      ret << "OUTPUT C FULL\n";
    }
    if (msg.ins_system_status[0] & 0x00100000) {
      ret << "OUTPUT D FULL\n";
    }
    if (msg.ins_system_status[0] & 0x00200000) {
      ret <<"OUTPUT E FULL\n";
    }

    // timing info
    if (msg.ins_system_status[0] & 0x01000000) {
      ret <<"Internal Time Used\n";
    }
    if (msg.ins_system_status[1] & 0x00000200) {
      //ret <<"UTC Data Detected\n";
    }
    if (msg.ins_system_status[1] & 0x00000400) {
      //ret <<"Altitude Data Detected\n";
    }
    if (msg.ins_system_status[1] & 0x00000800) {
      //ret <<"PPS Data Detected\n";
    }
    if (msg.ins_system_status[2] & 0x00000001) {
      //ret <<"UTC2 detected\n";
    }
    if (msg.ins_system_status[2] & 0x00000002) {
      //ret <<"PPS2 detected\n";
    }
    if (msg.ins_system_status[2] & 0x00000008) {
      //ret <<"NTP sync in progress\n";
    }
    if (msg.ins_system_status[2] & 0x00000010) {
      //ret <<"NTP received\n";
    }
    if (msg.ins_system_status[2] & 0x00000020) {
      //ret <<"NTP sync successful\n";
    }
    if (msg.ins_system_status[2] & 0x00000040) {
      ret <<"NTP sync failed\n";
    }

    // normal algorithmic conditions
    if (msg.ins_algo_status[0] & 0x40000000) {
      //ret <<"Heave Initialization\n";
    }
    if (msg.ins_algo_status[1] & 0x00100000) {
      //ret <<"Static Convergence ON\n";
    }
    if (msg.ins_algo_status[1] & 0x00200000) {
      //ret <<"Static Convergence ended; NAV OK\n";
    }
    if (msg.ins_algo_status[2] & 0x00000001) {
      //ret <<"Sound Velocity Recv\n";
    }
    if (msg.ins_algo_status[2] & 0x00000002) {
      //ret <<"Sound Velocity Valid\n";
    }
    if (msg.ins_algo_status[2] & 0x00000004) {
      //ret <<"Sound Velocity Waiting\n";
    }
    if (msg.ins_algo_status[2] & 0x00000008) {
      ret <<"Sound Velocity Rejected\n";
    }
    if (msg.ins_algo_status[2] & 0x00020000) {
      //ret <<"DVL Calibration Check Active\n";
    }
    if (msg.ins_algo_status[2] & 0x00080000) {
      //ret <<"Relative speed ZUP enabled\n";
    }
    if (msg.ins_algo_status[2] & 0x00100000) {
      //ret <<"Relative speed ZUP valid\n";
    }
    if (msg.ins_algo_status[2] & 0x08000000) {
      //ret <<"Polar data valid\n";
    }
    if (msg.ins_algo_status[3] & 0x00000100) {
      //ret <<"DVL Distance Traveled Valid\n";
    }
    if (msg.ins_algo_status[3] & 0x00000200) {
      //ret <<"DVL Calibration NONE\n";
    }
    if (msg.ins_algo_status[3] & 0x00000400) {
      //ret <<"DVL Rough Calibration\n";
    }
    if (msg.ins_algo_status[3] & 0x00000800) {
      //ret <<"DVL Fine Calibration\n";
    }
    if (msg.ins_algo_status[3] & 0x00001000) {
      //ret <<"DVL Calibration Check in Progress\n";
    }

    // normal I/O activity notification
    if (msg.ins_system_status[0] & 0x04000000) {
      //ret <<"Ethernet activity\n";
    }
    if (msg.ins_system_status[0] & 0x00000100) {
      //ret <<"Input R activity\n";
    }
    if (msg.ins_system_status[0] & 0x00000200) {
      if (MODEL == PhinsIns::COMPACT_C7) {
	ret <<"Invalid activity for PHINS type C7\n";
      } else {
	//ret << "Input A activity\n";
      }
    }
    if (msg.ins_system_status[0] & 0x00000400) {
      if (MODEL == PhinsIns::COMPACT_C7) {
	ret << "Invalid activity for PHINS type C7\n";
      } else {
	//ret << "Input B activity\n";
      }
    }
    if (msg.ins_system_status[0] & 0x00000800) {
      if (MODEL == PhinsIns::COMPACT_C7) {
	ret << "Invalid activity for PHINS type C7\n";
      } else {
	//ret << "Input C activity\n";
      }
    }
    if (msg.ins_system_status[0] & 0x00001000) {
      if (MODEL == PhinsIns::COMPACT_C7) {
	//ret << "Input A activity\n";
      } else {
	//ret << "Input D activity\n";
      }
    }
    if (msg.ins_system_status[0] & 0x00002000) {
      if (MODEL == PhinsIns::COMPACT_C7) {
	//ret << "Input B activity\n";
      } else {
	//ret << "Input E activity\n";
      }
    }
    if (msg.ins_system_status[0] & 0x00004000) {
      if (MODEL == PhinsIns::COMPACT_C7) {
	//ret << "Input C activity\n";
      } else {
	//ret << "Input F activity\n";
      }
    }
    if (msg.ins_system_status[0] & 0x00008000) {
      if (MODEL == PhinsIns::COMPACT_C7) {
	//ret << "Input D activity\n";
      } else {
	//ret << "Input G activity\n";
      }
    }
    if (msg.ins_system_status[0] & 0x08000000) {
      //ret <<"PulseIn A activity\n";
    }
    if (msg.ins_system_status[0] & 0x10000000) {
      //ret <<"PulseIn B activity\n";
    }
    if (msg.ins_system_status[0] & 0x20000000) {
      //ret <<"PulseIn C activity\n";
    }
    if (msg.ins_system_status[0] & 0x40000000) {
      //ret <<"PulseIn D activity\n";
    }

    return ret.str();
  }

} // namespace ds_rqt_widgets


























































































