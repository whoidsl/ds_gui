//
// Created by ivaughn on 12/17/19.
//

#include "ds_rqt_widgets/TotalBatteryWidget.h"

namespace ds_rqt_widgets {

// ////////////////////////////////////////////////////////////////////////// //
// Module Indicator
ModuleIndicator::ModuleIndicator(QWidget* parent) : QWidget(parent) {
  fault_ = false;
  discharge_ = false;
  charge_ = false;
}

bool ModuleIndicator::getFault() const {
  return fault_;
}

bool ModuleIndicator::getDischarge() const {
  return discharge_;
}

bool ModuleIndicator::getCharge() const {
  return charge_;
}

void ModuleIndicator::setFault(bool value) {
  fault_ = value;
}

void ModuleIndicator::setDischarge(bool value) {
  discharge_ = value;
}

void ModuleIndicator::setCharge(bool value) {
  charge_ = value;
}
void ModuleIndicator::resizeEvent(QResizeEvent *event) {
  QWidget::resizeEvent(event);
}

void ModuleIndicator::paintEvent(QPaintEvent *event) {
  QPainter painter(this);
  draw(painter);
  QWidget::paintEvent(event);
}

const int ModuleIndicator::SIZE = 20;

QSize ModuleIndicator::sizeHint() const {
  return QSize(SIZE, SIZE);
}

void ModuleIndicator::draw(QPainter& painter) {
  painter.setRenderHint(QPainter::Antialiasing);
  painter.setRenderHint(QPainter::TextAntialiasing);
  painter.setViewport(0, 0, width(), height());
  painter.save();
  painter.setPen(Qt::black);

  if (charge_ && discharge_) {
    // we have to draw both!  Yay.
    QPainterPath topLeft;
    topLeft.moveTo(0,0);
    topLeft.lineTo(width(), 0);
    topLeft.lineTo(0, height());
    topLeft.closeSubpath();
    painter.setBrush(Qt::yellow);
    painter.drawPath(topLeft);

    QPainterPath botRight;
    botRight.moveTo(width(), height());
    botRight.lineTo(width(), 0);
    botRight.lineTo(0, height());
    botRight.closeSubpath();
    painter.setBrush(Qt::green);
    painter.drawPath(botRight);

  } else if (charge_) {
    painter.setBrush(Qt::yellow);
    painter.drawRect(0, 0, width(), height());
  } else if (discharge_) {
    painter.setBrush(Qt::green);
    painter.drawRect(0, 0, width(), height());
  }

  if (fault_) {
    painter.setBrush(Qt::red);
    if (charge_ || discharge_) {
      QPainterPath topRight;
      topRight.moveTo(0, 0);
      topRight.lineTo(width(), 0);
      topRight.lineTo(width(), height());
      topRight.closeSubpath();
      painter.drawPath(topRight);
    } else {
      painter.drawRect(0, 0, width(), height());
    }
  }

  if (!fault_ && !charge_ && !discharge_) {
    // clear the indicator
    painter.setBrush(Qt::lightGray);
    painter.drawRect(0, 0, width(), height());
  }

  // DONE... cleanup!
  painter.restore();
}

// ////////////////////////////////////////////////////////////////////////// //
// Charge Indicator
ChargeIndicator::ChargeIndicator(QWidget* parent) : QWidget(parent) {
  charge_percent_ = 0;
}

double ChargeIndicator::getChargePercent() const {
  return charge_percent_;
}

void ChargeIndicator::setChargePercent(double pct) {
  charge_percent_ = pct;
}

void ChargeIndicator::resizeEvent(QResizeEvent *event) {
  QWidget::resizeEvent(event);
}

void ChargeIndicator::paintEvent(QPaintEvent *event) {
  QPainter painter(this);
  draw(painter);
  QWidget::paintEvent(event);
}

const int ChargeIndicator::SIZE = 20;

QSize ChargeIndicator::sizeHint() const {
  return QSize(SIZE/5, SIZE);
}

void ChargeIndicator::draw(QPainter& painter) {
  painter.setRenderHint(QPainter::Antialiasing);
  painter.setRenderHint(QPainter::TextAntialiasing);
  painter.setViewport(0, 0, width(), height());
  painter.save();
  painter.setWindow(0, 0, width(), height());

  // draw the background
  if (this->isEnabled()) {
    painter.setPen(Qt::white);
    painter.setBrush(Qt::black);
  } else {
    painter.setPen(Qt::lightGray);
    painter.setBrush(Qt::darkGray);
  }
  painter.drawRect(0,0,width(), height());

  double pct_value = charge_percent_;
  // clip
  if (pct_value < 0)
    pct_value = 0;
  if (pct_value > 100.0)
    pct_value = 100.0;

  int len = round(height()*pct_value/100.0);
  if (this->isEnabled()) {
    painter.setPen(Qt::white);
    painter.setBrush(Qt::blue);
  } else {
    painter.setPen(Qt::lightGray);
    painter.setBrush(Qt::darkBlue);
  }

  painter.drawRect(0, height()-len, width(), len);

  painter.restore();
}

// ////////////////////////////////////////////////////////////////////////// //
// Overall Widget

TotalBatteryWidget::TotalBatteryWidget(int n_bats, QWidget* parent)
: QFrame(parent), num_bats(n_bats) {
  setup();
}

void TotalBatteryWidget::refresh() {
  update();
}

void TotalBatteryWidget::updateBatMan(AnyMessage raw_msg) {
  auto msg = raw_msg.as<ds_hotel_msgs::BatMan>();

  // do our own sums
  float charge_Ah=0;
  float capacity_Ah=0;

  if (msg->moduleAh.size() != msg->moduleCapacity.size()) {
    ROS_ERROR_STREAM("Got BatMan message with disimilar charge state / capacity sizes!");
    return;
  }

  for (size_t i=0; i<msg->moduleAh.size(); i++) {
    charge_Ah += msg->moduleAh[i];
    capacity_Ah += msg->moduleCapacity[i];
    modules[i]->setCharge(msg->moduleCharging[i]);
    modules[i]->setDischarge(msg->moduleDischarging[i]);
  }

  charge->setChargePercent(100.0*charge_Ah/capacity_Ah);
  percent->setText(QString("%1 %").arg(100.0*charge_Ah/capacity_Ah, 5, 'f', 1));
  ampHours->setText(QString("%1 Ah").arg(charge_Ah, 5, 'f', 2));
  QString min_cell_str = QString("%1%2C").arg(msg->minCellTemp, 4, 'f', 1).arg(QChar(0260));
  minCellTemp->setText(min_cell_str);
  QString max_cell_str = QString("%1%2C").arg(msg->maxCellTemp, 4, 'f', 1).arg(QChar(0260));
  maxCellTemp->setText(max_cell_str);
}

void TotalBatteryWidget::setup() {
  int row = 0;
  QFont data_font;
  data_font.setBold(true);
  data_font.setPixelSize(20);

  QFont temp_font;
  temp_font.setBold(true);

  layout = new QGridLayout();

  ampHours = new QLabel(tr("? Ah"), this);
  ampHours->setAlignment(Qt::AlignCenter);
  ampHours->setFont(data_font);
  layout->addWidget(ampHours, row++, 0, 1, 2);

  percent = new QLabel(tr("? %"), this);
  percent->setAlignment(Qt::AlignCenter);
  percent->setFont(data_font);
  layout->addWidget(percent, row++, 0, 1, 2);

  charge = new ChargeIndicator(this);
  charge->setChargePercent(0.0);
  layout->addWidget(charge, row, 1, num_bats, 1);

  for (int i=0; i < num_bats; i++) {
    modules.push_back(new ModuleIndicator(this));
    layout->addWidget(modules.back(), row++, 0);
  }

  minCellTemp = new QLabel(tr("? C"), this);
  minCellTemp->setAlignment(Qt::AlignCenter);
  minCellTemp->setFont(temp_font);
  layout->addWidget(minCellTemp, row, 0, 1, 1);

  maxCellTemp = new QLabel(tr("? C"), this);
  maxCellTemp->setAlignment(Qt::AlignCenter);
  maxCellTemp->setFont(temp_font);
  layout->addWidget(maxCellTemp, row, 1, 1, 1);
  row++;

  this->setLayout(layout);
  this->setFrameStyle(QFrame::StyledPanel | QFrame::Raised);
}

} // namespace ds_rqt_widgets
