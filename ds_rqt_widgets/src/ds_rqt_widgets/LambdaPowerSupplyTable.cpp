//
// Created by ivaughn on 10/7/20.
//

#include "ds_rqt_widgets/LambdaPowerSupplyTable.h"

namespace Field {
const static size_t name = 0;

const static size_t meas_volts = name+2;
const static size_t prog_volts = meas_volts+1;

const static size_t meas_amps = prog_volts+2;
const static size_t prog_amps = meas_amps+1;

const static size_t output_enable = prog_amps+2;
const static size_t status = output_enable+1;
const static size_t frontpanel_locked = status+1;
const static size_t mode = frontpanel_locked+1;

const static size_t latency = mode + 2;
const static size_t interval = latency + 1;
const static size_t age = interval + 1;
const static size_t NUM_ROWS = age+1;
}

namespace ds_rqt_widgets {

LambdaPowerSupplyTable::LambdaPowerSupplyTable(const std::string &name, QWidget *parent)
    : SensorTableBase(Field::age, name, parent) {
  setup();
}

void LambdaPowerSupplyTable::updatePowerSupply(AnyMessage raw) {
  msg = *(raw.as<ds_hotel_msgs::PowerSupply>());
  updateMsgTime();
  refresh();
}

void LambdaPowerSupplyTable::refresh() {
  if (!checkRefresh()) {
    return;
  }

  fields[Field::meas_volts]->setText(QString("%1 V").arg(msg.meas_volts, 6, 'f', 2));
  fields[Field::prog_volts]->setText(QString("%1 V").arg(msg.prog_volts, 6, 'f', 2));

  fields[Field::meas_amps]->setText(QString("%1 A").arg(msg.meas_amps, 6, 'f', 2));
  fields[Field::prog_amps]->setText(QString("%1 A").arg(msg.prog_amps, 6, 'f', 2));

  if (msg.output_enable) {
    fields[Field::output_enable]->setText(QString("Output ON"));
  } else {
    fields[Field::output_enable]->setText(QString("Output Off"));
  }
  fields[Field::status]->setText(QString::fromStdString(msg.status_msg));
  if (msg.frontpanel_locked) {
    fields[Field::frontpanel_locked]->setText("Locked");
  } else {
    fields[Field::frontpanel_locked]->setText("Unlocked");
  }
  std::string mode_str;
  if (msg.constant_voltage && msg.constant_current) {
    mode_str = "Constant Voltage/Current";
  } else if (msg.constant_voltage) {
    mode_str = "Constant Voltage";
  } else if (msg.constant_current) {
    mode_str = "Constant Current";
  }
  fields[Field::mode]->setText(QString::fromStdString(mode_str));

  ros::Duration latency = msg.ds_header.io_time - msg.header.stamp;
  fields[Field::latency]->setText(QString("%1 ms").arg(latency.toSec()*1000.0, 6, 'f', 1));

  fields[Field::interval]->setText(QString("%1 ms").arg(msg_interval, 8));
}

void LambdaPowerSupplyTable::setup() {
  std::vector<std::string> row_labels(Field::NUM_ROWS, " ");

  row_labels[Field::meas_volts] = "Meas. Volts";
  row_labels[Field::prog_volts] = "Prog. Volts";

  row_labels[Field::meas_amps] = "Meas. Amps";
  row_labels[Field::prog_amps] = "Prog. Amps";

  row_labels[Field::output_enable] = "Output Enable";
  row_labels[Field::status] = "Status";
  row_labels[Field::frontpanel_locked] = "Frontpanel";
  row_labels[Field::mode] = "Mode";

  row_labels[Field::latency] = "Latency";
  row_labels[Field::interval] = "Interval";
  row_labels[Field::age] = "Message Age";

  SensorTableBase::setup(row_labels);

  // make the header and have it span both columns
  fields[Field::name]->setText(QString::fromStdString(name_));
  layout->addWidget(fields[Field::name], Field::name, 0, 1, 2);
  fields[Field::name]->setAlignment(Qt::AlignCenter);
}

} // namespace ds_rqt_widgets
