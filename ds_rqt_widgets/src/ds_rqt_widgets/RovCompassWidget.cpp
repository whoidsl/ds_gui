//
// Created by ivaughn on 2/25/19.
//

#include "ds_rqt_widgets/RovCompassWidget.h"
#include <limits>

#define SIZE 200

namespace ds_rqt_widgets {

RovCompassWidget::RovCompassWidget(QWidget *parent) : QWidget(parent) {
  heading = 0;
  headingSetPoint = std::numeric_limits<double>::quiet_NaN();
}

void RovCompassWidget::refresh() {
  update();
}

double RovCompassWidget::getHeadingValue() const {
  return heading;
}

double RovCompassWidget::getHeadingSetpoint() const {
  return headingSetPoint;
}

void RovCompassWidget::setHeadingValueRadians(double headingRad) {
  heading = headingRad*180.0/M_PI;
  update();
}

void RovCompassWidget::setHeadingSetpointRadians(double headingRad) {
  headingSetPoint = headingRad*180.0/M_PI;
}

void RovCompassWidget::setHeadingValueDegrees(double headingDeg) {
  heading = headingDeg;
  update();
}

void RovCompassWidget::setHeadingSetpointDegrees(double headingDeg) {
  headingSetPoint = headingDeg;
}

void RovCompassWidget::resizeEvent(QResizeEvent *event) {
  QWidget::resizeEvent(event);
}

void RovCompassWidget::paintEvent(QPaintEvent *event) {
  QPainter painter(this);

  updateBackgroundCache();
  painter.drawPixmap(0, 0, width(), height(), backgroundCache);

  setupPainter(painter, width(), height());

  // if (northUp) {
  drawSetPoint(painter);
  drawValue(painter);
  // }

  QWidget::paintEvent(event);
}

QSize RovCompassWidget::sizeHint() const {
  return QSize(SIZE, SIZE);
}

void RovCompassWidget::setupPainter(QPainter& painter, int width, int height) {
  int side = qMin(width, height);
  painter.setViewport((width - side)/2, (height-side)/2, side, side);
  painter.setWindow(-SIZE/2, -SIZE/2, SIZE, SIZE);

  painter.setRenderHint(QPainter::Antialiasing);
  painter.setRenderHint(QPainter::TextAntialiasing);
}

void RovCompassWidget::drawBackground(QPainter& painter) {
  setupPainter(painter, width(), height());

  // prepare to draw stuff
  QPen pen(Qt::lightGray);
  pen.setWidth(1);
  painter.setRenderHint(QPainter::Antialiasing);
  painter.setRenderHint(QPainter::TextAntialiasing);
  painter.setPen(pen);

  // draw an external circle
  painter.setBrush(Qt::black);
  painter.drawEllipse(-SIZE/2, -SIZE/2, SIZE, SIZE);

  // setup our pens and stuff
  QPen majorPen(Qt::lightGray, 2.0);
  QPen minorPen(Qt::darkGray, 1.0);
  const int TICK_STOP = SIZE/2-2;
  const int TICK_LEN_MAJOR = SIZE/16;
  const int TEXT_MAJOR_SIZE = SIZE/8-SIZE/32;
  const int TICK_LEN_MINOR = SIZE/16;
  const int TEXT_MINOR_SIZE = SIZE/16-SIZE/32;
  const int TICK_LEN_TINY  = SIZE/32;

  QFont majorFont = painter.font();
  majorFont.setPixelSize(TEXT_MAJOR_SIZE);

  QFont minorFont = painter.font();
  minorFont.setPixelSize(TEXT_MINOR_SIZE);

  // draw the circle
  for (int degree=0; degree<360; degree+=5) {
    painter.save();
    painter.rotate(degree);

    if (degree % 45 == 0) {
      painter.setPen(majorPen);
      painter.setFont(majorFont);
      painter.drawLine(0, -TICK_STOP + TICK_LEN_MAJOR, 0, -TICK_STOP);
      painter.drawText(-TEXT_MAJOR_SIZE*3/2, -TICK_STOP+TICK_LEN_MAJOR,
          3*TEXT_MAJOR_SIZE, TEXT_MAJOR_SIZE+TICK_LEN_TINY,
          Qt::AlignHCenter | Qt::AlignTop,
          QString::number(degree, 'd', 0));

    } else if (degree % 15 == 0) {
      painter.setPen(minorPen);
      painter.drawLine(0, TICK_STOP - TICK_LEN_MINOR, 0, TICK_STOP);

    } else {
      painter.setPen(minorPen);
      painter.drawLine(0, TICK_STOP - TICK_LEN_TINY,  0, TICK_STOP);
    }
    painter.restore();
  }
}

void RovCompassWidget::drawValue(QPainter& painter) {

  static const QPoint needle[3] {
    QPoint(0,-SIZE/16),
    QPoint(3*SIZE/8, 0),
    QPoint(0,SIZE/16),
  };

  painter.setPen(Qt::white);
  painter.setBrush(Qt::blue);

  painter.save();
  painter.rotate(heading - 90.0);
  painter.drawConvexPolygon(needle, 3);
  painter.restore();

  const int TEXT_SIZE = SIZE/8;
  QFont font = painter.font();
  font.setBold(true);
  font.setPixelSize(TEXT_SIZE-5);
  painter.setFont(font);

  QPen backgroundPen(Qt::white, 2.0);
  painter.setPen(backgroundPen);
  painter.setBrush(Qt::black);
  QRect text_rect(-TEXT_SIZE*3/2, -TEXT_SIZE/2, TEXT_SIZE*3, TEXT_SIZE);
  painter.drawRect(text_rect);
  painter.drawText(text_rect, Qt::AlignCenter, QString::number(heading, 'f', 1));

}

void RovCompassWidget::drawSetPoint(QPainter& painter) {
  const int OUTER_EDGE = SIZE/2-2;
  const int EDGE_SIZE = SIZE/16-2;

  if (!std::isfinite(headingSetPoint)) {
    return;
  }

  static const QPoint needle[3] {
    QPoint(OUTER_EDGE-EDGE_SIZE, 0), // tip
    QPoint(OUTER_EDGE, -EDGE_SIZE),
    QPoint(OUTER_EDGE,  EDGE_SIZE),
  };

  painter.setPen(Qt::lightGray);
  painter.setBrush(Qt::darkRed);

  painter.save();
  painter.rotate(headingSetPoint - 90.0);
  painter.drawConvexPolygon(needle, 3);
  painter.restore();

}

void RovCompassWidget::updateBackgroundCache() {
  if (backgroundCache.width() == width() && backgroundCache.height() == height()) {
    // cache is valid
    return;
  }

  backgroundCache = QPixmap(width(), height());
  backgroundCache.fill(palette().background().color());

  // we create a painter to paint to the cached backdrop image; note this
  // does NOT
  QPainter painter(&backgroundCache);

  setupPainter(painter, backgroundCache.width(), backgroundCache.height());
  drawBackground(painter);

}

}