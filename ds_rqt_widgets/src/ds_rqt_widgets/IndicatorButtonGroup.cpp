//
// Created by ivaughn on 11/4/20.
//

#include <ds_rqt_widgets/IndicatorButtonGroup.h>
#include <iostream>

namespace ds_rqt_widgets {

IndicatorButtonGroup::IndicatorButtonGroup(const std::string& long_name, const std::string& short_name,
                                           int cols, QWidget* parent)
    : QGroupBox(QString::fromStdString(long_name), parent), columns(cols) {
  groupName = short_name;
  current_value = -1;

  layout = new QGridLayout();
  this->setMinimumWidth(this->minimumSizeHint().width());
  this->setLayout(layout);
};

void IndicatorButtonGroup::updateMode(int value) {
  for (auto& but : buttons) {
    if (but.first == value) {
      but.second->setState(IndicatorButton::Status::GOOD);
      current_value = value;
    } else {
      but.second->setState(IndicatorButton::Status::OFF);
    }
  }
}

void IndicatorButtonGroup::setUnknown() {
  current_value = -1;
  for (auto& but : buttons) {
    but.second->setState(IndicatorButton::Status::UNKNOWN);
  }
}

void IndicatorButtonGroup::addButton(const std::string& name, int enumval) {
  std::string butName = "";
  if (groupName == "") {
    butName = name;
  }
  else {
    butName = groupName + "\n" + name;
  }
  auto b = new IndicatorButton(QString::fromStdString(butName), this);

  int r = buttons.size()/columns;
  int c = buttons.size() - r*columns;
  layout->addWidget(b, r, c);

  buttons.push_back(std::make_pair(enumval, b));
  connect(b, &IndicatorButton::released, [this, enumval](){this->send_signal(enumval);});
}

void IndicatorButtonGroup::addButton(const std::string& name, int enumval, const QString& tipText) {
  std::string butName = "";
  if (groupName == "") {
    butName = name;
  }
  else {
    butName = groupName + "\n" + name;
  }
  auto b = new IndicatorButton(QString::fromStdString(butName), this);
  this->setToolTip(tipText);

  int r = buttons.size()/columns;
  int c = buttons.size() - r*columns;
  layout->addWidget(b, r, c);

  buttons.push_back(std::make_pair(enumval, b));
  connect(b, &IndicatorButton::released, [this, enumval](){this->send_signal(enumval);});
}

void IndicatorButtonGroup::send_signal(int enumval) {
  current_value = enumval;
  emit button_selected(enumval);
}

int IndicatorButtonGroup::getValue() const {
  return current_value;
}

} // namespace ds_rqt_widgets
