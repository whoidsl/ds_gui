#include "ds_rqt_widgets/ROVCompass.h"

namespace ds_rqt_widgets {

ROVCompass::ROVCompass(QWidget *parent) :
    QWidget(parent) {
  heading = 0.0;
  headingSetPoint = 45.0;
  headingRate = 0;
  northUp = true;
  historyLength = 0;
}

void ROVCompass::resizeEvent(QResizeEvent *event) {
  QWidget::resizeEvent(event);
}

void ROVCompass::paintEvent(QPaintEvent *event) {
  drawBackground();
  if (northUp) {
    drawValueNeedle();
    drawSetPointNeedle();
  } else {
    drawHistory();

  }
  QWidget::paintEvent(event);
}

void ROVCompass::drawBackground(void) {
  QPainter painter(this);

  static const int vehicleHeadingPointer[3][2] = {
      {-2, -49}, {+2, -49}, {0, -45}
  };

  QPen thickPen(palette().foreground(), 1.5);
  QPen thinPen(palette().foreground(), 1.0);
  QPen reallyThinPen(palette().foreground(), 0.5);

  //QColor   indicatorColor;
  //indicatorColor = Qt::yellow;

  QColor thinPenColor;
  thinPenColor = Qt::white;

  QPen pen(Qt::white);
  int side = qMin(width(), height());
  painter.setViewport((width() - side) / 2, (height() - side) / 2,
                      side, side);
  painter.setWindow(-60, -60, 120, 120);

  QRadialGradient gradient;

  pen.setWidth(1);

  /* Initialize painter */
  painter.setRenderHint(QPainter::Antialiasing, true);
  painter.setRenderHint(QPainter::TextAntialiasing, true);
  painter.setPen(pen);


  /* Draw external circle */
  gradient = QRadialGradient(QPointF(-50, -50), 150, QPointF(-50, -50));
  gradient.setColorAt(0, QColor(224, 224, 224));
  gradient.setColorAt(1, QColor(28, 28, 28));
  painter.setPen(pen);
  painter.setBrush(QBrush(gradient));
  painter.drawEllipse(QPoint(0, 0), 48, 48);
  /* Draw inner circle */
  gradient = QRadialGradient(QPointF(0, 0), 150);
  gradient.setColorAt(0, QColor(224, 224, 224));
  gradient.setColorAt(1, QColor(28, 28, 28));
  painter.setPen(Qt::NoPen);
  painter.setBrush(QBrush(gradient));
  painter.drawEllipse(QPoint(0, 0), 45, 45);
  /* Draw inner shield */


  gradient = QRadialGradient(QPointF(0, 0), 50);
  gradient.setColorAt(0, QColor(0, 0, 128));
  gradient.setColorAt(1, QColor(0, 0, 16));
  painter.setBrush(gradient);
  painter.setPen(Qt::NoPen);

  painter.drawEllipse(QPoint(0, 0), 46, 46);
  painter.setPen(pen);
  painter.setBrush(Qt::black);
  painter.drawPoint(0, 0);

  /* draw rate arcs */
  painter.setBrush(Qt::red);
  QPainterPath ratePath;
  ratePath.moveTo(59.0, 0.0);
  ratePath.arcTo(-59.0, -59.0, 118.0, 118.0, 0.0, 180.0);
  ratePath.lineTo(-55.0, 0.0);

  ratePath.arcTo(-55.0, -55.0, 110.0, 110.0, 180.0, -180.0);
  ratePath.closeSubpath();
  painter.drawPath(ratePath);

  painter.setBrush(palette().foreground());
  // now draw the ticks

  //thinPen.setColor(indicatorColor);
  //painter.setPen(thinPen);
  //painter.setBrush(indicatorColor);
  //painter.drawPolygon(QPolygon(3, &vehicleHeadingPointer[0][0]));

  thinPen.setColor(thinPenColor);
  painter.setBrush(thinPenColor);

  QFont textFont = painter.font();

  textFont.setPointSize(6);
  painter.setFont(textFont);

  QFont smallFont = painter.font();
  smallFont.setPointSize(3);
  painter.setFont(smallFont);

  QFont bigFont = painter.font();
  bigFont.setPointSize(11);
  if (!northUp) {
    painter.rotate(-heading);
  }

  for (int degree = 0; degree < 360; ++degree) {
    painter.save();
    painter.rotate(degree);
    if (0 == degree % 45)  // draw a big tick and a label
    {
      painter.setPen(thickPen);
      painter.drawLine(0, -41, 0, -44);
      painter.setFont(textFont);
      painter.drawText(-10, -39, 20, 24,
                       Qt::AlignHCenter | Qt::AlignTop,
                       QString::number(degree));
    } else if (0 == degree % 15) {
      painter.setPen(thinPen);
      painter.drawLine(0, -42, 0, -44);
      painter.setFont(smallFont);
      painter.drawText(-5, -42, 10, 12,
                       Qt::AlignHCenter | Qt::AlignTop,
                       QString::number(degree));
    } else if (0 == degree % 5) {
      painter.setPen(reallyThinPen);
      painter.drawLine(0, -43, 0, -44);
    }

    painter.restore();
  }

  //painter.setFont(bigFont);
  //painter.drawText(-76,-47,QString::number(heading));

}

void ROVCompass::setHeadingValue(double theValue) {
  heading = theValue;
  //update();
}
void ROVCompass::setGoalValue(double theValue) {
  headingSetPoint = theValue;
  //update();
}

void ROVCompass::setRate(double theRate) {
  headingRate = theRate;
  //update();
}
void ROVCompass::setHeadingAndRate(double theHeading, double theRate) {
  heading = theHeading;
  headingRate = theRate;
  //update();
}

void ROVCompass::setHistory(qreal *theHistory, int theHistoryLength) {
  if (theHistoryLength > MAX_HISTORY) {
    theHistoryLength = MAX_HISTORY;
  }
  historyLength = theHistoryLength;
  memcpy(history, theHistory, sizeof(qreal) * historyLength);

}

void ROVCompass::drawValueNeedle() {
  int side = qMin(width(), height());
  double rotateHeading = heading - 90;
  static const QPoint needle[5] = {
      QPoint(0, -2),
      QPoint(45, -0),
      QPoint(0, 2),
      QPoint(-5, 2),
      QPoint(-5, -2),
  };
  QPainter painter(this);
  painter.setRenderHint(QPainter::Antialiasing);
  painter.translate(width() / 2, height() / 2);
  painter.scale(side / 120.0, side / 120.0);
  painter.save();
  painter.rotate(rotateHeading);
  painter.setBrush(Qt::red);
  painter.drawConvexPolygon(needle, 5);
  painter.restore();

}

void ROVCompass::drawSetPointNeedle() {
  int side = qMin(width(), height());
  double rotateHeading = headingSetPoint - 90;
  static const QPoint needle[5] = {
      QPoint(0, -1),
      QPoint(40, 0),
      QPoint(0, 1),
      QPoint(-2, 1),
      QPoint(-2, -1),
  };
  QPainter painter(this);
  painter.setRenderHint(QPainter::Antialiasing);
  painter.translate(width() / 2, height() / 2);
  painter.scale(side / 120.0, side / 120.0);
  painter.save();
  painter.rotate(rotateHeading);
  painter.setBrush(Qt::yellow);
  painter.drawConvexPolygon(needle, 5);
  painter.restore();

}

void ROVCompass::drawHistory() {

  QPainter painter(this);
  QPen pen(Qt::yellow);

  pen.setWidth(1);
  int side = qMin(width(), height());

  painter.setViewport((width() - side) / 2, (height() - side) / 2,
                      side, side);
  painter.setWindow(-50, -50, 100, 100);

  qreal myScale = 46.0 / MAX_HISTORY;

  painter.setRenderHint(QPainter::Antialiasing);
  painter.setPen(pen);
  painter.save();
  //painter.rotate(-heading);
  for (int thisPoint = 0; thisPoint < historyLength; thisPoint++) {
    painter.save();
    double rotation = heading - history[thisPoint];
    if (!northUp) {
      painter.rotate(-rotation);
    }
    double myXValue = ((MAX_HISTORY - thisPoint) * (myScale));
    painter.drawPoint(0.0, -(qreal) myXValue);
    painter.restore();

  }
  painter.restore();
  update();

}

void ROVCompass::refresh() {
  update();
}

}
