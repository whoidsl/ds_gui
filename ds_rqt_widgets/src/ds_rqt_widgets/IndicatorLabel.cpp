//
// Created by ivaughn on 2/22/19.
//

#include "ds_rqt_widgets/IndicatorLabel.h"
#include <iostream>
#include <QtWidgets/QStyle>
#include <QtWidgets/QStyleOption>
#include <QPainter>
#include <sstream>

namespace ds_rqt_widgets {

IndicatorLabel::IndicatorLabel(QWidget* parent) : QLabel(parent) {
  init();
}

IndicatorLabel::IndicatorLabel(const QString& text, QWidget* parent) : QLabel(text, parent) {
  init();
}

void IndicatorLabel::init() {

  // set default colors

  // Status::DISABLED
  colors_.push_back(ColorSpec(QColor(Qt::darkGray), QColor(Qt::gray)));

  // Status::BAD
  colors_.push_back(ColorSpec(QColor(Qt::white), QColor(Qt::red)));

  // Status::GOOD
  colors_.push_back(ColorSpec(QColor(Qt::black), QColor(Qt::green)));

  // Status::WARN
  colors_.push_back(ColorSpec(QColor(Qt::black), QColor(Qt::yellow)));

  // Status::ALERT
  colors_.push_back(ColorSpec(QColor(Qt::white), QColor(Qt::blue)));

  // Status::OFF
  colors_.push_back(ColorSpec(QColor(Qt::black), QColor(Qt::gray)));

  // Status::UNKNOWN
  colors_.push_back(ColorSpec(QColor(Qt::white), QColor(Qt::black)));

  setState(Status::UNKNOWN);
}

IndicatorLabel::ColorSpec IndicatorLabel::getColorForState(Status state) const {
  return colors_[state];
}

void IndicatorLabel::setColorForState(Status state, ColorSpec color) {
  colors_[state] = color;
}

IndicatorLabel::Status IndicatorLabel::getState() const {
  return current_state_;
}

IndicatorLabel::ColorSpec IndicatorLabel::getColor() const {
  return colors_[current_state_];
}

void IndicatorLabel::setState(int state) {
  if (state < 0 || state >= colors_.size()) {
    throw std::runtime_error("Unknown indicator button state!");
  }

  current_state_ = static_cast<Status>(state);

  this->setAutoFillBackground(true);

  std::stringstream stylesheet;
  stylesheet <<"QLabel { background-color: rgb(";
  stylesheet <<colors_[current_state_].background.red() <<", ";
  stylesheet <<colors_[current_state_].background.green() <<", ";
  stylesheet <<colors_[current_state_].background.blue() <<");";
  stylesheet <<" color: rgb(";
  stylesheet <<colors_[current_state_].text.red() <<", ";
  stylesheet <<colors_[current_state_].text.green() <<", ";
  stylesheet <<colors_[current_state_].text.blue() <<");";
  stylesheet <<" }";

  this->setStyleSheet(tr(stylesheet.str().c_str()));
  //this->show();

  this->setDisabled(current_state_ == Status::DISABLED);
}

}