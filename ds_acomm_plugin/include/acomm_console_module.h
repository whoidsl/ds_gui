/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
//
// Created by ivaughn on 12/11/18.
//

#ifndef SENTRY_DEV_WS_ACOMM_CONSOLE_MODULE_H
#define SENTRY_DEV_WS_ACOMM_CONSOLE_MODULE_H

#include <ros/ros.h>

#include <rqt_gui_cpp/plugin.h>
#include <qapplication.h>
#include <QWidget>
#include <QLineEdit>
#include <QLabel>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QCheckBox>
#include <QPushButton>
#include <QPlainTextEdit>
#include <QIntValidator>

#include <ds_acomms_msgs/AcousticModemData.h>

namespace ds_acomm_console {

const static int MIN_ADDRESS = 0;
const static int MAX_ADDRESS = 65535;
const static int DIGITS_ADDRESS = 5;

class AcommConsolePlugin : public rqt_gui_cpp::Plugin
{
  Q_OBJECT
 public:
  AcommConsolePlugin();
  virtual void initPlugin(qt_gui_cpp::PluginContext& context);
  virtual void shutdownPlugin();
  virtual void saveSettings(qt_gui_cpp::Settings& plugin_settings, qt_gui_cpp::Settings& instance_settings) const;
  virtual void restoreSettings(const qt_gui_cpp::Settings& plugin_settings,
                               const qt_gui_cpp::Settings& instance_settings);

 protected:
  void setupWidget();
  void setupConnections();
  void shutdownConnections();


 private:
  // ROS stuff
  QString incoming_topic;
  QString outgoing_topic;

  ros::Subscriber incoming_sub;
  ros::Subscriber outgoing_sub;

  ros::Publisher incoming_pub;

  // QT stuff
  QWidget* widget_;

  QVBoxLayout* topLayout_;
  QPlainTextEdit* terminalBox_;
  QHBoxLayout* sendLayout_;

  QLabel* sendSrcLbl_;
  QLineEdit* sendSrcAddr_;

  QLabel* sendDestLbl_;
  QLineEdit* sendDestAddr_;

  QLineEdit* sendPayload_;
  QPushButton* sendBut_;

  QIntValidator* addressValidator_;

 protected:
  void add_message(const ds_acomms_msgs::AcousticModemData &data,
                   const std::string& dir_text);

 public slots:
  void incoming_data(const ds_acomms_msgs::AcousticModemData &data);
  void outgoing_data(const ds_acomms_msgs::AcousticModemData &data);
  void send();
  void addText(QString text);

  // We need to use signals / slots to avoid some QT multithreading nonsense
 signals:
  void textReady(QString text);

};

}

#endif //SENTRY_DEV_WS_ACOMM_CONSOLE_MODULE_H
