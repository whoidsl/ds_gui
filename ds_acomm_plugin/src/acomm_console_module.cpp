//
// Created by ivaughn on 12/11/18.
//

#include "acomm_console_module.h"
#include <ds_acomms_msgs/AcousticModemData.h>
#include <pluginlib/class_list_macros.h>

#include <sstream>
#include <iomanip>
#include <boost/date_time.hpp>
#include <QtGui>

namespace ds_acomm_console {

AcommConsolePlugin::AcommConsolePlugin() : rqt_gui_cpp::Plugin(), widget_(0) {
  ROS_INFO_STREAM("Acomm console INITIALIZED!");
  setObjectName("AcommConsolePlugin");
}

void AcommConsolePlugin::initPlugin(qt_gui_cpp::PluginContext& context) {
  ROS_INFO_STREAM("init acomm console plugin");
  incoming_topic = "/sentry/acomms/incoming_acomm_data";
  outgoing_topic = "/sentry/acomms/outgoing_acomm_data";

  widget_ = new QWidget();

  setupWidget();

  context.addWidget(widget_);
}

void AcommConsolePlugin::shutdownPlugin() {
  ROS_INFO_STREAM("Shutdown connections!");

  // unregister all publishers, subscribers, etc
  shutdownConnections();

}

void AcommConsolePlugin::saveSettings(qt_gui_cpp::Settings& plugin_settings,
                                      qt_gui_cpp::Settings& instance_settings) const {
  instance_settings.setValue("incoming_acomm_topic", incoming_topic);
  instance_settings.setValue("outgoing_acomm_topic", outgoing_topic);

}
void AcommConsolePlugin::restoreSettings(const qt_gui_cpp::Settings& plugin_settings,
                               const qt_gui_cpp::Settings& instance_settings) {
  incoming_topic = instance_settings.value("incoming_acomm_topic", incoming_topic).toString();
  outgoing_topic = instance_settings.value("outgoing_acomm_topic", outgoing_topic).toString();

  setupConnections();
}

void AcommConsolePlugin::setupWidget() {

  terminalBox_ = new QPlainTextEdit(widget_);
  terminalBox_->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
  terminalBox_->setReadOnly(true);
  QTextDocument* doc = terminalBox_->document();
  QFont font = doc->defaultFont();
  font.setFamily("Courier New");
  doc->setDefaultFont(font);

  addressValidator_ = new QIntValidator(MIN_ADDRESS, MAX_ADDRESS, widget_);

  sendSrcLbl_ = new QLabel("Local:", widget_);
  sendSrcAddr_ = new QLineEdit("0", widget_);
  sendSrcAddr_ -> setValidator(addressValidator_);
  sendSrcAddr_ -> setFixedWidth(DIGITS_ADDRESS*10);
  sendSrcAddr_ -> setMaximumWidth(DIGITS_ADDRESS*10);

  sendDestLbl_ = new QLabel("Remote:", widget_);
  sendDestAddr_ = new QLineEdit("1", widget_);
  sendDestAddr_ -> setValidator(addressValidator_);
  sendDestAddr_ -> setFixedWidth(DIGITS_ADDRESS*10);
  sendDestAddr_ -> setMaximumWidth(DIGITS_ADDRESS*10);

  sendPayload_ = new QLineEdit(widget_);
  sendBut_ = new QPushButton("Send", widget_);

  sendLayout_ = new QHBoxLayout();
  sendLayout_->addWidget(sendSrcLbl_);
  sendLayout_->addWidget(sendSrcAddr_);
  sendLayout_->addWidget(sendDestLbl_);
  sendLayout_->addWidget(sendDestAddr_);
  sendLayout_->addWidget(sendBut_);

  topLayout_ = new QVBoxLayout();
  topLayout_->addWidget(terminalBox_);
  topLayout_->addWidget(sendPayload_);
  topLayout_->addLayout(sendLayout_);

  connect(sendBut_, SIGNAL(released()), this, SLOT(send()));
  connect(sendPayload_, SIGNAL(returnPressed()), this, SLOT(send()));

  connect(this, SIGNAL(textReady(QString)), this, SLOT(addText(QString)));

  widget_->setLayout(topLayout_);
}

void AcommConsolePlugin::setupConnections() {
  ROS_INFO_STREAM("setup connections");
  ros::NodeHandle nh = getMTNodeHandle();

  incoming_sub = nh.subscribe(incoming_topic.toStdString(), 10,
                              &AcommConsolePlugin::incoming_data, this);

  outgoing_sub = nh.subscribe(outgoing_topic.toStdString(), 10,
                              &AcommConsolePlugin::outgoing_data, this);

  incoming_pub = nh.advertise<ds_acomms_msgs::AcousticModemData>(
      incoming_topic.toStdString(), 10);
}

void AcommConsolePlugin::shutdownConnections() {
  incoming_sub.shutdown();
  outgoing_sub.shutdown();
  incoming_pub.shutdown();
}

void AcommConsolePlugin::incoming_data(const ds_acomms_msgs::AcousticModemData &data) {
  add_message(data, " --> ");
}

void AcommConsolePlugin::outgoing_data(const ds_acomms_msgs::AcousticModemData &data) {
  add_message(data, " <-- ");
}
void AcommConsolePlugin::add_message(const ds_acomms_msgs::AcousticModemData &data,
                                     const std::string& dir_text) {
  const boost::posix_time::time_duration &time = data.stamp.toBoost().time_of_day();
  std::stringstream output;
  output << std::setw(2) << std::setfill('0') << time.hours() << ":";
  output << std::setw(2) << std::setfill('0') << time.minutes() << ":";
  output << std::setw(2) << std::setfill('0') << time.seconds() << " ";
  output << std::setw(DIGITS_ADDRESS) << std::setfill(' ') << data.remote_addr << dir_text;
  output << std::setw(DIGITS_ADDRESS) << std::setfill(' ') << data.local_addr << " ";
  output << std::string{std::begin(data.payload), std::end(data.payload)};

  QString toAdd = QString::fromStdString(output.str());

  emit textReady(toAdd);
}

void AcommConsolePlugin::addText(QString text) {
  terminalBox_->appendPlainText(text);
}

void AcommConsolePlugin::send() {

  // Fill in the source / destination
  ds_acomms_msgs::AcousticModemData toSend;
  bool ok = true;
  toSend.remote_addr = sendDestAddr_->text().toInt(&ok);
  if (!ok) {
    ROS_ERROR_STREAM("Unable to convert string \"" << sendDestAddr_->text().toStdString()
                                                   <<"\" to integer!");
    return;
  }
  toSend.local_addr = sendSrcAddr_->text().toInt(&ok);
  if (!ok) {
    ROS_ERROR_STREAM("Unable to convert string \"" << sendSrcAddr_->text().toStdString()
                                                   <<"\" to integer!");
    return;
  }

  // Fill in the message payload
  std::string payload = sendPayload_->text().toStdString();
  toSend.payload.resize(payload.size());
  std::copy(std::begin(payload), std::end(payload), std::begin(toSend.payload));

  // Timestamp LAST
  toSend.stamp = ros::Time::now();

  incoming_pub.publish(toSend);
}

}

PLUGINLIB_EXPORT_CLASS(ds_acomm_console::AcommConsolePlugin, rqt_gui_cpp::Plugin);
