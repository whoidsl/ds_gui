# Copyright 2018 Woods Hole Oceanographic Institution
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
import os
import rospy
import rospkg
import threading

from qt_gui.plugin import Plugin
from python_qt_binding import loadUi
#from python_qt_binding.QtGui import QWidget
# PyQt5 change, it appears:
from python_qt_binding.QtWidgets import QWidget, QLabel, QVBoxLayout, QHBoxLayout

class DemoPlugin(Plugin):

    def __init__(self, context):
        super(DemoPlugin, self).__init__(context)
        # Give QObjects reasonable names
        self.setObjectName('DemoPlugin')
        print('DEMO PLUGIN LOADED')

        # Process standalone plugin command-line arguments
        from argparse import ArgumentParser
        parser = ArgumentParser()
        # Add argument(s) to the parser.
        parser.add_argument("-q", "--quiet", action="store_true",
                      dest="quiet",
                      help="Put plugin in silent mode")
        args, unknowns = parser.parse_known_args(context.argv())
        if not args.quiet:
            print('arguments: ', args)
            print('unknowns: ', unknowns)

        # Create QWidget
        self._widget = QWidget()

        # Create some QtWidgets
        self.vb = QVBoxLayout(self._widget)
        self.label1 = QLabel('Hello, rqt!', self._widget)
        self.label2 = QLabel('Hello, world!', self._widget)
        self.vb.addWidget(self.label1)
        self.vb.addWidget(self.label2)
        for i in range(10):
            self.vb.addWidget(QLabel(str(i), self._widget))

        # Get path to UI file which should be in the "resource" folder of this package
        #ui_file = os.path.join(rospkg.RosPack().get_path('ds_rqt_demo'), 'resource', 'demo_dialog.ui')
        # Extend the widget with all attributes and children from UI file
        #loadUi(ui_file, self._widget)
        # Give QObjects reasonable names
        self._widget.setObjectName('DemoPluginUi')
        # Show _widget.windowTitle on left-top of each plugin (when 
        # it's set in _widget). This is useful when you open multiple 
        # plugins at once. Also if you open multiple instances of your 
        # plugin at once, these lines add number to make it easy to 
        # tell from pane to pane.

        #self._widget.setWindowTitle('Demo rqt Plugin')
        if context.serial_number() > 1:
            self._widget.setWindowTitle(self._widget.windowTitle() + (' (%d)' % context.serial_number()))
#        else:
#            self._widget.setWindowTitle('Demo rqt Plugin')


        # Add widget to the user interface
        context.add_widget(self._widget)
        #context.add_widget(self.hb)


        # Example of using a thread to complete tasks.
        # May not be the correct or optimal way to proceed.
#        publisher_thread = threading.Thread(target=self.run_publisher)
#        publisher_thread.setDaemon(True)
#        publisher_thread.start()

    def run_publisher(self):
        import time
        from rosgraph_msgs.msg import Clock
        pub_clock = rospy.Publisher('/clock', Clock, queue_size=1)
        tw = time.time()
        while not rospy.is_shutdown():
            time.sleep(1)
            pub_clock.publish(rospy.Time.from_sec(tw))
            tw = time.time()

    def shutdown_plugin(self):
        # TODO unregister all publishers here
        pass

    def save_settings(self, plugin_settings, instance_settings):
        # TODO save intrinsic configuration, usually using:
        # instance_settings.set_value(k, v)
        pass

    def restore_settings(self, plugin_settings, instance_settings):
        # TODO restore intrinsic configuration, usually using:
        # v = instance_settings.value(k)
        pass

    #def trigger_configuration(self):
        # Comment in to signal that the plugin has a way to configure
        # This will enable a setting button (gear icon) in each dock widget title bar
        # Usually used to open a modal configuration dialog

