# Copyright 2018 Woods Hole Oceanographic Institution
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
import os
import rospy
import rospkg
import threading
from ds_hotel_msgs.msg import Battery
# from sensor_msgs.msg import BatteryState

from qt_gui.plugin import Plugin
# from python_qt_binding import loadUi
#from python_qt_binding.QtGui import QWidget
# PyQt5 change, it appears:
from PyQt5 import QtCore, QtGui, QtWidgets
from python_qt_binding.QtWidgets import QWidget, QLabel, QVBoxLayout, QHBoxLayout, QGridLayout

class BatteryPlugin(Plugin):

    def __init__(self, context):
        super(BatteryPlugin, self).__init__(context)
        # Give QObjects reasonable names
        self.setObjectName('BatteryPlugin')

        self.time_since = 0

        print('Battery PLUGIN LOADED')

        # Process standalone plugin command-line arguments
        from argparse import ArgumentParser
        parser = ArgumentParser()
        # Add argument(s) to the parser.
        parser.add_argument("-q", "--quiet", action="store_true",
                            dest="quiet",
                            help="Put plugin in silent mode")
        args, unknowns = parser.parse_known_args(context.argv())
        if not args.quiet:
            print('arguments: ', args)
            print('unknowns: ', unknowns)

        self._init_widgets()

        if context.serial_number() > 1:
            self._widget.setWindowTitle(self._widget.windowTitle() + (' (%d)' % context.serial_number()))

        context.add_widget(self._widget)

        self._B3_subscription = rospy.Subscriber("/demo/bat/bat1/state", Battery, self.Battery_callback)
        self._B6_subscription = rospy.Subscriber("/demo/bat/bat2/state", Battery, self.Battery_callback)
        self._B6_subscription = rospy.Subscriber("/demo/bat/bat3/state", Battery, self.Battery_callback)
        self._B6_subscription = rospy.Subscriber("/demo/bat/bat4/state", Battery, self.Battery_callback)
        self._B6_subscription = rospy.Subscriber("/demo/bat/bat5/state", Battery, self.Battery_callback)
        self._B6_subscription = rospy.Subscriber("/demo/bat/bat6/state", Battery, self.Battery_callback)
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.time_callback)
        self.timer.start(1000)

    def Battery_callback(self, b_msg):

        i = b_msg.idnum

        self.time_since[i] = 0
        self.labelN[i].setText( str( b_msg.idnum))
        self.labelP[i].setText( str( round(b_msg.percentFull, 1)))
        self.labelT[i].setText( str( round(b_msg.switchTemp , 0)))
        self.labelE[i].setText( b_msg.safetyString)
        self.labeltime[i].setText( str(self.time_since[i]))
        self.labeltime[i].show()

        max_t = 0
        for t in b_msg.temperatures:
            if t>max_t:
                max_t = t
        min_v = 100000
        max_v = 0
        for v in b_msg.voltages:
            if v>max_v:
                max_v = v
            if v<min_v:
                min_v = v
        self.labelMaxT[i].setText( str( max_t))
        self.labelMaxV[i].setText( str( max_v/1000.0))
        self.labelMinV[i].setText( str( min_v/1000.0))

        if b_msg.charging :
            self.labelS[i].setText("Charging")
        elif b_msg.discharging :
            self.labelS[i].setText("Discharging")
        else :
            self.labelS[i].setText("Off")


    def time_callback(self):
        for i in range( len(self.time_since)):
            self.time_since[i] += 1
            if len(self.time_since) > 1:
                self.labeltime[i].setText( str(self.time_since[i]))

    def _init_widgets(self):
        # Create QWidget
        self._widget = QWidget()

        self.grid = QGridLayout(self._widget)

        self.verticalSpacer = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.grid.addItem(self.verticalSpacer, 8, 0)

        self.labelN = []
        self.labelP = []
        self.labelT = []
        self.labelS = []
        self.labelE = []
        self.labelMaxV = []
        self.labelMinV = []
        self.labelMaxT = []
        self.labeltime = []
        self.time_since = []

        self.newfont = QtGui.QFont("Times", 14, QtGui.QFont.Bold)
        self.add_battery_display("Battery ID:", "Percent:", "Max V:", "Min V:", "Temp:", "Max T:", "Data age:", "Status:", "Safety:")
        self.newfont = QtGui.QFont("Times", 24, QtGui.QFont.Bold)
        self.add_battery_display("", "", "", "", "", "", "", "", "")
        self.add_battery_display("", "", "", "", "", "", "", "", "")
        self.add_battery_display("", "", "", "", "", "", "", "", "")
        self.add_battery_display("", "", "", "", "", "", "", "", "")
        self.add_battery_display("", "", "", "", "", "", "", "", "")
        self.add_battery_display("", "", "", "", "", "", "", "", "")

        self._widget.setObjectName('BatteryPluginUi')

    def add_battery_display(self, strN, strP, strMaxV, strMinV, strT, strMaxT, strD, strS, strE):

        num = len(self.time_since)

        self.labelN.append(QLabel(strN, self._widget))
        self.labelT.append(QLabel(strT, self._widget))
        self.labelMaxT.append(QLabel(strMaxT, self._widget))
        self.labelP.append(QLabel(strP, self._widget))
        self.labelMaxV.append(QLabel(strMaxV, self._widget))
        self.labelMinV.append(QLabel(strMinV, self._widget))
        self.labelS.append(QLabel(strS, self._widget))
        self.labelE.append(QLabel(strE, self._widget))
        self.labeltime.append(QLabel(strD, self._widget))

        self.grid.addWidget(self.labelN[num], 0, num)
        self.grid.addWidget(self.labelT[num], 1, num)
        self.grid.addWidget(self.labelMaxT[num], 2, num)
        self.grid.addWidget(self.labelP[num], 3, num)
        self.grid.addWidget(self.labelMaxV[num], 4, num)
        self.grid.addWidget(self.labelMinV[num], 5, num)
        self.grid.addWidget(self.labelS[num], 6, num)
        self.grid.addWidget(self.labelE[num], 7, num)
        self.grid.addWidget(self.labeltime[num], 9, num)

        self.labelN[num].setFont(self.newfont)
        self.labelT[num].setFont(self.newfont)
        self.labelMaxT[num].setFont(self.newfont)
        self.labelP[num].setFont(self.newfont)
        self.labelMaxV[num].setFont(self.newfont)
        self.labelMinV[num].setFont(self.newfont)
        self.labelS[num].setFont(self.newfont)
        self.labelE[num].setFont(self.newfont)
        self.labeltime[num].setFont(self.newfont)

        self.labeltime[num].hide()

        self.time_since.append(0)

    def shutdown_plugin(self):
        # TODO unregister all publishers here
        self._B3_subscription.unregister()
        self._B6_subscription.unregister()
        self.timer.stop()

    def save_settings(self, plugin_settings, instance_settings):
        # TODO save intrinsic configuration, usually using:
        # instance_settings.set_value(k, v)
        pass

    def restore_settings(self, plugin_settings, instance_settings):
        # TODO restore intrinsic configuration, usually using:
        # v = instance_settings.value(k)
        pass

    #def trigger_configuration(self):
    # Comment in to signal that the plugin has a way to configure
    # This will enable a setting button (gear icon) in each dock widget title bar
    # Usually used to open a modal configuration dialog

