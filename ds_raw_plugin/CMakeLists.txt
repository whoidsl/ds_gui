cmake_minimum_required(VERSION 2.8.3)
project(ds_raw_plugin)

## Compile as C++11, supported in ROS Kinetic and newer
add_compile_options(-std=c++14)

list(INSERT CMAKE_MODULE_PATH 0 ${PROJECT_SOURCE_DIR}/cmake)

## Find catkin macros and libraries
## if COMPONENTS list like find_package(catkin REQUIRED COMPONENTS xyz)
## is used, also find other catkin packages
find_package(catkin REQUIRED COMPONENTS
  ds_core_msgs
  roscpp
  roslint
  rqt_gui
  rqt_gui_cpp
)

find_package(ClangFormat)
if(CLANG_FORMAT_FOUND)
    add_custom_target(clang-format-ds-raw-plugin
            COMMENT
            "Run clang-format on all project C++ sources"
            WORKING_DIRECTORY
            ${PROJECT_SOURCE_DIR}
            COMMAND
            find src
            include
            -iname '*.h' -o -iname '*.cpp'
            | xargs ${CLANG_FORMAT_EXECUTABLE} -i
            )
endif(CLANG_FORMAT_FOUND)

if("${qt_gui_cpp_USE_QT_MAJOR_VERSION} " STREQUAL "5 ")
    find_package(Qt5Widgets REQUIRED)
else()
    find_package(Qt4 COMPONENTS QtCore QtGui REQUIRED)
    include(${QT_USE_FILE})
endif()

#catkin_python_setup()
roslint_cpp()

catkin_package(
        LIBRARIES ${PROJECT_NAME}
        CATKIN_DEPENDS roscpp rqt_gui_cpp ds_core_msgs
)

set(ds_raw_plugin_SRCS
        src/ds_raw_module.cpp
)

set(ds_raw_plugin_HDRS
        include/ds_raw_module.h
)


if("${qt_gui_cpp_USE_QT_MAJOR_VERSION} " STREQUAL "5 ")
    qt5_wrap_cpp(ds_raw_plugin_MOCS ${ds_raw_plugin_HDRS})
else()
    qt4_wrap_cpp(ds_raw_plugin_MOCS ${ds_raw_plugin_HDRS})
endif()

#########################################################
## Build

include_directories(
        include
        ${catkin_INCLUDE_DIRS}
)

add_library(${PROJECT_NAME}
        ${ds_raw_plugin_HDRS}
        ${ds_raw_plugin_SRCS}
        ${ds_raw_plugin_MOCS}
        )

add_dependencies(${PROJECT_NAME} ${${PROJECT_NAME}_EXPORTED_TARGETS} ${catkin_EXPORTED_TARGETS})

target_link_libraries(${PROJECT_NAME}
        ${catkin_LIBRARIES}
        )

if("${qt_gui_cpp_USE_QT_MAJOR_VERSION} " STREQUAL "5 ")
    target_link_libraries(${PROJECT_NAME} Qt5::Widgets)
else()
    target_link_libraries(${PROJECT_NAME} ${QT_QTCORE_LIBRARY} ${QT_QTGUI_LIBRARY})
endif()
