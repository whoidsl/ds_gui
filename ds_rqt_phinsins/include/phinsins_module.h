/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/

#ifndef RQT_CPP_PHINSINS_H
#define RQT_CPP_PHINSINS_H

#include <rqt_gui_cpp/plugin.h>
#include <QWidget>
#include <ros/service_client.h>
#include <QPushButton>
#include <QGridLayout>
#include <ds_rqt_widgets/PhinsInsTab.h>
#include <ds_rqt_widgets/PhinsInsController.h>
#include <ds_rqt_widgets/QtRosSubscription.h>
#include <ds_rqt_widgets/QtRosSubscriptionManager.h>
#include <ds_rqt_widgets/QtRosSubscriptionWidget.h>
#include <ds_rqt_widgets/QtRosDynamicReconfigureClient.h>

namespace ds_rqt_phinsins
{
class PhinsInsPlugin : public rqt_gui_cpp::Plugin
{
  Q_OBJECT
public:
  PhinsInsPlugin();
  virtual void initPlugin(qt_gui_cpp::PluginContext& context);
  virtual void shutdownPlugin();
  virtual void saveSettings(qt_gui_cpp::Settings& plugin_settings, qt_gui_cpp::Settings& instance_settings) const;
  virtual void restoreSettings(const qt_gui_cpp::Settings& plugin_settings,
                               const qt_gui_cpp::Settings& instance_settings);

public slots:
    
private:

  QString phinsbin_topic;
  QString phinsaccest_topic;
  QString phinsfogest_topic;
  QString phinsconfig_topic;
  QString reset_service;
  QString name_dynamicreconfigure;

  ros::NodeHandle nh;

  QWidget* widget_;
  QWidget* widget2_;

  void setup_widget();

  ds_rqt_widgets::PhinsInsTab* phinsInsTab;

  ds_rqt_widgets::QtRosSubscriptionManager *sub_manager;
  ds_rqt_widgets::QtRosSubscriptionWidget *sub_widget;

  ds_rqt_widgets::QtRosSubscription *sub_phinsbin;
  ds_rqt_widgets::QtRosSubscription *sub_phinsfogest;
  ds_rqt_widgets::QtRosSubscription *sub_phinsaccest;
  ds_rqt_widgets::QtRosDynamicReconfigureClient *dyn_config;



private slots:
};
}  // namespace rqt_example_cpp
#endif  // RQT_CPP_BATMAN_H
