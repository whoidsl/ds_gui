/**
* Copyright 2018 Woods Hole Oceanographic Institution
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* 1. Redistributions of source code must retain the above copyright notice,
*    this list of conditions and the following disclaimer.
*
* 2. Redistributions in binary form must reproduce the above copyright notice,
*    this list of conditions and the following disclaimer in the documentation
*    and/or other materials provided with the distribution.
*
* 3. Neither the name of the copyright holder nor the names of its contributors
*    may be used to endorse or promote products derived from this software
*    without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/

#include "../include/phinsins_module.h"
#include <pluginlib/class_list_macros.h>
#include <ros/node_handle.h>
#include <ds_sensor_msgs/PhinsStdbin3.h>
#include <ds_sensor_msgs/PhinsConfig.h>

namespace ds_rqt_phinsins
{
PhinsInsPlugin::PhinsInsPlugin() : rqt_gui_cpp::Plugin()
{
  // Constructor is called first before initPlugin function, needless to say.

  // give QObjects reasonable names
  setObjectName("PhinsInsPlugin");
}

void PhinsInsPlugin::initPlugin(qt_gui_cpp::PluginContext& context)
{
  //        nh = Nodelet();
  // access standalone command line arguments
  QStringList argv = context.argv();

  phinsbin_topic = "/jason/sensors/phinsins/phinsbin";
  phinsaccest_topic = "/jason/sensors/phinsins/accest";
  phinsfogest_topic = "/jason/sensors/phinsins/fogest";
  phinsconfig_topic = "/jason/sensors/phinsins";
  reset_service = "/jason/sensors/phinsins/phins_restart";
  name_dynamicreconfigure = "/jason/sensors/phinsins";

  // create QWidget
  widget_ = new QWidget();

  setup_widget();

  context.addWidget(widget_);

  phinsInsTab = new ds_rqt_widgets::PhinsInsTab("Phins INS",
						name_dynamicreconfigure.toStdString(),
						ds_rqt_widgets::PhinsIns::PhinsModel::PHINS3,
						ds_rqt_widgets::PhinsIns::PhinsInputs::GPS1 |
						ds_rqt_widgets::PhinsIns::PhinsInputs::GPS2 |
						ds_rqt_widgets::PhinsIns::PhinsInputs::DVL1 |
						ds_rqt_widgets::PhinsIns::PhinsInputs::DVLWT1 |
						ds_rqt_widgets::PhinsIns::PhinsInputs::DVL2 |
						ds_rqt_widgets::PhinsIns::PhinsInputs::DVLWT2 |
						ds_rqt_widgets::PhinsIns::PhinsInputs::DEPTH |
						ds_rqt_widgets::PhinsIns::PhinsInputs::USBL,
						widget_);

  nh = getMTNodeHandle();

  sub_manager = new ds_rqt_widgets::QtRosSubscriptionManager(nh);


  sub_phinsbin = ds_rqt_widgets::SubscribeToTopic<ds_sensor_msgs::PhinsStdbin3>(nh, "PhinsBin",
									      phinsbin_topic.toStdString(), 10);
  sub_manager->addSubscription(sub_phinsbin);
  sub_phinsfogest = ds_rqt_widgets::SubscribeToTopic<geometry_msgs::Vector3Stamped>(nh, "FogBias",
									      phinsfogest_topic.toStdString(), 10);
  sub_manager->addSubscription(sub_phinsfogest);
  sub_phinsaccest = ds_rqt_widgets::SubscribeToTopic<geometry_msgs::Vector3Stamped>(nh, "AccBias",
									      phinsaccest_topic.toStdString(), 10);
  sub_manager->addSubscription(sub_phinsaccest);

  widget2_ = new QWidget();
  sub_widget = new ds_rqt_widgets::QtRosSubscriptionWidget(widget2_, nh, sub_manager);
  //context.addWidget(widget2_);

  dyn_config = ds_rqt_widgets::CreateDynamicReconfigureClient<ds_sensor_msgs::PhinsConfig>(nh, "",
									        phinsconfig_topic.toStdString());

  connect(sub_phinsbin, SIGNAL(received(AnyMessage)), phinsInsTab, SLOT(updatePhinsbin(AnyMessage)), Qt::QueuedConnection);
  connect(sub_phinsfogest, SIGNAL(received(AnyMessage)), phinsInsTab, SLOT(updateGyroBias(AnyMessage)), Qt::QueuedConnection);
  connect(sub_phinsaccest, SIGNAL(received(AnyMessage)), phinsInsTab, SLOT(updateAccelBias(AnyMessage)), Qt::QueuedConnection);
  connect(dyn_config, SIGNAL(received(AnyMessage)), phinsInsTab, SLOT(updateConfig(AnyMessage)), Qt::QueuedConnection);

  // Connect new requested config to dyn reconfigure client
  connect(phinsInsTab->controller(), SIGNAL(newPhinsConfig(AnyMessage)), dyn_config, SLOT(setConfiguration(AnyMessage)), Qt::QueuedConnection);

}

void PhinsInsPlugin::shutdownPlugin()
{
}

void PhinsInsPlugin::saveSettings(qt_gui_cpp::Settings& plugin_settings, qt_gui_cpp::Settings& instance_settings) const
{
  instance_settings.setValue("phinsbin_topic", phinsbin_topic);
  instance_settings.setValue("phinsaccest_topic", phinsaccest_topic);
  instance_settings.setValue("phinsfogest_topic", phinsfogest_topic);
  instance_settings.setValue("phinsconfig_topic", phinsconfig_topic);
  instance_settings.setValue("reset_service", reset_service);
  instance_settings.setValue("name_dynamicreconfigure", name_dynamicreconfigure);
}

void PhinsInsPlugin::restoreSettings(const qt_gui_cpp::Settings& plugin_settings,
                                   const qt_gui_cpp::Settings& instance_settings)
{
  phinsbin_topic = instance_settings.value("phinsbin_topic", phinsbin_topic).toString();
  phinsaccest_topic = instance_settings.value("phinsaccest_topic", phinsaccest_topic).toString();
  phinsfogest_topic = instance_settings.value("phinsfogest_topic", phinsfogest_topic).toString();
  phinsconfig_topic = instance_settings.value("phinsconfig_topic", phinsconfig_topic).toString();
  reset_service = instance_settings.value("reset_service", reset_service).toString();
  name_dynamicreconfigure = instance_settings.value("name_dynamicreconfigure", name_dynamicreconfigure).toString();
}

// XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXx


void PhinsInsPlugin::setup_widget()
{
  
}


}  // namespace ds_rqt_phinsins
PLUGINLIB_EXPORT_CLASS(ds_rqt_phinsins::PhinsInsPlugin, rqt_gui_cpp::Plugin)
