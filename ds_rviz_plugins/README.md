# ds_rviz_plugins

Rviz plugins for ds-ros.  


## Contents of this Package

Everything here is designed to function as an RVIZ plugin.

## Getting Started

This is a DSL ROS package intended to be run as part of the larger DSL ROS echosystem.
See the [Sentry Wiki](http://sentry-wiki.whoi.edu/ROS_Upgrade)

### Prerequisites

What things you need to install the software and how to install them

```
Give examples
```

Although, you're setting up all your custom dependencies as debs or something right?  
And you're DEFINITELY saving any source tarballs you need to the DAV, right? 
 Yes, I'm looking at you!

### Installing

A step by step series of examples that tell you have to get a development env running

Say what the step will be

```
Give the example
```

And repeat

```
until finished
```

End with an example of getting some data out of the system or using it for a little demo

## Running the tests

Explain how to run the automated tests for this system

### Break down into end to end tests

Explain what these tests test and why

```
Give an example
```

Really, anything we need to qualify or re-qualify this software belongs in here.

## Deployment

Add additional notes about how to deploy this on a live system

We use [SemVer](http://semver.org/) for versioning.

## Authors

* **Your Name Here** - *Initial work* - [WHOI](yourusername@whoi.edu)

## Acknowledgments

* Hat tip to anyone who's code was used


